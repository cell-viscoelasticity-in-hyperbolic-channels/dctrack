from PyQt5 import QtWidgets


class TrackingSession(object):
    """
    This class provides a "gui" to keep track of the
    PyQt5.QtWidget.QApplication object that is needed for handling the GUI.
    Since the reference to this object must not get lost during execution
    of a script, an object of this "gui" class has to be called at the
    beginning of the script and it's internal attribute `app` has to be hand
    over to every new `TrackedDataSet` object that is created.
    """
    def __init__(self):
        self.app = QtWidgets.QApplication([])

    def get_app(self):
        return self.app
