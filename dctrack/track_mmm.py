import warnings
import numpy as np
import pathlib
import cv2
from scipy import spatial
from itertools import chain
from imageio import get_writer
from tqdm import tqdm, trange

import dclab
from dclab.rtdc_dataset.core import RTDCBase

from . import utils
from . import io
from .video import extend_img


class WrongPixelSizeType(BaseException):
    pass


class ObjectNumberError(BaseException):
    pass


class NoImageDataAvailable(BaseException):
    pass


# define some constants
# number of points in each row for path that cells must follow
ROW_GRID_POINTS = 50
# number of rows of the MMM chip
MMM_ROWS = 11

# some colors, basically default matplotlib colormap
# * 255 to convert to uint8 for use with cv2
COLORS = np.array(((0.12156, 0.46666, 0.70588),
                   (1.0, 0.49803, 0.05490),
                   (0.17254, 0.62745, 0.17254),
                   (0.83921, 0.15294, 0.15686),
                   (0.73725, 0.74117, 0.13333))) * 255


def path_length(path):
    """Calculate euclidian distance covered by a sorted list of (x,y) values

    Parameters
    ----------
    path: ndarray
        2D Array with xy values of path.

    Returns
    -------
    length: float
        Total length of the path as the sum of all its parts in the same units
        as path. Usually, that means in pixels.
    """
    x = path[:, 0]
    y = path[:, 1]
    n = len(x)
    lv = [np.sqrt((x[i] - x[i - 1]) ** 2 + (y[i] - y[i - 1]) ** 2) for i in
          range(1, n)]
    length = sum(lv)
    return length


class TrackedMMM(object):
    """
    Class for tracking objects in an MMM chip.
    In new data, the flow direction is from top-left to bottom-right, while
    in older data (especially data acquired with Shape-In 1), the flow
    direction is often from top-right to bottom-left.

    Notes
    ----------
    The region of interest should be just the serpentines, not the inlet and
    not the outlet.
    Please use the `:func:` `TrackedMMM.export_tracked_dataset_as_hdf5` method
    when you want to export the dataset as a .hdf5 (.rtdc) file.
    This is equivalent to the `rtdc_ds.export.hdf5` function but handles all
    the necessary and temporary features needed by dctrack.

    Per default, the filtered results are exported to videos and csv files,
    but the complete RTDC dataset is exported.
    """

    def __init__(self, data_input):
        self.input_file = data_input
        self.output_dir = None
        # load dataset when initiating object
        self.dataset = self._load_data()
        self.ind_good_events = []
        self.good_objects = []
        self.velocities = {}
        self.roi = None
        # multiplies with std of med_displacement to determine pred_zone size
        self.pred_zone_size = None
        self.channel_path = None
        self.img_with_path = None
        self.pixel_size = self.dataset.config["imaging"]["pixel size"]
        self.fps = self.dataset.config['imaging']['frame rate']
        # minimum pred_zone uncertainty in units of channel_path segments
        self.window_size = None
        # number of frames after tracking for an object stops if it wasn't
        # found within these frames
        self.frames_disappear = 3
        self.flow_direction = "right"
        self._check_dclab_metadata()
        self._set_user_tracking_metadata_as_attributes()

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        # close the HDF5 file
        self.dataset.h5file.close()

    def _load_data(self):
        """
        Load the measurement data to perform object tracking on and concert
        into a format that can be used by the tracking functions.

        Parameters:
            Currently supported:
            - .rtdc and .hdf5 files containing RT-DC datasets measured
            withShapeIn or exported with dclab
            - RTDCBase: rtdc datasets created with dclab.new_dataset

        Returns:
            dclab dataset; RTDCBase object that can be created with
            dclab.new_dataset()
        """
        if isinstance(self.input_file, (str, pathlib.Path)):
            path = pathlib.Path(self.input_file).resolve()
            if not path.exists():
                raise FileNotFoundError(f"Could not find file '{path}'")

            if path.suffix in [".rtdc", ".hdf5"]:
                dataset = dclab.new_dataset(path)
                self.set_output_dir(path.parent)
            else:
                msg = f"Input file type not supported: " \
                      f"{self.input_file.suffix}"
                raise NotImplementedError(msg)

        elif isinstance(self.input_file, RTDCBase):
            dataset = self.input_file
            self.set_output_dir(dataset.path.parent)
        else:
            msg = f"Data type not supported: {type(self.input_file)}"
            raise NotImplementedError(msg)

        # input data must contain the features needed for tracking
        for feat in utils.TRACKING_FEATURES:
            if feat not in dataset:
                raise ValueError(f"Input data does not contain necessary "
                                 f"feature for tracking: '{feat}'")
        for feat in utils.TRACK_MMM_TEMPORARY_FEATURES:
            if not dclab.dfn.feature_exists(feat):
                dclab.register_temporary_feature(feature=feat,
                                                 is_scalar=True)

        return dataset

    def _check_dclab_metadata(self):
        """Fill in the user section of the dclab config"""
        if "user" not in self.dataset.config:
            utils.create_user_config_section(self.dataset)
        utils.populate_user_config_section(self.dataset, mode="mmm")

    def _set_user_tracking_metadata_as_attributes(self):
        """Create the tracking attributes from the dclab config user section"""
        self.set_tracking_metadata(self.dataset.config["user"])

    def set_tracking_metadata(self, metadata):
        """Set the metadata for tracking, expects a dictionary. Ignores all
        keys with non-tracking related metadata.

        Parameters
        ----------
        metadata : dict
            The tracking metadata which can be set by the user. One or more
            metadata can be set at once. See notes.

        Notes
        -----
        The `metadata` given must be one of the dclab user config section
        keys. Options are: "roi", "pred_zone_size", "channel_path",
        "flow_direction", "window_size", "frames_disappear".
        Each of these have their own set methods.

        *To set non-tracking related metadata, please use dclab directly!*
        """
        if not isinstance(metadata,
                          (dict, dclab.rtdc_dataset.config.ConfigurationDict)):
            raise TypeError("`metadata` input must be a dictionary,"
                            f"got {type(metadata)} instead.")
        for key in metadata:
            self._set_metadata_by_key_value(key, metadata[key])

    def _set_metadata_by_key_value(self, key, value):
        """Internal method for setting the metadata by key and value"""
        if key == "roi":
            if not isinstance(value, list):
                # case when rtdc file was loaded
                value = list(value)
            self.set_roi(value)
        elif key == "pred_zone_size":
            self.set_pred_zone_size(value)
        elif key == "channel_path":
            self.set_channel_path(value)
            if len(value) == ROW_GRID_POINTS * MMM_ROWS and \
                    "image" in self.dataset:
                self.set_img_with_path(value)
        elif key == "flow_direction":
            self.set_flow_direction(value)
        elif key == "window_size":
            self.set_window_size(value)
        elif key == "frames_disappear":
            self.set_frames_disappear(value)
        else:
            # ignore other metadata
            warnings.warn(f"Unknown key '{key}' encountered! If you want to "
                          f"add metadata unrelated to tracking, please use "
                          f"dclab directly.")

    def print_tracking_metadata(self):
        """Print out the tracking metadata values"""
        return self.dataset.config["user"]

    def _add_temp_features(self):
        """Set the temporary features needed for tracking"""
        empty_array = np.full(len(self.dataset), np.nan)

        for new_feat in ["prediction_zone_start", "prediction_zone_end",
                         "object_number", "obj_area_before", "obj_area_after",
                         "obj_deform_before", "obj_deform_after"]:
            if new_feat not in self.dataset:
                dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                            feature=new_feat,
                                            data=empty_array.copy())

    def set_output_dir(self, path=None):
        """ Function to set the output directory."""
        if path is None or not isinstance(path, (str, pathlib.Path)):
            raise ValueError("No valid output path specified!")
        path = pathlib.Path(path)
        if not path.exists():
            path.mkdir(parents=True, exist_ok=True)
        self.output_dir = path

    def set_roi(self, value):
        self.roi = value
        self.dataset.config["user"]["roi"] = value

    def set_channel_path(self, value):
        self.channel_path = value
        self.dataset.config["user"]["channel_path"] = value

    def set_img_with_path(self, channel_path):
        if 'image' in self.dataset.features:
            img = np.median(self.dataset["image"][0:9], axis=0)
        else:
            img = None
        rows_y = np.unique(channel_path[:, 1])
        row_size = rows_y[1] - rows_y[0]
        row_limits = rows_y - 0.5 * row_size
        row_limits = np.append(row_limits, row_limits[-1] + row_size)
        for i in row_limits:
            cv2.line(img, (0, int(i)), (img.shape[1], int(i)), (255, 0, 0), 1)

        x_left_curve = np.min(channel_path[:, 0])
        x_right_curve = np.max(channel_path[:, 0])
        for i in rows_y:
            cv2.line(img, (0, int(i)), (img.shape[1], int(i)),
                     (255, 255, 255), 1)
            cv2.circle(img, (int(x_left_curve), int(i)), 3, (0, 255, 0), -1)
            cv2.circle(img, (int(x_right_curve), int(i)), 3, (0, 255, 0), -1)

        font = cv2.FONT_HERSHEY_SIMPLEX
        for point, enum in zip(channel_path, range(len(channel_path))):
            coords = (int(point[0]), int(point[1]))
            cv2.circle(img, coords, 2, (0, 0, 255), -1)
            if enum % 10 == 0:
                cv2.putText(img, str(enum), coords,
                            font, 0.3, (255, 255, 255), 1, cv2.LINE_AA)
        self.img_with_path = img

    def set_pixel_size(self, new_px_size):
        if not isinstance(new_px_size, (int, float, np.integer, np.floating)):
            raise WrongPixelSizeType
        self.pixel_size = new_px_size

    def set_window_size(self, value):
        if not isinstance(value, (int, float, np.integer, np.floating)):
            raise TypeError(f"Window_size should be of type int or "
                            f"float, got {type(value)} instead.")
        self.window_size = value
        self.dataset.config["user"]["window_size"] = value

    def set_frames_disappear(self, value):
        if not isinstance(value, (int, float, np.integer, np.floating)):
            raise TypeError(f"Number of frames after which an object should "
                            f"not be tracked anymore should be of type int or "
                            f"float, got {type(value)} instead.")
        else:
            self.frames_disappear = value
            self.dataset.config["user"]["frames_disappear"] = value

    def set_pred_zone_size(self, new_size=3):
        """Set the factor for how large the prediction zone should be.
        Default is "3", which means three times the standard deviation of the
        travelled path (based on path index).

        Parameters
        ----------
        new_size: int or float
        """
        if not isinstance(new_size, (int, float, np.integer, np.floating)):
            raise TypeError(f"Size factor for prediction zones must be of type"
                            f" int or float, got {type(new_size)} instead.")
        self.pred_zone_size = new_size
        self.dataset.config["user"]["pred_zone_size"] = new_size

    def set_flow_direction(self, flow_direction):
        """Set the flow direction of the cells so that the path through the
        chip can be set correctly. In recently acquired data (after 2020), the
        cells flow from top-left to bottom-right. In older data (especially
        data acquired with Shape-In 1), the flow direction is often from
        top-right to bottom-left.

        Parameters
        ----------
        flow_direction: str
            Must be either 'left' or 'right'.
        """
        if not isinstance(flow_direction, str):
            raise TypeError(f"Parameter 'flow_direction' must be a string"
                            f"saying either 'left' or 'right', got type "
                            f"{type(flow_direction)} instead!")
        else:
            if flow_direction.lower() == "left":
                self.flow_direction = "left"
                self.dataset.config["user"][
                    "flow_direction"] = self.flow_direction
            elif flow_direction.lower() == "right":
                self.flow_direction = "right"
                self.dataset.config["user"][
                    "flow_direction"] = self.flow_direction
            else:
                print(f"Flow direction was attempted to be set, but the value "
                      f"{flow_direction} was not recognized as one of the two "
                      f"allowed values 'left' or 'right'. Will use default "
                      f"value ('right') for now.")
                self.flow_direction = "right"
                self.dataset.config["user"][
                    "flow_direction"] = self.flow_direction

    def set_roi_gui(self, resize=1):
        """
        Get the region of interest for the dataset.

        Parameters
        ----------
        resize : float
            Scaling factor to rescale the image before selecting the region
            of interest.

        Notes
        -----
        roi : list
            The corner points of the region of interest: [x, y, x, y]
        """
        if 'image' in self.dataset.features:
            img = np.median(self.dataset["image"][0:9], axis=0)
        else:
            raise NoImageDataAvailable("The feature `image` is missing in"
                                       "the dataset.")
        image = cv2.resize(img, None, fx=resize, fy=resize,
                           interpolation=cv2.INTER_CUBIC)
        win_name = "Select region of interest. Press 'space' or 'enter' to " \
                   "finish."
        roi_raw = cv2.selectROI(win_name, image)
        roi = [int(coord / resize) for coord in roi_raw]
        cv2.destroyWindow(win_name)
        if not roi:
            raise ValueError("Region of interest is not correct! Aborted.")
        self.set_roi(roi)

    def set_roi_automatically(self):
        """Find the region of interest (ROI) based on how the standard
        deviation of pixels changes row by row or column by column in a
        vertical stripe in the middle of the image for vertical limits (y)
        and in a horizontal stripe in the center of the image for the
        horizontal (x) limits.

        Notes
        -----
        The function checks the top of the image, determines the channel limit,
        then rotates the image by 90 degrees (to the left), determines the
        channel limit again, and rotates ...
        This means, the first value in the list `borders` will be y_min,
        the second one (because the rotation is counter-clockwise) is x_max,
        then comes y_max, and then x_min.
        """
        img = self.dataset["image"][0]
        # coordinate system origin in top-left corner
        roi = []
        for nb_rotate, op in enumerate([1, -1, -1, 1]):
            img_rot = np.rot90(img, nb_rotate)
            px_std = 0
            line_ind = 0
            while px_std < 6:
                pxls = img_rot[line_ind, int(img_rot.shape[1] / 2 - 40):int(
                    img_rot.shape[1] / 2 + 10)]
                px_std = np.std(pxls)
                line_ind += 1
            if op == 1:
                roi.append(line_ind)
            else:
                roi.append(img_rot.shape[0] - line_ind)

        # set roi: (x_min, y_min, x_max, y_max)
        self.set_roi([roi[3], roi[0], roi[1], roi[2]])

    def track(self):
        """
        Track objects in an MMM channel. The region of interest needs to be
        set already. For that, either call `set_roi_automatically` or
        `set_roi_gui`.

        Notes
        -----
        This function calls the following list of functions to track the
        events:
            - self._add_temp_features (optional, if not called before)
            - self.get_channel_path (optional, if channel path doesn't exist
              yet)
            - self.export_path_img()
            - self.find_objects_and_pred_zones()
            - self.get_good_events()
            - self.assign_cell_params()
            - self.row_velocity()
        """
        if "object_number" not in self.dataset:
            self._add_temp_features()
        if len(self.channel_path) != ROW_GRID_POINTS * MMM_ROWS:
            self.get_channel_path()
        self.export_path_img()
        self.find_objects_and_pred_zones()
        self.get_good_events()
        self.assign_cell_params()
        self.row_velocity()

    def get_channel_path(self):
        """
        Determine channel path of the MMM channel from the detected event
        values and add path index data as temporary feature to the dataset.
        Objects enter the channel in the top-left corner and move to the
        bottom-right corner. In older data, cells often move from top-right
        to bottom-left corner.

        Notes
        -----
        For the best path, the data should be prefiltered such that the there
        are no events outside the channel. Otherwise, the channel path will
        be a little distorted compared to the channel.

        This function sets the attribute `img_with_path`.
        """
        # find extreme values for y --> define first and last rows from this
        min_y = self.roi[1] - 3
        max_y = self.roi[3] + 3

        img = self.dataset["image"][0]
        if min_y < 0:
            min_y = 0
        if max_y > img.shape[0]:
            max_y = img.shape[0]

        # pos_x is in um, so we directly convert to pixel values
        arr_x_pos = self.dataset["pos_x"][:] / self.pixel_size
        arr_y_pos = self.dataset["pos_y"][:] / self.pixel_size
        # from histogram of y-values: get limits for the y values of each row
        _, row_limits = np.histogram(arr_y_pos, bins=MMM_ROWS,
                                     range=(min_y, max_y))
        row_size = row_limits[1] - row_limits[0]

        # define the middle line of each row
        rows_y = row_limits[:-1] + 0.5 * row_size

        # find min and max x positions in the channel = find curve positions
        ind = np.nonzero(
            (arr_y_pos > row_limits[1]) & (arr_y_pos < row_limits[-2]))[0]
        x_inner_rows = np.array(arr_x_pos)[ind]

        x_left_curve = np.min(x_inner_rows[x_inner_rows > self.roi[0]])
        x_right_curve = np.max(x_inner_rows[x_inner_rows < self.roi[2]])

        x_grid = np.linspace(x_left_curve, x_right_curve, ROW_GRID_POINTS)

        # create the path that cells must follow logically through the channel
        # starting from top-left to bottom-right
        # x_grid[0] will be the first entry for all even rows
        # --> cells go from left to right in even rows
        # opposite behavior in odd rows
        channel_path = []
        # rowN+1 bc. of definition of np.arange = [start,stop)
        evens = np.arange(0, MMM_ROWS + 1, 2)

        if self.flow_direction == "left":
            # if path is mirrored, the cells are flowing in the opposite
            # direction (right to left) and the rows must be reversed
            for k in range(len(rows_y)):
                if k in evens:
                    row = [[i, rows_y[k]] for i in x_grid[::-1]]
                else:
                    row = [[i, rows_y[k]] for i in x_grid]
                channel_path.append(row)
        else:
            for k in range(len(rows_y)):
                if k in evens:
                    row = [[i, rows_y[k]] for i in x_grid]
                else:
                    row = [[i, rows_y[k]] for i in x_grid[::-1]]
                channel_path.append(row)
        channel_path = np.array(list(chain.from_iterable(channel_path)))
        self.set_channel_path(channel_path)

        # create an image with the path drawn into the channel
        self.set_img_with_path(channel_path)

        # find the position on the channel_path for each contour and save it
        path_index = np.zeros_like(arr_x_pos)
        for i in range(len(arr_x_pos)):
            pt = [arr_x_pos[i], arr_y_pos[i]]
            # find point on channel_path that is closest to point
            path_index[i] = spatial.KDTree(channel_path).query(pt)[1]

        dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                    feature="path_index",
                                    data=path_index)

    def get_median_displacement(self):
        """Get median displacement from single cells passing the channel. This
        is basically the same as estimating the median velocity of objects
        and will be used for tracking to estimate the size of the prediction
        zone.
        All frames that contain more than one object are not used for the
        calculation. Only events in consecutive frames are considered.
        Objects must not move backwards, so only those with a positive
        displacement are considered.

        Returns
        -------
        med_path_ind_displacement : float
            median displacement for all events in units of path indices.
        delta_path_ind_displacement : float
            tolerance of detection window for all events. Minimal value is
            `self.window_size`
        """
        frames = self.dataset["frame"][:]

        # find frames with more than 1 element/duplicates in frameNrs
        seen = {}
        duplicates = []
        for i in frames:
            if i not in seen:
                seen[i] = 1
            else:
                if seen[i] == 1:
                    duplicates.append(i)
                seen[i] += 1
        # create list of frame numbers where only one object is in channel
        frames_uniq = [value for value in frames if value not in duplicates]
        # Index of unique objects
        uniq_idx = [np.where(np.array(frames) == value)[0]
                    for value in frames_uniq]

        # get path positions from single cell events to calculate median
        # displacement in units of path indices
        path_pos_uniq = np.array(self.dataset['path_index'][:],
                                 dtype=object)[uniq_idx]
        # indexing results in weird array shape, so do some reshaping
        path_pos_uniq = path_pos_uniq[:, 0]

        path_pos_diff = np.diff(path_pos_uniq)
        # throw out all negative values --> here a new object appeared
        # only take displacement of consecutive events
        frame_diff = np.diff(frames_uniq)[path_pos_diff > 0]
        path_pos_diff = path_pos_diff[path_pos_diff > 0]
        path_pos_diff = path_pos_diff[frame_diff == 1]
        med_path_ind_displacement = np.median(path_pos_diff)
        std_path_ind_displacement = np.std(path_pos_diff)

        # cut off outliers in data for more reliable value
        # data = data within 2*sigma intervall
        path_pos_diff = path_pos_diff[
            path_pos_diff >= med_path_ind_displacement
            - std_path_ind_displacement]
        path_pos_diff = path_pos_diff[
            path_pos_diff <= med_path_ind_displacement
            + std_path_ind_displacement]
        med_path_ind_displacement = np.median(path_pos_diff)

        # Uncertanty for the median displacement of the objects
        delta_path_ind_displacement = self.pred_zone_size * np.std(
            path_pos_diff)
        if delta_path_ind_displacement < self.window_size:
            delta_path_ind_displacement = self.window_size

        return med_path_ind_displacement, delta_path_ind_displacement

    def find_objects_and_pred_zones(self):
        """
        Here happens the actual tracking.
        This function assigns an object number to the events detected.
        Object index and prediction zones are added as temporary features to
        the rtdc dataset. It assigns to an event a prediction zone determined
        from an object in the previous frame, meaning:
        The prediction zone of event x is the zone in which this event x was
        predicted to be based on previous events, it is not the zone in which
        the object will be next.
        `self.dataset` needs to contain the information about the
        path_index already, so `self.get_channel_path()` has to be run before.
        """
        # median path displacement is in units of channel path points per frame
        (med_path_displacement,
         Delta_path_displacement) = self.get_median_displacement()
        # pos_x[i] + (med_x_displacement +/- Delta_x_displacement) creates a
        # zone where the obect from frame i is expected to be in frame i+1

        frames = np.array(self.dataset['frame'][:])
        index = np.array(self.dataset['index'][:])
        path_index = np.array(self.dataset['path_index'][:])

        # list that contains the frame number only once
        # set messes up the list for some reason --> sort
        frames_uniq = np.sort(list(set(frames)))

        # create lists that contain all entries in each frame for pos_x and
        # event_index
        path_indices = []
        event_indices = []
        for frame in frames_uniq:
            ind = np.nonzero(frames == frame)
            path_indices.append(path_index[ind])
            event_indices.append(index[ind])

        # create list that contains objects that should be kept track of
        tracked_objects = []
        # another list to handle new objects found during one iteration over
        # events in a frame
        new_tracked_objects = []

        print("Tracking cells")
        stop_tracking = []
        # iterate over every frame
        for frame_ind in trange(len(frames_uniq)):
            frame = frames_uniq[frame_ind]

            # update current list of tracked objects
            for obj_rmv in stop_tracking:
                tracked_objects.remove(obj_rmv)
            for obj_new in new_tracked_objects:
                tracked_objects.append(obj_new)
            tracked_objects.sort()
            tr_objcts_frame = tracked_objects.copy()
            stop_tracking = []
            new_tracked_objects = []

            # iterate over the events in the frame
            # Take into account the path_index and sort the events according
            # to it from big to small;
            # then start with the event with the largest
            # path_index and check if it fits to one of the tracked objects.
            # If yes, one should ignore that object from the list
            # tracked_objects for the remainder of the tracking for that
            # frame, because each object can only occur once in a frame, and
            # we don't want the same object_number to be assigned twice.
            # As long as the events are ordered by path_index and the
            # tracked objects by value, the chances for mixing up events
            # should be very small.
            sorted_indices = np.argsort(path_indices[frame_ind])[::-1]
            e_indices_sorted = [
                event_indices[frame_ind][a] for a in sorted_indices]
            p_indices_sorted = [
                path_indices[frame_ind][a] for a in sorted_indices]
            for i in range(len(e_indices_sorted)):

                # get event and path indices based on sorted path_index
                e_ind = e_indices_sorted[i]
                p_index = p_indices_sorted[i]

                found = False

                for tr_obj in tr_objcts_frame:
                    # object entries in previous frame
                    obj_idx_ds = np.nonzero(self.dataset['object_number']
                                            == tr_obj)[0]

                    if obj_idx_ds.size == 0:
                        # this should actually not happen, because all object
                        # numbers that appear in the list `tracked_object` were
                        # also assigned to an event.
                        raise ObjectNumberError("An object was about to be "
                                                "tracked that was not found "
                                                "before! Check how that object"
                                                " number got into the list "
                                                "`tracked_objects`.")

                    # if object was not found for >max_skip_frames: add to
                    # stop_tracking and continue
                    prev_frame = self.dataset['frame'][obj_idx_ds][-1]
                    if (frame - prev_frame) > self.frames_disappear:
                        stop_tracking.append(tr_obj)
                        continue

                    prev_p_index = self.dataset['path_index'][obj_idx_ds][-1]

                    # predZone starts at prev position-1 to ensure
                    # that cells that got stuck at constriction get
                    # still assigned correctly
                    pred_zone_start = prev_p_index - 1
                    if pred_zone_start < 0:
                        pred_zone_start = 0

                    # determine end of prediction zone
                    pred_zone_end = (prev_p_index
                                     + (frame - prev_frame)
                                     * med_path_displacement
                                     + Delta_path_displacement)
                    # learn median displacement from cell displacement so
                    # far if more than 5 events were tracked for the object
                    if obj_idx_ds.size > 5:
                        # get object path so far
                        obj_path = self.dataset["path_index"][obj_idx_ds]
                        obj_frames = self.dataset["frame"][obj_idx_ds]

                        if len(obj_frames) == len(np.unique(obj_frames)):
                            displacement = (np.diff(obj_path)
                                            // np.diff(obj_frames))
                            # For med_displacement don't take into account
                            # if cell got stuck at a constriction:
                            # displacement mustbe larger than 1!
                            displacement = displacement[displacement > 1]
                            if len(displacement) > 5:
                                med_displacement = np.median(displacement)

                                pred_zone_end = (prev_p_index
                                                 + (frame - prev_frame)
                                                 * med_displacement
                                                 + Delta_path_displacement)

                    # check if prediction zone is outside the channel
                    # stop tracking if object didn't show for certain number
                    # of consecutive frames (`self.frames_disappear`)
                    if ((pred_zone_start + med_path_displacement)
                            > max(path_index)
                            or (frame - prev_frame) > self.frames_disappear):
                        stop_tracking.append(tr_obj)

                    if pred_zone_start <= p_index < pred_zone_end:
                        # ind - 1 because dclab starts counting the event
                        # index feature at 1, while python enumerates
                        # objects starting at 0
                        obj_data = self.dataset["object_number"][:].copy()
                        obj_data[e_ind - 1] = tr_obj
                        dclab.set_temporary_feature(self.dataset,
                                                    feature="object_number",
                                                    data=obj_data)

                        pred_zone_data = self.dataset[
                                             "prediction_zone_start"][:].copy()
                        pred_zone_data[e_ind - 1] = pred_zone_start
                        dclab.set_temporary_feature(
                            self.dataset, feature="prediction_zone_start",
                            data=pred_zone_data)

                        pred_zone_data = self.dataset[
                                             "prediction_zone_end"][:].copy()
                        pred_zone_data[e_ind - 1] = pred_zone_end
                        dclab.set_temporary_feature(
                            self.dataset, feature="prediction_zone_end",
                            data=pred_zone_data)

                        found = True
                        # we do not check whether another tracked object fits
                        # to that event (causes problems when several cells
                        # are close and their pred_zone overlap)
                        # and we do not check for that object anymore, it was
                        # already found
                        tr_objcts_frame.remove(tr_obj)
                        break

                if not found:
                    new_obj_nr = np.nanmax(
                        self.dataset['object_number'][:]) + 1
                    if np.isnan(new_obj_nr):
                        new_obj_nr = 1
                    new_tracked_objects.append(new_obj_nr)
                    object_numbers = self.dataset['object_number'][:].copy()
                    object_numbers[e_ind - 1] = new_obj_nr
                    dclab.set_temporary_feature(self.dataset,
                                                feature="object_number",
                                                data=object_numbers)

            # list might contain same element twice
            # --> removing procedure causes error
            # --> get rid of duplicates
            stop_tracking = list(set(stop_tracking))

    def get_good_events(self):
        """Filter out unwanted events from the dataset:
        - two objects in same prediction zone;
        - Object path not from beginning to end;
        - two objects in the channel at the same time

        This function creates an object called self.ind_good_events that can
        be used to filter the dataset for good events. It's an array with zeros
        for bad and ones for good events.
        """
        good_obj_nbs = []
        del_obj_nbs = []
        list_of_objcts = np.unique(self.dataset["object_number"][:])
        for obj_nr in list_of_objcts:
            obj_ind = np.nonzero(self.dataset["object_number"][:] == obj_nr)[0]
            obj_frames = self.dataset["frame"][obj_ind]
            list_of_double_frames = [fr for fr in obj_frames if
                                     list(self.dataset["frame"][:]).count(fr)
                                     > 1]

            # same object detected twice on the same frame
            if len(obj_frames) > len(np.unique(obj_frames)):
                del_obj_nbs.append(obj_nr)

            # measured cells must make it from channel entrance to exit
            elif ((self.dataset['path_index'][obj_ind[0]] >= 2)
                  or (self.dataset['path_index'][obj_ind[-1]] <=
                      ROW_GRID_POINTS * MMM_ROWS - 3)):
                del_obj_nbs.append(obj_nr)

            # only one object in the channel at a time
            elif list_of_double_frames:
                del_obj_nbs.append(obj_nr)
            else:
                good_obj_nbs.append(obj_nr)

        bad_events = []
        for del_obj_nb in set(del_obj_nbs):
            obj_ind = np.nonzero(
                self.dataset['object_number'][:] == del_obj_nb)[0]
            bad_events.extend(list(obj_ind))

        # create an array with true or false for each event saying if it's
        # a "good" event or not
        good_events = []
        for i in range(len(self.dataset)):
            if i in bad_events:
                good_events.extend([0])
            else:
                good_events.extend([1])
        self.ind_good_events = good_events
        self.good_objects = good_obj_nbs

    def assign_cell_params(self):
        """
        Determine area and deformation of a tracked object before and after
        transit through the channel and store that information in the dataset.
        """
        arr_area_before = np.full(len(self.dataset), np.nan)
        arr_area_after = np.full(len(self.dataset), np.nan)
        arr_deform_before = np.full(len(self.dataset), np.nan)
        arr_deform_after = np.full(len(self.dataset), np.nan)
        list_of_objcts = np.unique(self.dataset["object_number"][:])
        for obj_nr in list_of_objcts:
            obj_ind = np.nonzero(self.dataset["object_number"][:] == obj_nr)[0]

            # take the first occurance of the object and check if that is
            # located before the channel and that porosity is ok.
            first_path_pos = self.dataset["path_index"][obj_ind][0]
            # get porosity value to check that event was detected well
            por = float(self.dataset['area_ratio'][obj_ind][0])

            if first_path_pos < 2 and por <= 1.1:
                arr_area_before[obj_ind] = float(
                    self.dataset['area_um'][obj_ind][0])
                arr_deform_before[obj_ind] = float(
                    self.dataset['deform'][obj_ind][0])

            # similar for area/deform after channel:
            # check the last occurance of the object, check if it lies at the
            # end of the channel and check porosity
            last_path_pos = self.dataset['path_index'][obj_ind][-1]
            por = float(self.dataset['area_ratio'][obj_ind][-1])

            if last_path_pos > ROW_GRID_POINTS * MMM_ROWS - 3 and por <= 1.1:
                arr_area_after[obj_ind] = float(
                    self.dataset['area_um'][obj_ind][-1])
                arr_deform_after[obj_ind] = float(
                    self.dataset['deform'][obj_ind][-1])

        dclab.set_temporary_feature(self.dataset,
                                    "obj_area_before",
                                    arr_area_before)
        dclab.set_temporary_feature(self.dataset,
                                    "obj_area_after",
                                    arr_area_after)
        dclab.set_temporary_feature(self.dataset,
                                    "obj_deform_before",
                                    arr_deform_before)
        dclab.set_temporary_feature(self.dataset,
                                    "obj_deform_after",
                                    arr_deform_after)

    def row_velocity(self):
        """Calculate the velocity a cell travels per row.
        Units are mm/s."""
        list_of_objcts = np.unique(self.dataset["object_number"][:])
        for obj_nr in list_of_objcts:
            self.velocities.update({obj_nr: {}})
            obj_ind = np.nonzero(self.dataset["object_number"][:] == obj_nr)[0]
            path_index = self.dataset['path_index'][obj_ind]

            # find the indices for each row
            # each row has exactly ROW_GRID_POINTS indices
            # Exclude the points at the border of each row because the
            # calculation in the curves is inaccurate
            ind_row1 = path_index < ROW_GRID_POINTS - 1
            ind_row2 = (path_index >= 1 * ROW_GRID_POINTS + 1) & (
                    path_index < 2 * ROW_GRID_POINTS - 1)
            ind_row3 = (path_index >= 2 * ROW_GRID_POINTS + 1) & (
                    path_index < 3 * ROW_GRID_POINTS - 1)
            ind_row4 = (path_index >= 3 * ROW_GRID_POINTS + 1) & (
                    path_index < 4 * ROW_GRID_POINTS - 1)
            ind_row5 = (path_index >= 4 * ROW_GRID_POINTS + 1) & (
                    path_index < 5 * ROW_GRID_POINTS - 1)
            ind_row6 = (path_index >= 5 * ROW_GRID_POINTS + 1) & (
                    path_index < 6 * ROW_GRID_POINTS - 1)
            ind_row7 = (path_index >= 6 * ROW_GRID_POINTS + 1) & (
                    path_index < 7 * ROW_GRID_POINTS - 1)
            ind_row8 = (path_index >= 7 * ROW_GRID_POINTS + 1) & (
                    path_index < 8 * ROW_GRID_POINTS - 1)
            ind_row9 = (path_index >= 8 * ROW_GRID_POINTS + 1) & (
                    path_index < 9 * ROW_GRID_POINTS - 1)
            ind_row10 = (path_index >= 9 * ROW_GRID_POINTS + 1) & (
                    path_index < 10 * ROW_GRID_POINTS - 1)
            ind_row11 = path_index >= 10 * ROW_GRID_POINTS + 1

            row_inds = [ind_row1, ind_row2, ind_row3, ind_row4, ind_row5,
                        ind_row6, ind_row7, ind_row8, ind_row9, ind_row10,
                        ind_row11]

            for row_number, ind in enumerate(row_inds):
                pos_x = self.dataset['pos_x'][obj_ind][ind]
                frames = self.dataset['frame'][obj_ind][ind]

                # calculate velocity based on distance and time differences
                # between first and last occurance of an object in that row
                # in units of mm/s
                try:
                    velocity = abs(
                        ((pos_x[-1] - pos_x[0]) / 1000.) /
                        ((frames[-1] - frames[0]) / float(self.fps)))
                except (IndexError, ValueError):
                    velocity = np.nan

                row_key = 'v_row' + str(row_number + 1)
                vel = {row_key: velocity}
                self.velocities[obj_nr].update(vel)

    def export_tracking_results(self, filtered=True):
        """Save data of transit times, velocities, area before/after the
         channel, deformation before/after the channel of each cell to a csv
         file.
         The units of transit times are seconds, the units of the velocities
         are mm/s, the units of area are um^2.

        Parameters
        ----------
        filtered: bool
            If True, only the filtered data with events that were tracked
            from the channel entrance to the channel outlet will be exported.
            If False, all data will be exported.

        Notes
        -----
        The values for mean, median and std of transit times/velocities
        are computed over the list of average transit times/velocities, so
        it's a mean of means, median of means, std of means...
        """
        print("Starting to export data to .csv file.")
        if not self.good_objects and filtered:
            print("No filtered data available to export. Did you call "
                  "'get_good_events()' already?\nWill export unfiltered "
                  "data now.")
            filtered = False

        save_keys = ["index", "cell number",
                     "first vidframe", "last vidframe",
                     "transit time [s]", "avg velocity [mm/s]",
                     "area before [um^2]", "area after [um^2]",
                     "deform before [a.u.]", "deform after [a.u.]"]

        for row_ind in np.arange(1, MMM_ROWS + 1):
            row_key = "v_row" + str(row_ind) + " [mm/s]"
            save_keys.append(row_key)

        header = ",".join(save_keys)

        if filtered:
            list_of_objects = self.good_objects
        else:
            list_of_objects = np.unique(self.dataset["object_number"][:])
        index_nr = np.arange(1, len(list_of_objects) + 1)
        cell_nr = []
        first_vidframe = []
        last_vidframe = []
        transit_time = []
        avg_velocity = []

        area_before = []
        area_after = []
        deform_before = []
        deform_after = []

        row_velocity_dict = {}
        for row_ind in np.arange(1, MMM_ROWS + 1):
            row_key = "v_row" + str(row_ind)
            row_velocity_dict[row_key] = []

        for obj_nr in list_of_objects:
            cell_nr.append(obj_nr)
            object_nbs = np.array(self.dataset["object_number"][:])
            obj_ind = np.nonzero(object_nbs == obj_nr)[0]
            first_vidframe.append(self.dataset['frame'][obj_ind][0])
            last_vidframe.append(self.dataset['frame'][obj_ind][-1])
            if len(obj_ind) > 1:
                # need at least 2 events per object to calculate any velocities
                first_pos = self.dataset['pos_x'][obj_ind][0]
                last_pos = self.dataset['pos_x'][obj_ind][-1]

                # distance in um of first event from path_index 1
                dist_inlet = first_pos - self.channel_path[1][
                    0] * self.pixel_size
                # avg. speed between first two events in um/s
                vel_inlet = abs(
                    first_pos - self.dataset['pos_x'][obj_ind][1]) / \
                    float(self.dataset['time'][obj_ind][1] -
                          self.dataset['time'][obj_ind][0])

                # distance in um of last event from second to last path_index
                dist_outlet = last_pos - self.channel_path[
                    ROW_GRID_POINTS*MMM_ROWS - 2][0] * self.pixel_size
                # avg. speed between last two events in um/s
                vel_outlet = abs(
                    last_pos - self.dataset['pos_x'][obj_ind][-2]) / \
                    float(self.dataset['time'][obj_ind][-1] -
                          self.dataset['time'][obj_ind][-2])

                # time in s
                time = (last_vidframe[-1] - first_vidframe[-1]) / float(
                    self.fps)
                time_corrected = time - dist_inlet / vel_inlet \
                    + dist_outlet / vel_outlet
                transit_time.append(time_corrected)

                # path length through channel in mm
                path = self.channel_path[1:ROW_GRID_POINTS*MMM_ROWS-2]
                path_len = path_length(path) * self.pixel_size / 1000.

                avg_velocity.append(path_len / time_corrected)

                area_before.append(self.dataset['obj_area_before'][obj_ind][0])
                area_after.append(self.dataset['obj_area_after'][obj_ind][-1])
                deform_before.append(
                    self.dataset['obj_deform_before'][obj_ind][0])
                deform_after.append(
                    self.dataset['obj_deform_after'][obj_ind][-1])

                for row_ind in np.arange(1, MMM_ROWS + 1):
                    row_key = "v_row" + str(row_ind)
                    row_velocity_dict[row_key].append(
                        self.velocities[obj_nr][row_key])
            else:
                transit_time.append(np.nan)
                avg_velocity.append(np.nan)
                area_before.append(np.nan)
                area_after.append(np.nan)
                deform_before.append(np.nan)
                deform_after.append(np.nan)
                for row_ind in np.arange(1, MMM_ROWS + 1):
                    row_key = "v_row" + str(row_ind)
                    row_velocity_dict[row_key].append(np.nan)

        sort_ind = np.argsort(cell_nr)

        cell_nr = np.array(cell_nr)[sort_ind]
        first_vidframe = np.array(first_vidframe)[sort_ind]
        last_vidframe = np.array(last_vidframe)[sort_ind]
        transit_time = np.array(transit_time)[sort_ind]
        avg_velocity = np.array(avg_velocity)[sort_ind]

        area_before = np.array(area_before)[sort_ind]
        area_after = np.array(area_after)[sort_ind]
        deform_before = np.array(deform_before)[sort_ind]
        deform_after = np.array(deform_after)[sort_ind]

        data = np.vstack((index_nr, cell_nr, first_vidframe, last_vidframe,
                          transit_time, avg_velocity,
                          area_before, area_after,
                          deform_before, deform_after)).T

        for row_ind in np.arange(1, MMM_ROWS + 1):
            row_key = "v_row" + str(row_ind)
            row_data = np.array(row_velocity_dict[row_key])
            data = np.c_[data, row_data]
        if filtered:
            output_file = str(
                self.output_dir / "tracked_MMM_data_filtered.csv")
        else:
            output_file = str(self.output_dir / "tracked_MMM_data.csv")
        with open(output_file, "w") as tfile:
            # write header
            tfile.write(header + "\n")
            np.savetxt(tfile, data[0:, :], delimiter=",", fmt='%.8f')
        print("Finished exporting data to .csv file.")

    def export_path_img(self):
        """Export an image of the channel with the channel path."""
        cv2.imwrite(str(self.output_dir / "channel_img_with_path.png"),
                    self.img_with_path)

    def export_tracked_dataset_as_hdf5(self, path=None,
                                       features=None, **kwargs):
        """Calls the function `export_tracked_dataset_as_hdf5` in the io
        module, which is basically a wrapper function for the normal dclab
        hdf5 export.

        See `dctrack.io.export_tracked_dataset_as_hdf5` for parameter
        details.

        See Also
        --------
        dctrack.io.export_tracked_dataset_as_hdf5
        dclab.rtdc_dataset.export.hdf5
        """
        if path is None:
            path = self.output_dir / "tracked_mmm_data.rtdc"
        io.export_tracked_dataset_as_hdf5(
            self, path=path, mode="mmm", features=features, **kwargs)
        print("Finished exporting dataset to RTDC file.")

    def export_video(self, vid_path=None, first_frame=0,
                     last_frame=0, draw_zones=True, filtered=True):
        """
        Write video + corresponding prediction zones as overlay.

        Parameters
        ----------
        vid_path: str or pathlib.Path
            Path to directory to export the video file to. If not given, the
            output directory is used which is per default the same directory
            as the input file.
        first_frame: int
            First frame in video file to analyze. Default is first_frame = 0.
        last_frame: int
            Last frame in video file to analyze. If LastFrame = 0 (default) all
            video frames will be analyzed.
        draw_zones: bool
            If True, the prediction zones will be drawn on the frames.
            If False, only the images and contours as in the .rtdc file will
            be exported to the video file.
        filtered: bool
            If True, only the filtered data with events that were tracked
            from the channel entrance to the channel outlet will be exported.
            If False, all data will be exported.
        """
        if vid_path is None:
            vid_path = self.output_dir
            print("Save exported video to output diretory.")
        if isinstance(vid_path, str) or isinstance(vid_path, pathlib.Path):
            vid_path = pathlib.Path(vid_path)
        else:
            raise ValueError(f"The output path for the video file must be "
                             f"either of type `string` or `pathlib.Path`, "
                             f"got {type(vid_path)} instead")
        if vid_path.is_dir():
            print(f"Save exported video here:\n{str(vid_path)}")
        else:
            vid_path.mkdir(parents=True)

        if not self.good_objects and filtered:
            print("No filtered data available to export. Did you call "
                  "'get_good_events()' already?\nWill export unfiltered "
                  "data now.")
            filtered = False

        output_path = str(vid_path / "tracked_data")
        if draw_zones:
            output_path = output_path + "_with_predzones"
        if filtered:
            output_path = output_path + "_filtered"
        output_path = output_path + ".avi"
        vidwriter = get_writer(output_path, fps=60, quality=8)

        object_index = np.array(self.dataset['object_number'][:],
                                dtype=np.int32)
        pos_x = self.dataset['pos_x'][:]
        pos_y = self.dataset['pos_y'][:]
        pos_x_pixel = np.array(pos_x / self.pixel_size, dtype=np.float32)
        pos_y_pixel = np.array(pos_y / self.pixel_size, dtype=np.float32)

        if last_frame == 0 or last_frame > len(self.dataset['frame'][:]):
            last_frame = len(self.dataset['frame'][:])
        if first_frame < 0:
            first_frame = 0

        if draw_zones:
            zone_starts = np.array(self.dataset['prediction_zone_start'][:],
                                   dtype=np.int32)
            zone_ends = np.array(self.dataset['prediction_zone_end'][:],
                                 dtype=np.int32)

        # don't use duplicates of frames in video --> unique frames only
        frames = np.array(self.dataset['frame'][:])
        frames_unique, idx_frames_unique = np.unique(frames, return_index=True)
        # add offset from first_frame to unique indices
        idx_frames_unique += first_frame

        font = cv2.FONT_HERSHEY_SIMPLEX
        pbar = tqdm(idx_frames_unique[first_frame:last_frame],
                    desc="Exporting video")
        for frame_nr in pbar:
            image = self.dataset['image'][frame_nr]
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
            frame = self.dataset['frame'][frame_nr]

            # find index of all frames in `dataset`
            frame_ind = np.nonzero(self.dataset['frame'][:] == frame)[0]
            objects = object_index[frame_ind]
            pos_x_pix_frame = pos_x_pixel[frame_ind]
            pos_y_pix_frame = pos_y_pixel[frame_ind]
            # draw prediction zones on image copy, then overlay a
            # transparent version over the original image at the end
            if draw_zones:
                zone_starts_frame = zone_starts[frame_ind]
                zone_ends_frame = zone_ends[frame_ind]
                image_out = image.copy()

            contours = [self.dataset['contour'][i] for i in frame_ind]

            # write video to see how detection worked out
            if len(frame_ind) > 0:
                for cc in range(len(frame_ind)):
                    # get object index
                    obj_ind = objects[cc]

                    if (filtered and obj_ind in self.good_objects) or (
                            not filtered):
                        # get contour object in format suited for
                        # cv2.drawContours
                        cont_ii = [np.array(contours[cc], dtype=np.int32)]
                        # draw closed colored contour around object
                        color_ii = COLORS[obj_ind % len(COLORS)]
                        cv2.drawContours(image, cont_ii, -1,
                                         color_ii, -1)
                        # write object index above object
                        cv2.putText(image, str(obj_ind),
                                    (int(pos_x_pix_frame[cc]) - 5,
                                     int(pos_y_pix_frame[cc])),
                                    font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

                        if draw_zones:
                            pred_path = np.array(
                                self.channel_path[
                                    zone_starts_frame[cc]:zone_ends_frame[cc]],
                                dtype=np.int32)

                            if len(pred_path) > 0:
                                for ind in range(len(pred_path) - 1):
                                    cv2.line(image_out, tuple(pred_path[ind]),
                                             tuple(pred_path[ind + 1]),
                                             COLORS[obj_ind % len(COLORS)], 8)

                                # add text to original and overlay, otherway
                                # text would appear transparent in final image
                                cv2.putText(image_out, str(obj_ind),
                                            (int(pos_x_pix_frame[cc]) - 5,
                                             int(pos_y_pix_frame[cc])), font,
                                            0.5, (255, 255, 255), 1,
                                            cv2.LINE_AA)
            if draw_zones:
                # transparency of the pred zone overlay
                alpha = 0.3
                cv2.addWeighted(image_out, alpha, image, 1 - alpha, 0, image)

            # extend image with black pixels, so each dimension is dividable by
            # macro_block_size; needed for video codecs to work correctly
            image = extend_img(image, macro_block_size=16)

            vidwriter.append_data(image)

        vidwriter.close()
        print("Finished exporting video.")
