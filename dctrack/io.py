
import pathlib
import warnings
import h5py
import dclab
from dclab.rtdc_dataset.core import RTDCBase

from . import utils
from ._version import version


def load_data_for_tracking(data_input):
    """
    Load the measurement data to perform object tracking on and concert
    into a format that can be used by the tracking functions.

    Parameters:
        Currently supported:

            - .rtdc and .hdf5 files containing RT-DC datasets measured with
            ShapeIn or exported with dclab
            - RTDCBase: rtdc datasets created with dclab.new_dataset

    Returns:
        dclab dataset; RTDCBase object that can be created with
        dclab.new_dataset()
    """

    if isinstance(data_input, (str, pathlib.Path)):

        path = pathlib.Path(data_input).resolve()
        if not path.exists():
            raise FileNotFoundError(
                "Could not find file '{}'".format(path))

        if path.suffix in [".rtdc", ".hdf5"]:
            dataset = dclab.new_dataset(path)

    elif isinstance(data_input, RTDCBase):
        dataset = data_input
    else:
        msg = "data type not supported: {}".format(type(data_input))
        raise NotImplementedError(msg)

    # input data must contain the features needed for tracking
    for feat in utils.TRACKING_FEATURES:
        if feat not in dataset:
            raise ValueError("Input data does not contain necessary "
                             "feature for tracking: '{}'".format(feat))

    check_and_load_temporary_features()

    return dataset


def check_and_load_temporary_features():
    """Check if the temporary feature already exists and register

    Plugin features are not used because they don't allow for index assignment
    """
    for feat in utils.TRACK_TEMPORARY_FEATURES:
        if not dclab.definitions.feature_exists(feat):
            dclab.register_temporary_feature(feature=feat)


def update_software_version(ds_path: (str, pathlib.Path),
                            write_attribute: bool = True):
    """Perform version branding

    Append a " | dctrack X.Y.Z" to the "setup:software version"
    attribute.

    Parameters
    ----------
    ds_path: str
        The dataset whose metadata shall be updated
    write_attribute: bool
        If True (default), write the version string to the
        "setup:software version" attribute, else just return the updated
        string.
    """
    with h5py.File(ds_path, "r") as h5file:
        old_version = h5file.attrs.get("setup:software version", "")
    if isinstance(old_version, bytes):
        old_version = old_version.decode("utf-8")
    version_chain = [vv.strip() for vv in old_version.split("|")]
    version_chain = [vv for vv in version_chain if vv]
    cur_version = f"dctrack {version}"

    if version_chain:
        if version_chain[-1] != cur_version:
            version_chain.append(cur_version)
    else:
        version_chain = [cur_version]
    new_version = " | ".join(version_chain)
    if write_attribute:
        with h5py.File(ds_path, "a") as h5file:
            h5file.attrs["setup:software version"] = new_version
    else:
        return new_version


def export_tracked_dataset_as_hdf5(tr_ds, path, mode="default", features=None,
                                   **kwargs):
    """Wrapper function on the normal dclab hdf5 export.
    Will save the current version of dctrack to the `software version` string
    in the metadata section of the RTDC file.

    Parameters
    ----------
    tr_ds: TrackedDataSet or TrackedMMM
        The dataset to export.
    path: str or Pathlib.Path
        Path of the new file.
    mode: str
        `default` means the dataset is from a normal tracking experiment in
        either a
        - straight channel
        - hyperbolic channel
        - multi-constriction channel
        `mmm` means the dataset is from a MMM tracking experiment
    features: list
        A list of features to be exported. Note that necessary and temporary
        features will automatically be exported.
    **kwargs
        See dclab documentation for further keyword arguments for the function
        `ds.export.hdf5`
    """
    if mode == "default":
        necessary_features = (utils.TRACKING_FEATURES +
                              utils.TRACK_TEMPORARY_FEATURES)
    elif mode == "mmm":
        necessary_features = (utils.TRACKING_FEATURES +
                              utils.TRACK_MMM_TEMPORARY_FEATURES)
    else:
        raise NotImplementedError(f"The given tracking mode {mode} is not"
                                  f"recognized!")

    # export loaded scalar features if no feature list is provided
    if features is None:
        features_loaded = tr_ds.dataset.features_loaded
        features_scalar = tr_ds.dataset.features_scalar
        features = list(set(features_loaded) & set(features_scalar))

    # check for available tracking features
    available_features = []
    for feat in necessary_features:
        # only grab the temporary features which have been set
        if feat in tr_ds.dataset:
            available_features.append(feat)
    if len(available_features) == 0:
        warn_msg = "No temporary features will be exported!"
        warnings.warn(warn_msg, UserWarning)

    export_features = list(set(features +
                               available_features))
    tr_ds.dataset.export.hdf5(path=path, features=export_features, **kwargs)
    update_software_version(path, write_attribute=True)
