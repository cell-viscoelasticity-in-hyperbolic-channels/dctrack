import warnings
import re
import numpy as np
import time as tm
from tqdm.autonotebook import tqdm
import dclab
import dclab.definitions as dfn
from dclab.rtdc_dataset.feat_temp import deregister_temporary_feature

from .graph_selection import image_select_xmin_xmax
from . import utils
from .velocity_curve import (
    get_init_velocity_curve, fit_velocity,
    predict_displacement, check_flow_direction
)
from .video import video_write
from .channel_detection import find_channel_regions, plot_channel_regions_lines
from . import analysis_tools
from . import io
from . import track_tools


class DatasetNotTrackedError(BaseException):
    pass


class FluorescenceNotUsableError(BaseException):
    pass


class TrackedDataSet(object):
    def __init__(self, data_input):
        """ TrackedDataSet is used to track the movement of `unique_objects`,
        such as cell, moving through an RT-DC experiment.
        This works even with multi-constriction channels, as long as the first
        channel region is either inlet or channel.

        Parameters
        ----------
        data_input
           See `_load_data` for details.

        Notes
        -----
        Please use the `TrackedDataSet.export_tracked_dataset_as_hdf5` method
        when you want to export the `TrackedDataSet` to as a .hdf5 (.rtdc)
        file. This is equivalent to the `rtdc_ds.export.hdf5` function but
        handles all the necessary and temporary features needed by dctrack.

        Channel regions should start and end where an inflection point of the
        channel walls can be seen. For constrictions, this means the position
        where the funnel part begins, not the constriction itself.
        """
        self._data_input = data_input
        # load dataset when initiating object
        self.dataset = self._load_data()
        # set important attributes from rtdc config
        self.fps = self.dataset.config['imaging']['frame rate']
        self.pixel_size = self.dataset.config['imaging']['pixel size']
        self.roi_size_x = self.dataset.config['imaging']['roi size x']
        self.flow_rate = self.dataset.config['setup']['flow rate']
        self.already_tracked = False
        self.multiple_constrictions = False
        self.nb_channel_regions = 1
        self.flow_direction = "left"
        self.channel = None
        self.inlet = None
        self.outlet = None
        self.channel_regions = []
        self.channel_x_min = None
        self.channel_x_max = None
        self.tolerance_window = 25
        # some attributes about the chip geometry
        self.channel_height = self.dataset.config['setup']['channel width']
        self.channel_width = self.dataset.config['setup']['channel width']
        # attributes to filter object features
        self.filtered = False
        self.filter_obj_distance = True
        self.filter_obj_n_events = True
        self.filter_backward_movement = True
        self.filter_obj_start_to_end = True
        # attributes for checking the velocity fit
        self._init_velocity_frames = 10000  # frames to use to get init v-curve
        self._init_velocity_curve = None
        self._velocity_fits = None
        # load and set some more variables
        self._set_channel_image()
        self._check_dclab_metadata()
        self._set_user_tracking_metadata_as_attributes()

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        # close the HDF5 file
        self.dataset.h5file.close()

    def _load_data(self):
        """
        Load the measurement data to perform object tracking on and concert
        into a format that can be used by the tracking functions.

        Parameters:
            Currently supported:

                - .rtdc and .hdf5 files containing RT-DC datasets measured with
                ShapeIn or exported with dclab
                - RTDCBase: rtdc datasets created with dclab.new_dataset

        Returns:
            dclab dataset; RTDCBase object that can be created with
            dclab.new_dataset()
        """
        dataset = io.load_data_for_tracking(self._data_input)
        return dataset

    def _set_channel_image(self):
        """Set the initial image if it exists in the dataset. Uses median
        over the first nine images to remove objects that might interfere
        with the automatic channel detection."""
        if 'image' in self.dataset.features:
            self._channel_image = np.median(self.dataset["image"][0:9], axis=0)
        else:
            self._channel_image = None

    def _check_dclab_metadata(self):
        """Fill in the user section of the dclab config"""
        if "user" not in self.dataset.config:
            utils.create_user_config_section(self.dataset)
        utils.populate_user_config_section(self.dataset)

    def _set_user_tracking_metadata_as_attributes(self):
        """Create the tracking attributes from the dclab config user section"""
        self.set_tracking_metadata(self.dataset.config["user"])

    def set_channel(self, metadata_value):
        if not isinstance(metadata_value, (bool, np.bool_)):
            raise TypeError(f"Wrong datatype for metadata parameter 'channel'."
                            f" Expected type `bool`, got type "
                            f"{type(metadata_value)} instead.")
        self.channel = metadata_value
        self.dataset.config["user"]["channel"] = self.channel

    def set_inlet(self, metadata_value):
        if not isinstance(metadata_value, (bool, np.bool_)):
            raise TypeError(f"Wrong datatype for metadata parameter 'inlet'."
                            f" Expected type `bool`, got type "
                            f"{type(metadata_value)} instead.")
        self.inlet = metadata_value
        self.dataset.config["user"]["inlet"] = self.inlet

    def set_outlet(self, metadata_value):
        if not isinstance(metadata_value, (bool, np.bool_)):
            raise TypeError(f"Wrong datatype for metadata parameter 'outlet'."
                            f" Expected type `bool`, got type "
                            f"{type(metadata_value)} instead.")
        self.outlet = metadata_value
        self.dataset.config["user"]["outlet"] = self.outlet

    def set_multiple_constrictions(self, metadata_value):
        if not isinstance(metadata_value, (bool, np.bool_)):
            raise TypeError(f"Wrong datatype for metadata parameter 'multiple"
                            f"_constrictions'. Expected type `bool`, got type"
                            f" {type(metadata_value)} instead.")
        self.multiple_constrictions = metadata_value
        self.dataset.config[
            "user"]["multiple_constrictions"] = self.multiple_constrictions

    def set_nb_channel_regions(self, metadata_value):
        if not isinstance(metadata_value, (int, np.integer)):
            raise TypeError(f"Wrong datatype for metadata parameter 'nb_"
                            f"channel_regions'. Expected type `int`, got type "
                            f"{type(metadata_value)} instead.")
        self.nb_channel_regions = metadata_value
        self.dataset.config["user"]["nb_channel_regions"] = \
            self.nb_channel_regions

    def set_channel_regions(self, metadata_value):
        """Unit is pixels."""
        if not isinstance(metadata_value, list):
            raise TypeError(f"Wrong datatype for metadata parameter "
                            f"'channel_regions`. Expected type `list`, got "
                            f"type {type(metadata_value)} instead.")
        self.channel_regions = metadata_value
        self.dataset.config["user"]["channel_regions"] = self.channel_regions

    def set_channel_x_min(self, metadata_value):
        """not the beginning of the channel in terms of flow, but the border
        of it that is at the smallest x value. Unit is pixels."""
        if not isinstance(
                metadata_value, (int, float, np.integer, np.floating)):
            raise TypeError(f"Wrong datatype for metadata parameter 'channel"
                            f"_x_min'. Expected type `int` or `float`, got"
                            f" type {type(metadata_value)} instead.")
        self.channel_x_min = metadata_value
        self.dataset.config["user"]["channel_x_min"] = self.channel_x_min

    def set_channel_x_max(self, metadata_value):
        """not the end of the channel in terms of flow, but the border of it
        that is at the largest x value. Unit is pixels."""
        if not isinstance(
                metadata_value, (int, float, np.integer, np.floating)):
            raise TypeError(f"Wrong datatype for metadata parameter 'channel"
                            f"_x_max'. Expected type `int` or `float`, got"
                            f" type {type(metadata_value)} instead.")
        self.channel_x_max = metadata_value
        self.dataset.config["user"]["channel_x_max"] = self.channel_x_max

    def set_flow_direction(self, metadata_value):
        if not isinstance(metadata_value, str):
            raise TypeError(f"Wrong datatype for metadata parameter 'flow"
                            f"_direction'. Expected type `string`, got type "
                            f"{type(metadata_value)} instead.")
        self.flow_direction = metadata_value
        self.dataset.config["user"]["flow_direction"] = self.flow_direction

    def set_tolerance_window(self, metadata_value):
        # while tracking, how much can the real position deviate from the
        # predicted one in um
        if not isinstance(metadata_value,
                          (int, float, np.integer, np.floating)):
            raise TypeError(f"Wrong datatype for metadata parameter 'tolerance"
                            f"_window'. Expected type `int` or `float`, got"
                            f" type {type(metadata_value)} instead.")
        self.tolerance_window = metadata_value
        self.dataset.config["user"]["tolerance_window"] = self.tolerance_window

    def _set_metadata_by_key_value(self, key, value):
        """Internal method for setting the metadata by key and value"""
        if key == "channel":
            self.set_channel(value)
        elif key == "inlet":
            self.set_inlet(value)
        elif key == "outlet":
            self.set_outlet(value)
        elif key == "multiple_constrictions":
            self.set_multiple_constrictions(value)
        elif key == "nb_channel_regions":
            self.set_nb_channel_regions(value)
        elif key == "n_constrictions":
            # n_constrictions is needed for backwards compatability
            if not value == "auto":
                self.set_nb_channel_regions(value)
        elif key == "channel_regions":
            if not isinstance(value, list):
                # case when rtdc file was loaded
                value = list(map(list, value))
            self.set_channel_regions(value)
        elif key == "channel_x_min":
            self.set_channel_x_min(value)
        elif key == "channel_x_max":
            self.set_channel_x_max(value)
        elif key == "flow_direction":
            self.set_flow_direction(value)
        elif key == "tolerance_window":
            self.set_tolerance_window(value)
        else:
            # ignore other metadata
            warnings.warn(f"Unknown key '{key}' encountered! If you want to "
                          f"add metadata unrelated to tracking, please use "
                          f"dclab directly.")

    def set_tracking_metadata(self, metadata):
        """Set the metadata as a dictionary

        Parameters
        ----------
        metadata : dict
            The tracking metadata which can be set by the user. One or more
            metadata can be set at once. See notes.

        Notes
        -----
        The `metadata` given must be one of the dclab user config section
        keys. Options are: "channel", "inlet", "outlet",
        "multiple_constrictions", "nb_channel_regions", "channel_regions",
        "channel_x_min", "channel_x_max", "flow_direction", "tolerance_window".
        Each of these have their own set methods.
        For "channel_regions" we recommend using `set_channel_values_auto`.

        """
        if not isinstance(metadata,
                          (dict, dclab.rtdc_dataset.config.ConfigurationDict)):
            raise TypeError("`metadata` input must be a dictionary,"
                            f"got {type(metadata)} instead.")
        for key in metadata:
            self._set_metadata_by_key_value(key, metadata[key])

    def print_tracking_metadata(self):
        """Print out the tracking metadata values"""
        return self.dataset.config["user"]

    def plot_channel_regions(self, auto_close=False):
        """Plot the `channel_regions` metadata values as red lines into an
        image of the channel.

        Parameters
        ----------
        auto_close : bool
            If set to True, the figure will not display. Used for tests.

        See Also
        --------
        dctrack.channel_detection.plot_channel_regions_lines
        """
        plot_channel_regions_lines(self._channel_image, self.channel_regions,
                                   auto_close=auto_close)

    def set_channel_values_manual(self, *, x_min=None,
                                  x_max=None, channel_regions=None):
        """Manually set the min and max channel values and optionally the
        channel_regions. If channel regions are given, `channel_min` and
        `channel_max` are determined based on them and the flow direction.
        Unit is pixels.

        Parameters
        ----------
        x_min : number, optional
            The smaller number of the two x_positions that define the channel
            boundaries. This is not necesarily the start of the channel.
        x_max : number, optional
            The bigger number of the two x_positions that define the channel
            boundaries. This is not necessarily the end of the channel.
        channel_regions : list of lists of numbers
            Each list has length two and values should be sorted.
            Example: [[10, 25], [25, 78], [78, 98],  ... ]
        """
        # Consider chip geometry when updating channel values manually
        if channel_regions is not None:
            assert isinstance(channel_regions, list)
            assert isinstance(channel_regions[0], list)
            self.set_channel_regions(channel_regions)
            if self.flow_direction == "left":
                if self.inlet and self.outlet:
                    self.set_channel_x_min(channel_regions[0][1])
                    self.set_channel_x_max(channel_regions[-1][0])
                if self.inlet and not self.outlet:
                    self.set_channel_x_min(channel_regions[0][0])
                    self.set_channel_x_max(channel_regions[-1][0])
                if not self.inlet and self.outlet:
                    self.set_channel_x_min(channel_regions[0][1])
                    self.set_channel_x_max(channel_regions[-1][1])
                if not self.inlet and not self.outlet:
                    self.set_channel_x_min(channel_regions[0][0])
                    self.set_channel_x_max(channel_regions[-1][1])
            if self.flow_direction == "right":
                if self.inlet and self.outlet:
                    self.set_channel_x_min(channel_regions[0][1])
                    self.set_channel_x_max(channel_regions[-1][0])
                if self.inlet and not self.outlet:
                    self.set_channel_x_min(channel_regions[0][1])
                    self.set_channel_x_max(channel_regions[-1][1])
                if not self.inlet and self.outlet:
                    self.set_channel_x_min(channel_regions[0][0])
                    self.set_channel_x_max(channel_regions[-1][0])
                if not self.inlet and not self.outlet:
                    self.set_channel_x_min(channel_regions[0][0])
                    self.set_channel_x_max(channel_regions[-1][1])
        else:
            assert isinstance(x_min, (int, float))
            assert isinstance(x_max, (int, float))
            self.set_channel_x_min(x_min)
            self.set_channel_x_max(x_max)
            self.set_channel_regions([[x_min, x_max]])
            if self.multiple_constrictions:
                raise UserWarning(
                    "`multiple_constriction` is `True`, `channel_regions` "
                    "was overwritten with just two values! Please check if "
                    "that is correct!")

    def set_channel_values_gui(self, gui, auto_close=False):
        """
        Open GUI window to select the channel region directly on the image from
        the dataset. Calls function `set_channel_values_manual` to save them.

        Parameters
        ----------
        gui
            QApplication from TrackingSession for handling the GUI. Has to be
            instantiated via `main.TrackingSession()`.
        auto_close : bool
            If set to True, the figure will be closed automatically.
            Used for tests.
        """
        if self.multiple_constrictions:
            if self.nb_channel_regions <= 0:
                raise ValueError(
                    f"Current mode: Multiple constriction measurement. "
                    f"But the number of constrictions seems to be "
                    f"invalid: {self.nb_channel_regions}")
            regions = []
            for i in range(self.nb_channel_regions):
                x, y = image_select_xmin_xmax(
                    gui, self._channel_image, auto_close=auto_close)
                regions.extend([[int(x), int(y)]])
            regions = sorted(regions)
            self.set_channel_values_manual(channel_regions=regions)
        else:
            x_min, x_max = image_select_xmin_xmax(
                gui, self._channel_image, auto_close=auto_close)
            self.set_channel_values_manual(x_min=x_min, x_max=x_max)

    def set_channel_values_auto(self):
        """Automatically detects the `channel_regions` via the module
        `channel_detection`. Uses the function `set_channel_values_manual`
        to save them. Unit is pixels.

        See also
        --------
        dctrack.channel_detection.find_channel_regions
        """
        channel_regions = find_channel_regions(
            img=self._channel_image, inlet=self.inlet,
            channel=self.channel, outlet=self.outlet,
            multiple_constrictions=self.multiple_constrictions,
            n=self.nb_channel_regions)
        self.set_channel_values_manual(
            x_min=self.channel_x_min, x_max=self.channel_x_max,
            channel_regions=channel_regions)

    def get_flow_direction(self):
        """Return integer value representing the flow direction."""
        if self.flow_direction == 'left':
            flow = -1
        elif self.flow_direction == 'right':
            flow = 1
        else:
            errmsg = ("Unknown value for flow_direction: {}. "
                      "Valid values are: ('left', 'right')".format(
                          self.flow_direction))
            raise ValueError(errmsg)
        return flow

    def filter_feature(self, feature, filter_min, filter_max):
        """Filter a feature via the dclab dataset config file

        Parameters
        ----------
        feature : str
            Name of the dclab feature. This can be a temporary feature.
        filter_min : number
            Minimum (starting) value of the feature to be filtered
        filter_max : number
            Maximum (ending) value of the feature to be filtered

        Returns
        -------
        filt_feature_data : 1d numpy array
            The filtered feature data

        Raises
        ------
        DatasetNotTrackedError
            If `track` has not yet been successfully called for the tracked
            dataset
        """
        if not self.already_tracked:
            raise DatasetNotTrackedError("`track()` has not been called yet.")

        self.dataset.config['filtering'][f'{feature} min'] = filter_min
        self.dataset.config['filtering'][f'{feature} max'] = filter_max
        self.dataset.apply_filter()

        filt_feature_data = self.dataset[f"{feature}"][self.dataset.filter.all]
        return filt_feature_data

    def update_temp_feature(self, feature: str, ids, value):
        """Update entries of a temporary feature with the same new value."""
        feat_data = self.dataset[feature][:].copy()
        feat_data[ids] = value
        dclab.set_temporary_feature(
            rtdc_ds=self.dataset,
            feature=feature,
            data=feat_data)

    def track(self, frame_min=None, frame_max=None, timeout=None,
              max_skip_frames=10):
        """Assign an object number to events belonging to the same object
        flowing through the geometry. Function checks for events in one frame
        and compares if these already existed in the previous frames.
        It assigns to an event a prediction zone determined from an object in
        the previous frame, meaning: The prediction zone of event x is the
        zone in which this event x was predicted to be based on previous
        events, it is not the zone in which the object will be next.

        Parameters
        ----------
        frame_min : int
            First frame to start tracking
        frame_max : int
            Last frame -> stop tracking at this frame
        timeout : int, float
            Maximum execution time of the tracking in seconds. After the
            tracking exceeds timeout, the tracking function will stop and the
            results up to this point will be saved.
        max_skip_frames : int
            Maximum number of frames an event is missing between frames before
            it is dropped for further tracking
        """
        # channel_regions is needed for tracking
        if not self.channel_regions:
            raise ValueError("You need to set the `channel_regions`")
        self._create_object_prediction_features()
        # fit the velocity curves over the channel that is later used to
        # estimate the displacement of an object
        f_dir = self.flow_direction
        x_init, v_init = get_init_velocity_curve(
            self.dataset, self.flow_rate, self.channel_width,
            self.channel_height, flow_direction=f_dir,
            last_frame=self._init_velocity_frames
        )
        self._init_velocity_curve = np.vstack((x_init, v_init)).T

        scaled_channel_regions = (self.channel_x_min * self.pixel_size,
                                  self.channel_x_max * self.pixel_size)
        velocity_fits = fit_velocity(
            x_init, v_init, scaled_channel_regions,
            self.roi_size_x * self.pixel_size,
            fit_inlet=self.inlet,
            fit_channel=self.channel,
            fit_outlet=self.outlet
        )
        self._velocity_fits = velocity_fits

        # Multiple events can occur in the same frame. For tracking we need to
        # iterate only once per frame
        frames_unique = np.unique(self.dataset['frame'][:])

        # list of all objects that are currently tracked
        tracked_objects = [0]
        # list of objects that should  not be tracked anymore and to remove
        # from `tracked_objects` in the next frame
        stop_tracking = []

        # measure time and break loop if time>timeout
        if timeout:
            time_0 = tm.time()

        frames_all = np.array(self.dataset['frame'][:])
        pbar = tqdm(frames_unique[frame_min:frame_max],
                    desc="Tracking in progress")
        for frame in pbar:
            # measure time and break loop if time>timeout
            if timeout:
                time_loop = tm.time() - time_0
                if time_loop > timeout:
                    print("Timeout reached. Stop execution of `track()`")
                    break

            # find all events belonging to this frame
            frame_ids = np.nonzero(frames_all == frame)[0]

            # remove objects where prediction zone left the channel
            for obj_rmv in stop_tracking:
                tracked_objects.remove(obj_rmv)
            stop_tracking = []
            new_objects = []

            # iterate over events in same frame
            for ids in frame_ids:
                # set to true when event can be assigned to currently
                # tracked object
                found = False
                pos_x = self.dataset['pos_x'][ids]
                time = self.dataset['time'][ids]

                for tr_obj in tracked_objects:
                    # object entries in previous frame
                    obj_idx_ds = np.nonzero(self.dataset['object_number']
                                            == tr_obj)[0]
                    if obj_idx_ds.size > 0:
                        # if object was not found for >max_skip_frames: add to
                        # stop_tracking and continue
                        frame_obj_prev = self.dataset['frame'][obj_idx_ds][-1]
                        if (frame - frame_obj_prev) > max_skip_frames:
                            stop_tracking.append(tr_obj)
                            continue

                        pos_x_obj_prev = self.dataset['pos_x'][obj_idx_ds][-1]
                        time_obj_prev = self.dataset['time'][obj_idx_ds][-1]
                        dt = time - time_obj_prev
                        # dt=0 means some error occurred to calculate dt
                        # probably the object was already found in an earlier
                        # iteration
                        if dt == 0:
                            continue

                        displacement = predict_displacement(
                            pos_x_obj_prev,
                            velocity_fits,
                            dt,
                            flow_direction=self.flow_direction)

                        # stop tracking the object if error in
                        # predict_displacement occurs -> object likely left the
                        # channel
                        if displacement is None:
                            stop_tracking.append(tr_obj)
                            continue

                        prediction_zone_start = (pos_x_obj_prev
                                                 + displacement
                                                 - self.tolerance_window)
                        prediction_zone_end = (pos_x_obj_prev
                                               + displacement
                                               + self.tolerance_window)

                        # check if prediction zone is outside the channel
                        # if so: stop tracking this object
                        if self.flow_direction == 'left':
                            if prediction_zone_end < 0:
                                stop_tracking.append(tr_obj)
                            elif (prediction_zone_start
                                  < pos_x
                                  < prediction_zone_end):
                                self.update_temp_feature("object_number",
                                                         ids, tr_obj)
                                self.update_temp_feature(
                                    "prediction_zone_start", ids,
                                    prediction_zone_start)
                                self.update_temp_feature(
                                    "prediction_zone_end", ids,
                                    prediction_zone_end)
                                found = True
                        elif self.flow_direction == 'right':
                            if (prediction_zone_start
                                    > self.roi_size_x * self.pixel_size):
                                stop_tracking.append(tr_obj)
                            elif (prediction_zone_start
                                  < pos_x
                                  < prediction_zone_end):
                                self.update_temp_feature("object_number",
                                                         ids, tr_obj)
                                self.update_temp_feature(
                                    "prediction_zone_start", ids,
                                    prediction_zone_start)
                                self.update_temp_feature(
                                    "prediction_zone_end", ids,
                                    prediction_zone_end)
                                found = True
                        else:
                            err_msg = "`self.flow_direction` must be in " \
                                      "[\"left\", \"right\"]"
                            raise NotImplementedError(err_msg)
                    else:
                        stop_tracking.append(tr_obj)

                # if event does not fall in any prediction zone, create new one
                if not found:
                    obj_nmbrs = self.dataset['object_number'][:].copy()
                    new_obj_nr = np.nanmax(obj_nmbrs) + 1
                    obj_nmbrs[ids] = new_obj_nr
                    dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                                feature="object_number",
                                                data=obj_nmbrs)
                    new_objects.append(new_obj_nr)

            for new_obj in new_objects:
                tracked_objects.append(new_obj)

            # stop tracking can contain duplicates that cause an error
            # when removing these objects from `tracked_objects`
            # -> make list with unique entries
            stop_tracking = list(set(stop_tracking))

        self.already_tracked = True
        self.assign_velocity_time()

    def assign_velocity_time(self):
        """ Compute time in region and velocity for every object captured in
        `track`. Currently only works properly for geometries with one channel
        region.

        Dependent on selected regions, the following features will be assigned
        to the dataset: `'velocity'`, `'time_channel'`, `'time_inlet'`,
        `'time_outlet'`.
        """
        self._create_velocity_time_features()
        velocity = track_tools.get_velocity(self.dataset)
        dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                    feature="velocity",
                                    data=velocity)

        # define inlet and outlet x-positions
        if self.channel_x_min is None or self.channel_x_max is None:
            self.set_channel_values_auto()
        # convert to meters
        x0_channel = self.channel_x_min * self.pixel_size
        x1_channel = self.channel_x_max * self.pixel_size

        flow = check_flow_direction(self.flow_direction)

        if flow == -1:  # left
            channel_start = x1_channel
            channel_end = x0_channel
            inlet_start = self.roi_size_x * self.pixel_size
        elif flow == 1:  # right
            channel_start = x0_channel
            channel_end = x1_channel
            inlet_start = 0
        else:
            raise ValueError("`flow` must be in [-1,1]")

        if self.channel:
            time_channel = track_tools.get_time_after_x(self.dataset,
                                                        channel_start,
                                                        flow)
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature="time_channel",
                                        data=time_channel)

        if self.inlet:
            time_inlet = track_tools.get_time_after_x(self.dataset,
                                                      inlet_start,
                                                      flow)
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature="time_inlet",
                                        data=time_inlet)

        if self.outlet:
            time_outlet = track_tools.get_time_after_x(self.dataset,
                                                       channel_end,
                                                       flow)
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature="time_outlet",
                                        data=time_outlet)

    def assign_fluorescence_peak(self):
        """Assign each event of an object the fluorescence information that
        the object has when it is close to the light sheet.
        Only considers the `flx_max` features.
        Creates for each `flx_max` feature a new temporary feature called
        `flx_max_obj`."""
        if not self.check_if_fluorescence_is_usable():
            raise FluorescenceNotUsableError()
        if not self.already_tracked:
            raise DatasetNotTrackedError("Data needs to be tracked before "
                                         "fluorescence peaks can be "
                                         "assigned.")
        device_id = self.dataset.config["setup"]["identifier"]
        device_data = utils.device_info[device_id]
        pos_light_sheet = device_data["pos_light_sheet"]
        list_of_fl_feats = [feat for feat in self.dataset.features
                            if re.match("fl[0-9]_max", feat)]

        self._create_new_fluorescence_features()
        # in px
        roi_center = self.dataset.config["imaging"]["roi position x"]
        left_edge = roi_center - self.dataset.config["imaging"][
            "roi size x"] / 2
        # in um, so that we can use the `pos_x` feature
        new_light_sheet_pos_um = (pos_light_sheet - left_edge) * \
            self.pixel_size

        object_numbers = np.array(self.dataset["object_number"][:])
        list_of_objects = np.unique(object_numbers)
        warning_counter = 0
        for object_nb in list_of_objects:
            # get object at light sheet
            obj_indices = np.nonzero(object_numbers == object_nb)[0]
            obj_positions = self.dataset["pos_x"][obj_indices]

            relative_positions = np.abs(obj_positions - new_light_sheet_pos_um)

            if np.min(relative_positions) < 20:
                closest_event = np.argmin(relative_positions)
                for feat in list_of_fl_feats:
                    self.update_temp_feature(
                        feature=f'{feat}_obj',
                        ids=obj_indices,
                        value=self.dataset[feat][obj_indices[closest_event]])
            else:
                warning_counter += 1

        if warning_counter:
            warnings.warn("The closest event of an object was further than"
                          " 20 um away from the light sheet. This happened for"
                          f" {warning_counter} object(s).")

    def time_after_x(self, x_reference, return_value=True,
                     add_to_dataset=False, variable_name=None):
        """Compute the time after passing position `x_reference` in the
        channel. Array can be returned (default) and/or added to the dataset
        with a user-defined feature name.

        Parameters
        ----------
        x_reference : float
            x-value in um to compute the time after
        return_value : bool
            If True (default) resulting array is returned
        add_to_dataset : bool
            If True (default=False) the result will be added to the dataset. If
            True, then a `variable_name` has to be defined.
        variable_name : str
            The name time feature you want to assign. This will be converted to
            a `dclab` user-defined temporary feature

        Returns
        -------
        time: 1d numpy array
            The computed times in reference to `x_reference`

        Raises
        ------
        ValueError
            If `add_to_dataset=True` but `variable_name != str`
        """
        flow = check_flow_direction(self.flow_direction)
        time = track_tools.get_time_after_x(self.dataset, x_reference,
                                            flow)
        if add_to_dataset:
            if not isinstance(variable_name, str):
                err_msg = ("You must define a variable name with the option "
                           "`variable_name=` if you want to add the time to "
                           "the dataset.")
                raise ValueError(err_msg)

            if not dclab.definitions.feature_exists(variable_name):
                dclab.register_temporary_feature(feature=variable_name)
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature=variable_name,
                                        data=time)
        if return_value:
            return time

    def export_tracked_dataset_as_hdf5(self, path, features=None, **kwargs):
        """Wrapper function on the normal dclab hdf5 export.

        See `dctrack.io.export_tracked_dataset_as_hdf5` for parameter
        details.

        See Also
        --------
        dctrack.io.export_tracked_dataset_as_hdf5
        """
        io.export_tracked_dataset_as_hdf5(self, path=path,
                                          mode="default", features=features,
                                          **kwargs)

    def write_video(self, save_path=None, first_frame=0, last_frame=0,
                    draw_zones=False, filtered=False):
        """Wrapper function for video writing.

        See `dctrack.video.video_write` for parameter details.
        """
        if filtered and self.filtered:
            ds = dclab.new_dataset(self.dataset)
        elif filtered and not self.filtered:
            warnings.warn("Dataset was not filtered. Will use unfiltered data",
                          SyntaxWarning)
            ds = self.dataset
        else:
            ds = self.dataset
        video_write(ds, save_path=save_path,
                    first_frame=first_frame, last_frame=last_frame,
                    draw_zones=draw_zones)

    def get_object(self, object_number, loaded_only=True, scalar_only=True):
        return analysis_tools.get_object(
            self.dataset, object_number, loaded_only=loaded_only,
            scalar_only=scalar_only)

    def _create_object_prediction_features(self):
        """Set the temporary features needed for tracking"""
        empty_array = np.full(len(self.dataset), np.nan)
        # first event in dataset gets object number = 0
        init_object_numbers = empty_array.copy()
        init_object_numbers[0] = 0
        dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                    feature='object_number',
                                    data=init_object_numbers)

        for new_feat in ['prediction_zone_start', 'prediction_zone_end']:
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature=new_feat,
                                        data=empty_array.copy())

    def check_if_fluorescence_is_usable(self):
        """Check based on frame rate and ROI position whether potentially
        available fluorescence in a dataset are usable.
        Returns True if yes and False if the information is not usable or if
        it is not even in the file.

        Returns `True` if all requirements to use the fluorescence information
        are met, otherwise `False` is returned."""
        device_id = self.dataset.config["setup"]["identifier"]
        try:
            device_data = utils.device_info[device_id]
        except KeyError:
            raise FluorescenceNotUsableError("Device not found, no default "
                                             "imaging information available "
                                             "for your device!")
        imaging_info = self.dataset.config["imaging"]
        list_of_fl_feats = [feat for feat in self.dataset.features
                            if re.match("fl[0-9]_max", feat)]
        if not list_of_fl_feats:
            return False
        if imaging_info["frame rate"] not in utils.possible_fl_fps:
            warnings.warn("The frame rate used for measuring is not suited to "
                          "match fluorescence features with events.")
            return False
        if imaging_info["roi position y"] != device_data["roi_pos_y"]:
            warnings.warn("ROI position was shifted in y direction. Not "
                          "possible to use fluorescence features.")
            return False
        if not imaging_info["roi position x"] - imaging_info["roi size x"] / 2\
            < device_data["pos_light_sheet"] < imaging_info["roi position x"]\
                + imaging_info["roi size x"] / 2:
            warnings.warn("Light sheet out of ROI. Cannot use fluorescence "
                          "information.")
            return False
        return True

    def _create_new_fluorescence_features(self):
        """Set new temporary feature for fluorescence information."""
        empty_array = np.full(len(self.dataset), np.nan)
        for feat in ["fl1_max", "fl2_max", "fl3_max"]:
            if feat in self.dataset and f'{feat}_obj' not in self.dataset:
                if not dfn.feature_exists(f'{feat}_obj'):
                    dclab.register_temporary_feature(feature=f'{feat}_obj',
                                                     is_scalar=True)
                dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                            feature=f'{feat}_obj',
                                            data=empty_array.copy())

    def _create_velocity_time_features(self):
        """Set the temporary features needed for assigning velocity and time"""
        empty_array = np.full(len(self.dataset), np.nan)
        dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                    feature='velocity',
                                    data=empty_array.copy())
        if self.channel:
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature='time_channel',
                                        data=empty_array.copy())
        if self.inlet:
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature='time_inlet',
                                        data=empty_array.copy())
        if self.outlet:
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature='time_outlet',
                                        data=empty_array.copy())

    def _create_filter_features(self):
        """Set the temporary features needed to filter tracked object features
        """
        empty_array = np.full(len(self.dataset), np.nan)
        if self.filter_obj_n_events:
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature='obj_n_events',
                                        data=empty_array.copy())
        else:
            err_msg = "Time assignment only implemented for self.channel=True"
            raise NotImplementedError(err_msg)
        if self.filter_obj_distance:
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature='obj_distance',
                                        data=empty_array.copy())

    def filter(self, inplace=False,
               max_back_movement=0, min_events=0, max_events=1000,
               min_distance=0, max_distance=2000,
               return_filter=False, return_filtered_ds=False):
        """Perform object filtering on the data. Function performs
        `self.dataset.apply_filter()`.

        Parameters
        ----------
        inplace : *optional*, bool
            If True, filter the current dataset of the `TrackedDataSet`
        max_back_movement : *optional*, float
            Maximum distance in um an object is allowed to move backwards
            during tracking.
        min_events : *optional*, int
            Minimum number of events an object should have.
        max_events : *optional*, int
            Maximum number of events for an object.
        min_distance : *optional*, float
            Minimum distance in um an object has to cover during tracking.
        max_distance : *optional*, float
            Maximum distance in um an object can cover during tracking.
        return_filter : *optional* bool
            If True, the filter array will be returned
        return_filtered_ds : *optional* bool
            If True, the filtered dataset will be returned

        Returns
        -------
        If return_filter==True:
        out : ndarray
            Boolean array containing the filter for self.dataset
        If return_filtered_ds==True:
        out : ndarray
            Filtered dataset
        If return_filter==True and return_filtered_ds==True
        out : tuple
            filter_array, filtered_dataset
        """
        utils.check_ds_for_feat(self.dataset, feature="object_number")

        flow = check_flow_direction(self.flow_direction)
        ds = self.dataset

        # only put out events that have an object number assigned
        filters = ~np.isnan(ds['object_number'])

        uniq_objects = self.objects()
        for obj in uniq_objects:
            obj_dict, obj_idx = analysis_tools.get_object(ds, obj,
                                                          return_index=True)
            if self.filter_backward_movement:
                x_diff = flow * np.diff(obj_dict['pos_x'])

                if np.any(x_diff < -max_back_movement):
                    filters[obj_idx] = False
                    continue

            if self.filter_obj_n_events:
                if not min_events <= len(obj_idx[obj_idx]) <= max_events:
                    filters[obj_idx] = False
                    continue

            if self.filter_obj_distance:
                obj_distance = abs(max(obj_dict['pos_x'])
                                   - min(obj_dict['pos_x']))
                if not min_distance <= obj_distance <= max_distance:
                    filters[obj_idx] = False

        # True entries in filters_final should be also True in filtered ds
        self.dataset.filter.manual[~filters] = False
        self.dataset.apply_filter()
        if inplace:
            self.dataset = dclab.new_dataset(ds)
        self.filtered = True

        if return_filtered_ds and not return_filter:
            return dclab.new_dataset(self.dataset)
        elif return_filter and not return_filtered_ds:
            return filters
        elif return_filter and return_filtered_ds:
            return filters, dclab.new_dataset(self.dataset)

    def objects(self, filtered=False):
        """Get a list of the object numbers included in the tracked dataset.

        Parameters
        ----------
        filtered : *optional* bool
            If True, only filtered objects will be returned
        Returns
        -------
        out : ndarray
            Array with available object numbers
        """
        if filtered:
            if self.filtered:
                ds = dclab.new_dataset(self.dataset)
            else:
                warn_msg = "No filters applied to the dataset. Return " \
                           "objects without filters."
                warnings.warn(warn_msg, UserWarning)
                ds = self.dataset
        else:
            ds = self.dataset

        uniq_objects = np.unique(ds['object_number'])
        # remove nans
        return uniq_objects[~np.isnan(uniq_objects)]

    def get_extensional_paras_dataframe(self, **kws):
        """Compute features used for analysis in extensional fields and return
        data in a pandas dataframe.

        Returns: pandas.Dataframe()
            Dataframe with additional analysis parameters
        """
        return analysis_tools.compute_extensional_para_dataframe(self, **kws)

    def get_ellipse_data(self, show_progress=False):
        """Calculate the ellipse features for all events in the dataset and add
        to dataset.

        Parameters
        ----------
        show_progress: *bool*
            If True, a progress bar showing the progress for the dataset will
            be displayed.
        """
        df_ellipse = analysis_tools.calc_ellipses(self.dataset,
                                                  show_progress=show_progress)
        for feat in df_ellipse.columns:
            deregister_temporary_feature(feat)
            dclab.register_temporary_feature(feature=feat)
            data = df_ellipse[feat].to_numpy()
            dclab.set_temporary_feature(rtdc_ds=self.dataset,
                                        feature=feat,
                                        data=data.copy())

    def get_ellipse_data_scalar(self, ds_original, show_progress=False):
        """Calculate the ellipse parameters for a scalar dataset by matching it
        with the original dataset.

        Parameters
        ----------
        ds_original: RTDCDataSet
            RTDCDataSet of original data with contour and image.
        show_progress: *bool*
            If True, a progress bar showing the progress for the dataset will
            be displayed.
        Returns
        -------
        Dataset with ellipse features.
        """
        self.dataset = analysis_tools.calc_ellipse_from_scalar_export(
            self.dataset, ds_original, show_progress=show_progress)
