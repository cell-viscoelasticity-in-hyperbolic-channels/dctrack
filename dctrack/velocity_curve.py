import numpy as np
import dclab


def velocity_max_rectangular_duct(flow_rate, width, height):
    """The maximum flow velocity of a Newtonian liquid through a rectangular
    duct.

    Parameters
    ----------
    flow_rate : Flow rate in ul/s
    width : channel width in um
    height : channel height in um

    Returns
    -------
    velocity_max : Maximum flow velocity in um/s
    """
    prefactor = 48 * flow_rate * 1e9 \
        / (np.pi**3 * (1 - 0.63*height / width) * height * width)  # um/s
    sum_a = 8 / 9
    sum_b = 1 / (np.cosh(np.pi / 2 * width / height))
    sum_c = 1 / (np.cosh(3 * np.pi / 2 * width / height))
    return prefactor * (sum_a + sum_b + sum_c)


def get_init_velocity_curve(data, flow_rate, chanel_width, channel_height,
                            flow_direction='left', first_frame=0,
                            last_frame=2000):
    """Compute the velocity - x-position curve from objects occurring in
    consecutive frames moving in the direction of the flow.
    When tracking objects in multi-constriction channels, usually a pressure
    pump was used to drive the objects through the chip, which results in a
    flow rate of 0 ul/s. In that case, the median displacement will be
    estimated.

    Parameters:
        data - Data container generated with load_data
        frame_rate - frame rate used in the measurement
        flow_rate - Flow rate in ul/s
        width - channel width in um
        height - channel height in um
        flow-direction - *optional* Direction of the flow. Possible values:
        ('left', 'right'). Left means flow from right to left. default='left'
    Returns: *tuple* (x_positions, velocity)
        x-position and velocity in the units of data['pos_x']*frame_rate
    """
    flow = check_flow_direction(flow_direction)

    size_data = len(data['frame'])

    if last_frame == 0 or last_frame > size_data:
        last_frame = size_data

    data_slice = np.array(data['frame'])[first_frame:last_frame]
    frames_uniq, index_uniq, counts = np.unique(data_slice,
                                                return_index=True,
                                                return_counts=True)
    # frames_uniq still contains the frame number where multiple
    # events occurred
    # only take frames that were counted only once
    frames_uniq = frames_uniq[np.nonzero(counts == 1)[0]]
    index_uniq = index_uniq[np.nonzero(counts == 1)[0]]

    # only consecutive frames
    index_consec_frames = np.diff(frames_uniq) == 1
    index_uniq = index_uniq[1:][index_consec_frames]
    frames_uniq = frames_uniq[1:][index_consec_frames]

    # add offset from first_frame to index_uniq
    index_uniq += first_frame

    pos_x_uniq = np.array(data['pos_x'])[index_uniq]

    # ensure positive values with flow multiplier
    frame_rate = data.config['imaging']['frame rate']
    velocity = np.diff(pos_x_uniq) / (np.diff(frames_uniq) / frame_rate) * flow

    # only take positive velocity values and v < v_max
    v_max = velocity_max_rectangular_duct(flow_rate, chanel_width,
                                          channel_height)
    if v_max <= 1E-8:
        # likely a pressure pump was used
        # estimate median displacement between frames
        pos_x_diff = np.diff(pos_x_uniq) * flow
        frame_diff = np.diff(frames_uniq)[pos_x_diff > 0]
        pos_x_diff = pos_x_diff[pos_x_diff > 0]
        pos_x_diff = pos_x_diff[frame_diff == 1]
        med_pos_displacement = np.median(pos_x_diff)
        v_max = med_pos_displacement * frame_rate

    v_good_idx = (velocity > 0) & (velocity < v_max)

    # match dimension of pos_x array with velocity array
    x_positions = pos_x_uniq[1:]
    x_positions = x_positions[v_good_idx]
    velocity = velocity[v_good_idx]

    # sorting data helpful for curve fitting
    sort_ind = np.argsort(x_positions)
    x_positions = x_positions[sort_ind]
    velocity = velocity[sort_ind]

    return x_positions, velocity


def velocity_fit_channel(velocity, ci=1):
    """Estimate the steady median velocity in a straight channel.

    Parameters
    ----------
    velocity : np.array, array with the velocity values
    ci : Confidence interval in ci * standard deviation of velocity
    """
    median_v = np.median(velocity)
    std_v = np.std(velocity)
    # find events in interval ci*std_v around the median velocity
    # used to filter out outliers
    in_ci = ((median_v - ci * std_v < velocity)
             & (velocity < median_v + ci * std_v))
    v_filtered = velocity[in_ci]
    v_channel = np.median(v_filtered)

    return v_channel


def velocity_fit_poly(x_positions, velocity):
    """Fit curve for the velocity over x."""
    # 1st fit with higher weight on dense datapoints:
    # ensure that fit captures majority of data well and filter events smaller
    # than the fit curve after
    # TODO: transition to numpy.polynomial:
    #  https://numpy.org/doc/stable/reference/routines.polynomials.html
    kde = dclab.kde_methods.kde_histogram(x_positions, velocity)
    degree = 3
    poly_coeffs1 = np.polyfit(x_positions, velocity, degree, w=kde)
    poly1 = np.poly1d(poly_coeffs1)
    v_fit1 = poly1(x_positions)
    # init velocity curve comes with some outliers at low velocities that
    # disturb the fit. Filter these small values with min value from 1st fit
    v_min = min(v_fit1)
    ind_v_bigenough = velocity > v_min
    if np.any(ind_v_bigenough):
        x_filter = x_positions[ind_v_bigenough]
        v_filter = velocity[ind_v_bigenough]
        # 5th degree polynomial works good for standard RT-DC channels, might
        # need different solution for other geometries
        degree = 5
        poly_coeffs2 = np.polyfit(x_filter, v_filter, degree)
        poly2 = np.poly1d(poly_coeffs2)

        return poly2
    else:
        return None


def fit_velocity(x_positions, velocity, channel_zone, image_width,
                 fit_inlet=False, fit_channel=True, fit_outlet=False,
                 flow_direction='left'):
    """Get the velocity profile for the entire geometry.
    Returns dictionary with fit functions for different channel regions"""
    flow = check_flow_direction(flow_direction)

    if flow == -1:
        channel_start = channel_zone[1]
        channel_end = channel_zone[0]
        inlet_start = image_width
        outlet_end = 0
    elif flow == 1:
        channel_start = channel_zone[0]
        channel_end = channel_zone[1]
        inlet_start = 0
        outlet_end = image_width

    velocity_fits = {}
    if fit_channel:
        in_channel = ((flow * channel_start < flow * x_positions)
                      & (flow * x_positions < flow * channel_end))
        # if in_channel.size > 0:
        if np.any(in_channel):
            v_in_channel = velocity[in_channel]
            v_channel = velocity_fit_channel(v_in_channel)
            region = sorted([channel_start, channel_end])
            velocity_fits['channel'] = {'v': v_channel,
                                        'x0': region[0],
                                        'x1': region[1]
                                        }
    if fit_inlet:
        in_inlet = flow * x_positions < flow * channel_start
        region = sorted([inlet_start, channel_start])
        if np.any(in_inlet):
            x_in_inlet = x_positions[in_inlet]
            v_in_inlet = velocity[in_inlet]
            poly_inlet = velocity_fit_poly(x_in_inlet, v_in_inlet)
            if poly_inlet:
                velocity_fits['inlet'] = {'poly': poly_inlet,
                                          'x0': region[0],
                                          'x1': region[1]
                                          }
            else:
                print("Not enough data available for outlet fitting. Will "
                      "take channel velocity if available.")
                if fit_channel:
                    velocity_fits['inlet'] = {'v': v_channel,
                                              'x0': region[0],
                                              'x1': region[1]
                                              }
                else:
                    raise ValueError("`fit_channel==False` -> Estimation of "
                                     "inlet velocity not possible")
        # fail safe if there is not enough data in the inlet region available
        else:
            print("Not enough data available for inlet fitting. Will take "
                  "channel velocity if available.")
            if fit_channel:
                velocity_fits['inlet'] = {'v': v_channel,
                                          'x0': region[0],
                                          'x1': region[1]
                                          }
            else:
                raise ValueError("`fit_channel==False` -> Estimation of inlet "
                                 "velocity not possible")

    if fit_outlet:
        in_outlet = flow * x_positions > flow * channel_end
        region = sorted([channel_end, outlet_end])
        if np.any(in_outlet):
            x_in_outlet = x_positions[in_outlet]
            v_in_outlet = velocity[in_outlet]
            poly_outlet = velocity_fit_poly(x_in_outlet, v_in_outlet)
            if poly_outlet:
                velocity_fits['outlet'] = {'poly': poly_outlet,
                                           'x0': region[0],
                                           'x1': region[1]
                                           }
            else:
                print("Not enough data available for outlet fitting. Will "
                      "take channel velocity if available.")
                if fit_channel:
                    velocity_fits['outlet'] = {'v': v_channel,
                                               'x0': region[0],
                                               'x1': region[1]
                                               }
                else:
                    raise ValueError("`fit_channel==False` -> Estimation of "
                                     "outlet velocity not possible")
        # fail safe if there is not enough data in the outlet region available
        else:
            print("No outlet data available for fitting. Will take channel "
                  "velocity if available.")
            if fit_channel:
                velocity_fits['outlet'] = velocity_fits['channel']
            else:
                raise ValueError("`fit_channel==False` -> Estimation of outlet"
                                 " velocity not possible")

    # If no channel velocity could be determined, estimate based on inlet or
    # outlet fit
    if fit_channel and 'channel' not in velocity_fits.keys():
        if 'inlet' in velocity_fits.keys():
            poly = velocity_fits['inlet']['poly']
            v_channel_start = poly(channel_start)
            region = sorted([channel_start, channel_end])
            velocity_fits['channel'] = {'v': v_channel_start,
                                        'x0': region[0],
                                        'x1': region[1]
                                        }
        elif 'outlet' in velocity_fits.keys():
            poly = velocity_fits['outlet']['poly']
            v_channel_end = poly(channel_end)
            region = sorted([channel_start, channel_end])
            velocity_fits['channel'] = {'v': v_channel_end,
                                        'x0': region[0],
                                        'x1': region[1]
                                        }
    return velocity_fits


def predict_displacement(x, fit_dict, delta_t, flow_direction='left'):
    """Predict the displacement on position x during time delta_t based on the
    velocity curve saved in fit_dict"""
    flow = check_flow_direction(flow_direction)
    region = None

    # check in which region of the flow the object is
    for key in fit_dict.keys():
        if fit_dict[key]['x0'] <= x <= fit_dict[key]['x1']:
            region = key

    if region is None:
        # raise ValueError("x not found in any channel region")
        return None
    elif region == 'channel':
        v = fit_dict[region]['v']
    else:
        if 'poly' in fit_dict[region].keys():
            poly = fit_dict[region]['poly']
            v = poly(x)
        elif 'v' in fit_dict[region].keys():
            v = fit_dict[region]['v']
        else:
            raise ValueError("No velocity prediction available for object at "
                             "x = {} µm".format_map(x))

    displacement = v * delta_t * flow
    return displacement


def check_flow_direction(flow_direction):
    if flow_direction == 'left':
        flow = -1
    elif flow_direction == 'right':
        flow = 1
    else:
        errmsg = ("Unknown value for flow_direction: {}. "
                  "Valid values are: ('left', 'right')".format(flow_direction))
        raise ValueError(errmsg)

    return flow
