
import pyqtgraph as pg


def image_select_xmin_xmax(app, image, x_min=None, x_max=None,
                           window_title=" ", auto_close=False):
    """Select maximum and minimum x-position in an image with sliders"""
    height, width = image.shape

    # Interpret image data as row-major instead of col-major
    pg.setConfigOptions(imageAxisOrder='row-major')

    # main window
    w = pg.QtWidgets.QWidget()
    # window for channel selection
    win = pg.GraphicsLayoutWidget(title=window_title)
    win.resize(width, height)
    win.show()
    win.setWindowTitle(window_title)
    vb = pg.ViewBox()
    win.setCentralItem(vb)
    # lock the aspect ratio so pixels are always square
    vb.setAspectLocked(True)

    # Item for displaying image data
    img_item = pg.ImageItem()
    vb.addItem(img_item)
    img_item.setImage(image)
    vb.setAutoPan()

    # define infinite line object
    inf_line_pen = pg.mkPen(color='w', width=3)
    hover_line_pen = pg.mkPen(color='g', width=5)
    brush = pg.mkBrush((0, 255, 0, 25))
    lin_region = pg.LinearRegionItem(movable=True,
                                     orientation='vertical',
                                     pen=inf_line_pen,
                                     hoverPen=hover_line_pen,
                                     brush=brush,
                                     bounds=[0, width])
    if isinstance(x_min, int) and isinstance(x_max, int):
        lin_region.setRegion((x_min, x_max))
    else:
        lin_region.setRegion((0, width))
    vb.addItem(lin_region)

    # Add a Ok button to confirm the selection
    # This implementation is super messy but works for now
    # TODO proper built of a qt window with channel selection and ok button
    button = pg.QtWidgets.QPushButton("Ok")
    # button.clicked.connect(w.close)
    button.clicked.connect(app.quit)
    if auto_close:
        button.animateClick()

    layout = pg.QtWidgets.QGridLayout()
    w.setLayout(layout)

    layout.addWidget(win, 0, 0)
    layout.addWidget(button, 1, 0)

    w.show()
    app.exec_()

    x_min, x_max = lin_region.getRegion()

    return float(x_min), float(x_max)
