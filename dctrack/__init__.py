""""
This is a Python library for post-measurement analysis of real-time
deformability cytometry (RT-DC) datasets to track individual objects when they
pass a channel and assign contour and image parameters. The aim is to make it
possible to track cells from the datasets independent from the actual channel
design.
"""
# flake8: noqa: F401
from . import track
from . import track_mmm
from . import graph_selection
from . import channel_detection
from . import velocity_curve
from . import analysis_tools
from .main import TrackingSession

from ._version import __version__
