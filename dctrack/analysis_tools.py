from __future__ import annotations

import pathlib
import warnings
import numpy as np
import pandas as pd
from dclab.features.emodulus.pxcorr import corr_deform_with_area_um
import dclab
from dclab.rtdc_dataset.feat_temp import deregister_temporary_feature
from skimage.measure import EllipseModel
from tqdm.autonotebook import tqdm

from . import track
from .utils import check_ds_for_feat


class FeatureNotValidError(BaseException):
    pass


def get_object(dataset, object_number, loaded_only=True, scalar_only=True,
               return_index=False):
    """Return slice of `dataset` that only contains the data for events with
    `dataset[object_number]==object_number`.

    Parameters
    ----------
    dataset : RTDCBase
        Tracked dataset. Must contain feature 'object_number'
    object_number : int
        Number of object to return
    loaded_only : bool
        If set True, only already available scalar features will be collected
        (innate scalar features + tracking features).
    scalar_only : bool
        If set to True, only the scalar features will be collected. This
        improves performance of the function considerably.
    return_index : bool
        If set True, return the index of teh object events in `dataset`

    Returns
    -------
    object_dict: dictionary
        Dictionary with keys=dataset.features that only contains data for
        `object_number`

    or *tuple* (object_dict, object_index)

    Raises
    -------
    FeatureDoesNotExistError
        If `dataset` does not contain feature `object_number`
    """
    check_ds_for_feat(dataset, "object_number")

    object_dict = {}
    object_idx = dataset['object_number'] == object_number

    if loaded_only:
        features_scalar_loaded = list(set(dataset.features_loaded)
                                      & set(dataset.features_scalar))
        for feat in features_scalar_loaded:
            object_dict[feat] = dataset[feat][object_idx]
    else:
        # calculate scalars
        features_scalar = dataset.features_scalar
        for feat in features_scalar:
            object_dict[feat] = dataset[feat][object_idx]

        # calculate non scalars
        if not scalar_only:
            features_all = dataset.features
            contour = "contour"
            features_non_scalar = list(
                set(features_all) - set(features_scalar) - {contour})
            for feat in features_non_scalar:
                object_dict[feat] = dataset[feat][:][object_idx]
            # contour is a non-uniform shape
            object_dict[contour] = dataset[contour][object_idx]

    if return_index:
        return object_dict, object_idx
    else:
        return object_dict


def convert_x_and_time(tr_ds, df, x_ref, label):
    """Convert x-position and time in reference to an x-position in the channel

    Parameters
    ----------
    tr_ds: *TrackedDataSet*
        TrackedDataSet with object and time information
    df: *pandas.DataFrame* object
        Dataframe to add information to
    x_ref: *int* or *float*
        Reference x-position [um] to compute position and time for.
    label: *str*
        Name to save the resulting features.

    Returns
    -------
        Dataframe added with converted x and time.
    """
    x_convert = x_ref - df['pos_x']
    time_convert = tr_ds.time_after_x(x_ref)
    x_label = "x_" + label
    time_label = "time_" + label
    df[x_label] = x_convert
    df[time_label] = time_convert
    return df


def compute_extensional_para_dataframe(tr_ds,
                                       hyper_start=None, hyper_end=None,
                                       stable_extension_start=None):
    """
    Write the existing scalar features to a pandas dataframe and compute
    additional parameters that are used in extensional flow experiments.

    Parameters
    ----------
    tr_ds: `TrackedDataSet` instance of `dctrack`
    hyper_start: optional, *int* or *float*
        Define start of the hyperbolic region in um
    hyper_end: optional, *int* or *float*
        Define the end of the hyperbolic region in um
    stable_extension_start: optional, *int* or *float*
        Define where the region of stable extension rate starts in um

    Returns
    -------
    df: pandas.Dataframe()
        Dataframe with additional analysis parameters
    """
    ds = tr_ds.dataset
    df = pd.DataFrame()
    for feat in ds.features:
        df[feat] = ds[feat][:]

    df['deform_corr'] = (df['deform']
                         - corr_deform_with_area_um(df['area_um'],
                                                    px_um=tr_ds.pixel_size))
    df['taylor_def'] \
        = (df['size_x'] - df['size_y']) / (df['size_x'] + df['size_y'])
    df['net_tensile_strain'] \
        = (df['size_x'] - df['size_y']) / np.sqrt(df['size_x'] * df['size_y'])

    channel_entrance = tr_ds.channel_x_max * tr_ds.pixel_size
    channel_exit = tr_ds.channel_x_min * tr_ds.pixel_size

    df['x_inlet'] = channel_entrance - df['pos_x']
    df['x_outlet'] = channel_exit - df['pos_x']

    if isinstance(hyper_start, (int, float)):
        df = convert_x_and_time(tr_ds, df, hyper_start, "hyper")
    if isinstance(hyper_end, (int, float)):
        df = convert_x_and_time(tr_ds, df, hyper_end, "hyper_out")
    if isinstance(stable_extension_start, (int, float)):
        df = convert_x_and_time(tr_ds, df, stable_extension_start,
                                "hyper_stable")
    # compute the extension rates for each object
    df['extension_rate'] = np.nan
    uniq_objects = tr_ds.objects(filtered=tr_ds.filtered)
    for ii in uniq_objects[:]:
        idx = ii == ds['object_number']
        pos_x = ds['pos_x'][idx]
        velocity = np.abs(ds['velocity'][idx])

        extension_rate = np.empty(velocity.size)
        extension_rate[:] = np.nan
        extension_rate[1:] = np.diff(velocity) / np.diff(pos_x * 1e-6)
        df.loc[idx, 'extension_rate'] = extension_rate

    # extension rates and velocity usually negative
    df['-velocity'] = -df['velocity']
    df['-extension_rate'] = -df['extension_rate']

    return df


def export_data_of_multi_constriction(
        tracked_ds: track.TrackedDataSet,
        output_path: str | pathlib.Path,
        filtered: bool = True,
        special_filters: dict = None,
        **kwargs):
    """Export relevant data for analysis of experiments in multi-constriction
    channels.
    These inlude:
    - transit times through constrictions
    - transit times between constrictions
    - area before and after constrictions
    - deformation before and after constrictions
    - principal inertia ratio before and after constrictions

    These parameters are given for each constriction separately and
    all of that saved in a csv file for further analysis and plotting.

    Channel regions should start and end where an inflection point of the
    channel walls can be seen. For constrictions, this means the position
    where the funnel part begins, not the constriction itself.
    Each of the features mentioned above are meant to be from cells which are
    not more than the mean radius away from the position in the channel where
    the next channel region begins. Previous to version 0.5.0, this was done
    using an argument for this function with which one could specify the
    distance to the start of the next channel region. Since this is a factor
    that depends heavily on cell size, it will now consider the cell radius.

    Constrictions and channel parts are counted in the direction of flow.

    Parameters
    ----------
    tracked_ds
        The dataset from which data will be exported.
    output_path
        The path where to export the results to.
    filtered
        If True, only those objects will be considered that were alone in the
        channel and which made it from channel entry to exit. List of good
        events is generated in the function `get_filtered_object_numbers()`.
    special_filters
        Box filters on any feature that the objects have to lie in before they
        enter the channel (first event of an object is checked), e.g. area
        or fl3_max etc. Passed via argument to the function
        `get_filtered_object_numbers()`.
    kwargs
        Additional parameters that should be saved to the exported csv file.
        The key will be the column name, the value will be stored in each row.
        Must be scalar (int, float, string), otherwise (list, dict) it will be
        ignored.

    Returns
    -------
    multi_con_stats: pd.DataFrame
        A DataFrame with the single cell statistical data regarding transit
        times, areas, deformations, and inertia ratios.
    """
    sample_name = tracked_ds.dataset.config["experiment"]["sample"]
    if special_filters is None:
        special_filters = {}
    if not isinstance(special_filters, dict):
        raise TypeError(f"Filters need to be given as dictionary, not "
                        f"{type(special_filters)}")
    output_path = pathlib.Path(output_path)
    if output_path.suffix == ".csv":
        output_file = output_path
    else:
        if filtered:
            output_file = output_path / f"{sample_name}_statistics_" \
                                        f"multi_constriction_filtered.csv"
        else:
            output_file = output_path / f"{sample_name}_statistics_" \
                                        f"multi_constriction.csv"

    multi_con_stats = pd.DataFrame()

    object_numbers = np.array(tracked_ds.dataset["object_number"][:])
    object_numbers_uniq = np.unique(object_numbers)
    if filtered:
        object_numbers_uniq = get_filtered_object_numbers(tracked_ds,
                                                          special_filters)

    channel_regions = tracked_ds.channel_regions
    region_types = determine_region_type(tracked_ds)
    if tracked_ds.flow_direction == "left":
        channel_regions = channel_regions[::-1]

    if "inlet" in region_types:
        inlet_region = channel_regions[region_types == "inlet"]
    else:
        if region_types[0] == "channel":
            inlet_region = channel_regions[0]
        else:
            inlet_region = channel_regions[1]

    ds = dclab.new_dataset(tracked_ds.dataset)
    ds.config["filtering"]["pos_x min"] = np.min(
        inlet_region) * tracked_ds.pixel_size
    ds.config["filtering"]["pos_x max"] = np.max(
        inlet_region) * tracked_ds.pixel_size
    ds.apply_filter()
    mean_size_x = np.mean(ds["size_x"][ds.filter.all])
    mean_size_y = np.mean(ds["size_y"][ds.filter.all])
    mean_radius = (mean_size_x + mean_size_y) / 2 / 2

    for iter_idx, object_nb in enumerate(object_numbers_uniq):
        data_dict = {}
        data_dict.update({"cell number": object_nb})
        counter_chan = 0
        counter_con = 0

        df_obj = pd.DataFrame()

        for region_idx, region_type in enumerate(region_types):
            if region_type == "channel":
                counter_chan += 1
                counter = counter_chan
            elif region_type == "constriction":
                counter_con += 1
                counter = counter_con
            else:
                # inlet or outlet
                counter = 1

            area_start = np.nan
            deform_start = np.nan
            inert_ratio_princ_start = np.nan
            area_end = np.nan
            deform_end = np.nan
            inert_ratio_princ_end = np.nan

            time_start = np.nan
            time_end = np.nan
            transit_time = np.nan

            region_coords = channel_regions[region_idx]  # in pixels!
            if tracked_ds.flow_direction == "left":
                region_coords = region_coords[::-1]
            # get index of event closest to the channel point of interest
            obj_indices = np.nonzero(object_numbers == object_nb)[0]
            obj_positions = tracked_ds.dataset["pos_x"][obj_indices]

            # infer event with the smallest distance from region point, but
            # inside a channel part, not a constriction part
            cell_pos_in_region = (obj_positions > np.min(region_coords)
                                  * tracked_ds.pixel_size) & (
                                  obj_positions < np.max(
                                      region_coords) * tracked_ds.pixel_size)

            if np.count_nonzero(cell_pos_in_region) > 1:
                # we have at least two events in the current channel region
                i_start = np.min(obj_indices[cell_pos_in_region])
                i_end = np.max(obj_indices[cell_pos_in_region])

                diff_to_start = abs(
                    tracked_ds.dataset["pos_x"][i_start] -
                    region_coords[0] * tracked_ds.pixel_size)

                diff_to_end = abs(
                    tracked_ds.dataset["pos_x"][i_end] -
                    region_coords[1] * tracked_ds.pixel_size)

                # if objects further than a cell radius away, save np.nan
                if np.min(diff_to_start) < mean_radius:
                    area_start = tracked_ds.dataset["area_um"][i_start]
                    deform_start = tracked_ds.dataset["deform"][i_start]
                    inert_ratio_princ_start = tracked_ds.dataset[
                        "inert_ratio_prnc"][i_start]

                if np.min(diff_to_end) < mean_radius:
                    area_end = tracked_ds.dataset["area_um"][i_end]
                    deform_end = tracked_ds.dataset["deform"][i_end]
                    inert_ratio_princ_end = tracked_ds.dataset[
                        "inert_ratio_prnc"][i_end]

                time_start_uncorr = tracked_ds.dataset["time"][i_start]
                time_end_uncorr = tracked_ds.dataset["time"][i_end]

                # to correct the time stamps, we need a velocity estimation
                vel_start = abs((tracked_ds.dataset["pos_x"][i_start + 1] -
                                 tracked_ds.dataset["pos_x"][i_start]) /
                                (tracked_ds.dataset["time"][i_start + 1] -
                                 tracked_ds.dataset["time"][i_start]))
                vel_end = abs((tracked_ds.dataset["pos_x"][i_end] -
                               tracked_ds.dataset["pos_x"][i_end - 1]) /
                              (tracked_ds.dataset["time"][i_end] -
                               tracked_ds.dataset["time"][i_end - 1]))

                time_start = time_start_uncorr + diff_to_start / vel_start
                time_end = time_end_uncorr + diff_to_end / vel_end
                transit_time = abs(time_end - time_start)

            data_dict.update(
                {"region_type": region_type,
                 "region_type_occurance": counter,
                 "region_count": region_idx + 1,
                 "area start": area_start,
                 "area end": area_end,
                 "deform start": deform_start,
                 "deform end": deform_end,
                 "inert_ratio_princ start": inert_ratio_princ_start,
                 "inert_ratio_princ end": inert_ratio_princ_end,
                 "time start": time_start,
                 "time end": time_end,
                 "transit time": transit_time})

            df_region = pd.DataFrame(data_dict, index=[iter_idx])
            df_obj = pd.concat([df_obj, df_region], ignore_index=True)

        # go through multi_con_stats again and update all the constriction
        # values. Also recalculate transit time.
        for region_idx, region_type in enumerate(region_types):
            if region_type != "constriction":
                continue
            if region_idx == 0:
                for feat in ["area", "deform", "inert_ratio_princ", "time"]:
                    feat_start = df_obj[
                        (df_obj["region_type"] == "channel") & (df_obj[
                            "region_count"] == region_idx + 2)].iloc[
                        0][feat + " start"]

                    idx = df_obj[(df_obj[
                        "region_type"] == "constriction") & (df_obj[
                            "region_count"] == region_idx + 1)].index.tolist()
                    df_obj.loc[idx, [feat + " end"]] = feat_start

            if 0 < region_idx < len(region_types) - 1:
                for feat in ["area", "deform", "inert_ratio_princ", "time"]:
                    feat_end = df_obj[
                        (df_obj["region_type"] == "channel") & (df_obj[
                            "region_count"] == region_idx)].iloc[
                        0][feat + " end"]
                    idx = df_obj[(df_obj[
                        "region_type"] == "constriction") & (df_obj[
                            "region_count"] == region_idx + 1)].index.tolist()
                    df_obj.loc[idx, [feat + " start"]] = feat_end
                # same procedure for values at end of constriction:
                # take values from beginning of next channel part
                for feat in ["area", "deform", "inert_ratio_princ", "time"]:
                    feat_start = df_obj[
                        (df_obj["region_type"] == "channel") & (df_obj[
                            "region_count"] == region_idx + 2)].iloc[
                        0][feat + " start"]

                    idx = df_obj[(df_obj[
                        "region_type"] == "constriction") & (df_obj[
                            "region_count"] == region_idx + 1)].index.tolist()
                    df_obj.loc[idx, [feat + " end"]] = feat_start

            if region_idx == len(region_types) - 1:
                for feat in ["area", "deform", "inert_ratio_princ", "time"]:
                    feat_end = df_obj[
                        (df_obj["region_type"] == "channel") & (df_obj[
                            "region_count"] == region_idx)].iloc[
                        0][feat + " end"]
                    idx = df_obj[(df_obj[
                        "region_type"] == "constriction") & (df_obj[
                            "region_count"] == region_idx + 1)].index.tolist()
                    df_obj.loc[idx, [feat + " start"]] = feat_end

        # update transit time
        df_obj["transit time"] = df_obj["time end"] - df_obj["time start"]

        multi_con_stats = pd.concat([multi_con_stats, df_obj],
                                    ignore_index=True)
    for key in kwargs:
        if type(kwargs[key]) in (int, float, str):
            multi_con_stats[key] = kwargs[key]
        else:
            warnings.warn(f"Given keyword argument {key} is not scalar, it "
                          "will not be saved to the file!")
    multi_con_stats.to_csv(output_file, index=False, sep=";")
    return multi_con_stats


def get_stat_for_feature(df: pd.DataFrame,
                         feature: str = "transit time",
                         region_type: str = "constriction"):
    """Function to calculate statistics for the data exported with the function
    `export_data_of_multi_constriction`.

    Parameters
    ----------
    df: pd.DataFrame
        The data to calculate statistics for.
    feature: str
        Name of the feature for which statistics will be computed.
    region_type: str
        Either "constriction" or "channel".

    Returns
    -------
    stats_dict: dict of lists
        Mean, median and std of a given feature and region type. Each element
        of the dictionary is a list that contains as many values as there are
        region of the given type.
    """
    if feature not in df.keys():
        raise FeatureNotValidError(f"{feature} is not a valid feature in the "
                                   "given DataFrame.")
    df_region = df[df["region_type"] == region_type]
    region_count = df_region["region_type_occurance"].unique()
    stats_dict = {"mean": [], "median": [], "std": []}
    for region_nb in region_count:
        data = df_region[df_region["region_type_occurance"] == region_nb]
        stats_dict["mean"].append(np.nanmean(data[feature]))
        stats_dict["median"].append(np.nanmedian(data[feature]))
        stats_dict["std"].append(np.nanstd(data[feature]))
    return stats_dict


def get_filtered_object_numbers(tr_ds, filters=None):
    """Get a list of all objects that meet the following requirements.
    For analysis of multi-constriction experiments only!
    *For this to work, there should never be a constriction at the beginning of
    the ROI, objects should always be imaged first in either the inlet part
    or a straight channel part!*

    - Objects make it from before the first constriction (either channel or
      inlet part) to the last 10 % of the ROI
    - Only one object in the channel at the same time
    - if additional filters are given, the first event of an object has to
      lie in those filters (e.g. area filter).

    Parameters
    ----------
    tr_ds: TrackedDataSet
        The already tracked dataset
    filters: dict
        filters should look like this:
        filters = {"area_um": [10, 150],
                   "fl3_max": [300, 30000]}
    Returns
    -------
    good_objects
        A list of object numbers of all those objects that meet the filtering
        criteria and, if given, the conditions as set in filters.
    """
    if filters is None:
        filters = {}

    # start of first constriction (in flow direction) in um
    # and cells must make it through 90 % of the channel at least
    flow = tr_ds.get_flow_direction()
    ch_region_types = determine_region_type(tr_ds)
    region_idx = ch_region_types.index("constriction")
    if flow == 1:
        # to the right
        begin_first_constr = tr_ds.channel_regions[region_idx][
                                 0] * tr_ds.pixel_size
        ref_point_end = 0.9 * tr_ds.roi_size_x * tr_ds.pixel_size
    else:
        begin_first_constr = tr_ds.channel_regions[::-1][region_idx][
                                 -1] * tr_ds.pixel_size
        ref_point_end = 0.1 * tr_ds.roi_size_x * tr_ds.pixel_size

    bad_objects = []

    list_of_objcts = np.unique(tr_ds.dataset["object_number"][:])
    for obj_nr in list_of_objcts:
        obj_ind = np.nonzero(
            tr_ds.dataset["object_number"][:] == obj_nr)[0]
        obj_frames = tr_ds.dataset["frame"][obj_ind]
        list_of_double_frames = [fr for fr in obj_frames if
                                 list(tr_ds.dataset["frame"][:]).count(fr) > 1]

        # flow to the right
        if flow == 1:
            if np.min(tr_ds.dataset["pos_x"][obj_ind]) >= \
                begin_first_constr or np.max(
                    tr_ds.dataset["pos_x"][obj_ind]) <= ref_point_end:
                bad_objects.append(obj_nr)
        else:
            if np.min(tr_ds.dataset["pos_x"][obj_ind]) >= ref_point_end \
                or np.max(
                    tr_ds.dataset["pos_x"][obj_ind]) <= begin_first_constr:
                bad_objects.append(obj_nr)

        if list_of_double_frames:
            bad_objects.append(obj_nr)

        if filters:
            filters_met = []
            for feat in filters:
                feat_val = tr_ds.dataset[feat][obj_ind][0]
                if filters[feat][0] < feat_val < filters[feat][1]:
                    filters_met.append(True)
            if not len(filters_met) == len(filters):
                bad_objects.append(obj_nr)

    good_objects = [obj_nr for obj_nr in list_of_objcts if
                    obj_nr not in bad_objects]
    return good_objects


def determine_region_type(tracked_ds):
    """Based on the number of channel regions and whether there are inlet,
    channel and outlet region, this function assigns each part of the chip
    the respective type. This is needed to correctly determine the statistics.
    It considers the order of the channel regions to be ordered by value (which
    means the smallest values first, or from left to right in the image) and
    considers the flow direction when assigning the actual region types: The
    first element in the returned list describes the first channel region an
    object flows through in the chip.

    Parameters
    ----------
    tracked_ds
        Object of class `TrackedDataSet`. Has to have metadata about chip
        geometry, such as `inlet`, `channel`, `outlet` and `channel_regions`.

    Returns
    -------
    region_type : list[str]
        A list of strings. Each element describes the type of channel region
        for the regions described in `channel_regions`. One of the following:
        `inlet`, `channel`, `constriction`, `outlet`.
    """
    region_type = []
    mod = 1
    for i in range(tracked_ds.nb_channel_regions):
        if i == 0:
            if tracked_ds.inlet:
                region_type.append("inlet")
                mod = 0
            else:
                region_type.append("channel")
                mod = 1
        else:
            if i % 2 == mod:
                region_type.append("constriction")
            else:
                region_type.append("channel")
    if tracked_ds.outlet:
        region_type[-1] = "outlet"

    return region_type


def get_channel_regions(tracked_ds, *, region_type: str):
    """Returns a list of all channel regions for a given region type.

    Parameters
    ----------
    tracked_ds : TrackedDataSet
        The dataset for which all the regions of type `region_type` will be
        determined and returned.
    region_type : str
        The type of region for which all region coordinates shall be returned.

    Returns
    -------
    regions_for_analysis: list
        A list of lists. Each list contains the beginning and end of a
        certain channel region of type `region_type`.
        Unit is pixels, order is according to flow direction."""
    if isinstance(region_type, str) and region_type not in ["inlet",
                                                            "constriction",
                                                            "channel",
                                                            "outlet"]:
        raise ValueError("Unknown region type! Has to be either `inlet`,"
                         " `constriction`, `channel` or `outlet`.")

    present_regions = determine_region_type(tracked_ds)
    if tracked_ds.flow_direction == "left":
        channel_regions = tracked_ds.channel_regions[::-1]
    else:
        channel_regions = tracked_ds.channel_regions
    indices = np.where(np.array(present_regions) == region_type)[0]
    regions_for_analysis = []
    for idx in indices:
        regions_for_analysis.append(channel_regions[idx])
    return regions_for_analysis


def get_ellipse_features(contour):
    """Use skimage to fit an ellipse to a 2D contour.

    Parameters
    ----------
    contour: xy data of the contour (2D array)

    Returns
    -------
    tuple: 'center_x', 'center_y', 'a', 'b', 'angle'
    """
    a_points = np.array(contour)

    ell = EllipseModel()
    ell.estimate(a_points)
    return ell.params


def calc_ellipses(ds, show_progress=True):
    """Fit ellipses to all contours of a dataset. Creates a pandas.DataFrame.

    Parameters
    ----------
    ds: *RTDCDataSet*
        dclab rt-dc dataset
    show_progress: *bool*
            If True, a progress bar showing the progress for the dataset will
            be displayed.
    Returns
    -------
    DataFrame with ellipses
    (columns=['ell_cx', 'ell_cy', 'ell_major', 'ell_minor', 'ell_angle'])
    """
    if 'contour' not in ds.features:
        raise KeyError("'contour' feature not in dataset.")
    else:
        center_x = []
        center_y = []
        a_list = []
        b_list = []
        angle = []

        if show_progress:
            pbar = tqdm(range(len(ds)), desc="Calculating ellipses")
        else:
            pbar = range(len(ds))
        for ii in pbar:
            contour = ds["contour"][ii]
            xc, yc, a, b, theta = get_ellipse_features(contour)
            center_x.append(xc)
            center_y.append(yc)
            a_list.append(a)
            b_list.append(b)
            angle.append(theta)

        ellipses = pd.DataFrame(
            np.column_stack([center_x, center_y, a_list, b_list, angle]),
            columns=['ell_cx', 'ell_cy', 'ell_major', 'ell_minor', 'ell_angle']
            )
        return ellipses


def calc_ellipse_from_scalar_export(ds_scalar, ds_original,
                                    show_progress=True):
    """Add values for fitted ellipses to the dataset based on the contour data
    in the mathing original dataset.

    Parameters:
    ----------
    ds_scalar: *dclab.dataset*
        Tracked dataset that has tracking results in feature
        `object_number` but not features `'image'` and `'contour'`.
    ds_original: *dclab.dataset*
        Original dataset with images and contours.
    show_progress: *bool*
            If True, a progress bar showing the progress for the dataset will
            be displayed.
    Returns
    -------
    RTDCDataSet: Scalar dataset with added ellipse features.

    Raises
    ------
    ValueError
        If dataset does not contain features `'image'` and `'contour'`.
    """

    ellipse_feats = ['ell_cx', 'ell_cy', 'ell_major', 'ell_minor', 'ell_angle']
    # register necessary temporary features
    for feat in ellipse_feats:
        deregister_temporary_feature(feat)
        dclab.register_temporary_feature(feature=feat)

    if not ('image' in ds_original.features
            and 'contour' in ds_original.features):
        err_msg = "Original dataset needs to include images and contours."
        raise ValueError(err_msg)

    index_scalar = ds_scalar['index_online']
    center_x = np.empty_like(index_scalar)
    center_y = np.empty_like(index_scalar)
    a_list = np.empty_like(index_scalar)
    b_list = np.empty_like(index_scalar)
    angle = np.empty_like(index_scalar)

    if show_progress:
        pbar = enumerate(tqdm(index_scalar, desc="Calculating ellipses"))
    else:
        pbar = enumerate(index_scalar)
    for ii, idx_sca in pbar:
        ii_org = np.nonzero(ds_original['index_online'] == idx_sca)[0][0]
        contour = ds_original['contour'][ii_org]
        xc, yc, a, b, theta = get_ellipse_features(contour)
        center_x[ii] = xc
        center_y[ii] = yc
        a_list[ii] = a
        b_list[ii] = b
        angle[ii] = theta

    df_ellipse = pd.DataFrame(
        np.column_stack([center_x, center_y, a_list, b_list, angle]),
        columns=ellipse_feats
        )

    for feat in ellipse_feats:
        data = df_ellipse[feat].to_numpy()
        dclab.set_temporary_feature(rtdc_ds=ds_scalar,
                                    feature=feat,
                                    data=data.copy())
    return ds_scalar
