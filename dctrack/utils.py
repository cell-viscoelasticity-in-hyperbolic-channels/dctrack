
class FeatureDoesNotExistError(BaseException):
    pass


# features that need be included in a dataset to perform object tracking
TRACKING_FEATURES = ["pos_x", "frame", "index"]
# features that need to be registered for tracking
TRACK_TEMPORARY_FEATURES = [
    "object_number", "prediction_zone_start", "prediction_zone_end",
    "velocity", "time_channel", "time_inlet", "time_outlet",
]
# features that need to be registered for MMM tracking
TRACK_MMM_TEMPORARY_FEATURES = [
    "path_index", "object_number", "prediction_zone_start",
    "prediction_zone_end", "obj_area_before", "obj_area_after",
    "obj_deform_before", "obj_deform_after"
]

feature_labels = {
    "object_number": "Object nb.",
    "prediction_zone_start": "Pred. zone start [µm]",
    "prediction_zone_end": "Pred. zone end [µm]",
    "velocity": "Velocity [m/s]",
    "time_channel": "Time in channel [s]",
    "inlet_channel": "Time in inlet [s]",
    "outlet_channel": "Time in outlet [s]"
}

mmm_feature_labels = {
    "path_index": "Path index",
    "object_number": "Object nb.",
    "prediction_zone_start": "Pred. zone start [µm]",
    "prediction_zone_end": "Pred. zone end [µm]",
    "obj_area_before": "Area before channel [µm²]",
    "area before [um^2]": "Area before channel [µm²]",
    "obj_area_after": "Area after channel [µm²]",
    "area after [um^2]": "Area after channel [µm²]",
    "obj_deform_before": "Deformation before channel",
    "deform before [a.u.]": "Deformation before channel",
    "obj_deform_after": "Deformation after channel",
    "deform after [a.u.]": "Deformation after channel",
    "transit time [s]": "Transit time [s]",
    "avg velocity [mm/s]": "Avg. velocity [mm/s]",
}

# metadata needed for tracking
TRACK_METADATA_DEFAULTS = {
    "inlet": True,
    "channel": True,
    "outlet": False,
    "multiple_constrictions": False,
    "nb_channel_regions": 2,
    "flow_direction": "left",
    "tolerance_window": 25,
    "channel_regions": [[0, 10], [10, 20]],
    "channel_x_min": 0,
    "channel_x_max": 10,
}

# metadata needed for MMM tracking
TRACK_MMM_METADATA_DEFAULTS = {
    "flow_direction": "right",
    "pred_zone_size": 6,
    "roi": [0, 0, 10, 10],
    "channel_path": [],
    "window_size": 8,
    "frames_disappear": 3
}

# possible frame rates for fluorescence according to Shape-In2 manual
# version 1.4
possible_fl_fps = [10, 16, 25, 32, 40, 50, 64, 80, 100, 125, 160, 200, 250,
                   320, 400, 500, 625, 800, 1000, 1250, 1600, 2000, 2500,
                   3125, 3200, 4000]
# position of light sheet in pixels, setups 2 and 3
device_info = {
    # device 2
    "ZMDD-AcC-04682c-208b19": {"pos_light_sheet": 629, "roi_pos_y": 490},
    # device 3
    "ZMDD-AcC-774f84-3da488": {"pos_light_sheet": 533, "roi_pos_y": 470}
}


def create_user_config_section(rtdc_ds):
    """Create an empty user config section in dclab dataset"""
    rtdc_ds.config["user"] = {}


def populate_user_config_section(rtdc_ds, mode="default"):
    """Populate the user config section with defaults if they don't exist.

    Parameters
    ----------
    rtdc_ds: RTDCBase
    mode: str
        "default" for tracking in a straight channel, "mmm" for tracking in a
        channel with serpentines.
    """
    if mode == "default":
        for key in TRACK_METADATA_DEFAULTS:
            if key not in rtdc_ds.config["user"]:
                rtdc_ds.config["user"][key] = TRACK_METADATA_DEFAULTS[key]
    elif mode == "mmm":
        for key in TRACK_MMM_METADATA_DEFAULTS:
            if key not in rtdc_ds.config["user"]:
                rtdc_ds.config["user"][key] = TRACK_MMM_METADATA_DEFAULTS[key]


def check_ds_for_feat(dataset, feature: str):
    """Check if the dataset contains the feature `"feature"` and raise
    FeatureDoesNotExistError if not.
    """
    if feature not in dataset.features:
        err_msg = f"The dataset does not contain feature `{feature}`."
        if feature in TRACK_TEMPORARY_FEATURES:
            err_msg = err_msg + " You need to track the dataset first."
        raise FeatureDoesNotExistError(err_msg)
