import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import profile_line
from scipy.ndimage import gaussian_filter


def find_channel_regions(img, inlet=True, channel=True, outlet=False,
                         multiple_constrictions=False, n=2):
    """This function is used by TrackedDataSet, one just has to set the
    geometry of their channel.

    Parmeters
    ---------
    img : 2d array-like
    inlet : bool, default True
    channel : bool, default True
    outlet : bool, default False
    multiple_constrictions : bool, default False
        If `multiple_constrictions=True`, then it will ignore the values for
        inlet, channel and outlet.
    n : int
        Number of channel regions the function is supposed to find.
        Example: a chip with several constrictions, but only the inlet and the
        first constriction and parts of the second constriction were imaged.
        The following values should be found by the algorithm:
        begin of image, begin of channel, begin of first constriction,
        end of first constriction, begin of second constriction, end of image.
        In this example, there would be 5 channel regions defined by 6
        positions in the channel.

    Returns
    -------
    channel_regions : list of lists

    Raises
    ------
    ValueError
        If the combination of geometries is not recognised. See the tests for
        details.

    See Also
    --------
    calculate_channel_from_inflection_point : Detects the channel

    """
    if not channel:
        raise ValueError(
            "The channel has been set to False, I don't know what to do. "
            "Please set it to True.")

    channel_regions = []
    if multiple_constrictions:
        if n < 2:
            raise UserWarning(f"Number of channel regions n = {n} seems to be"
                              f" too small for a multiple constriction "
                              f"geometry!")
        # the algorithm has to find several positions in the image, while
        # beginning and end of the image will be added manually
        channel_regions = [0, img.shape[1]]
        xpos = calculate_channel_from_inflection_point(
            img=img, linewidth=5, n=n - 1, sigma=3)
        channel_regions.extend(list(xpos))
        channel_regions = sorted(channel_regions)
    elif inlet and not outlet or outlet and not inlet:
        # we look for three positions: the channel start/end and the borders
        # of the image
        if not n == 2:
            raise ValueError(f"There should be two channel regions instead "
                             f"of {n}!")
        channel_regions = [0, img.shape[1]]
        xpos = calculate_channel_from_inflection_point(
            img=img, linewidth=5, n=1, sigma=3)
        channel_regions.extend(list(xpos))
        channel_regions = sorted(channel_regions)
    elif inlet and outlet:
        # we look for four positions: the channel start + end and the borders
        # of the image
        if not n == 3:
            raise ValueError(f"There should be three channel regions instead "
                             f"of {n}!")
        channel_regions = [0, img.shape[1]]
        xpos = calculate_channel_from_inflection_point(
            img=img, linewidth=5, n=2, sigma=3)
        channel_regions.extend(list(xpos))
        channel_regions = sorted(channel_regions)
    elif channel and not inlet and not outlet:
        # if only one channel, then whole image is channel region
        if n == 1:
            print(f"You specified n = {n} channel regions, but there is only "
                  f"one channel region according to the metadata. Will "
                  f"ignore n for now.")
        channel_regions = [0, img.shape[1]]
    else:
        print("It seems you have an unknown geometry combination that is not "
              "recognised. This will prompt the manual GUI.")

    # channel_regions should be list of lists
    channel_regions = [[channel_regions[i], channel_regions[i + 1]] for i in
                       range(len(channel_regions) - 1)]
    return channel_regions


def calculate_channel_from_inflection_point(
        img, n=10, min_separation=20, linewidth=3, sigma=2,
        plot=False, reduce_func=None, **kwargs):
    """Automatically detects the x position of the entrance to the channel.

    Parameters
    ---------
    img : 2d array-like
    n : int
        Set the number of channel positions that should be returned.
        Basically all positions where a new part of the channel
        (inlet/channel/constriction/outlet) begins or ends.
    min_separation : int
        Minimum peak separation in pixels between peaks.
    linewidth : int
        Width of the scan, perpendicular to the line.
    sigma : int, optional
        Standard deviation for the gaussian kernel. Helps with noisey line
        profiles. See `scipy.ndimage.gaussian_filter` for details.
    plot : bool
        If set to True, a summary plot of the channel detection will be
        displayed.
    reduce_func : callable, optional
        Function used to calculate the aggregation of pixel values
        perpendicular to the profile_line direction when `linewidth` > 1.
        Unlike its behaviour in `skimage.measure.profile_line`,
        if set to None, then `np.nanmean` will be used as to handle edge NaNs.
    **kwargs : dict, optional
        Extra arguments to `profile_line`: refer to
        `skimage.measure.profile_line` for details.

    Returns
    -------
    n_channel_positions : 1D array

    Raises
    ------
    ValueError
        If `min_separation` is larger than the image width (img.shape[1]).
    UserWarning
        If one of the peaks is located at index 0

    See Also
    --------
    find_channel_regions : Handles channel zone type and endpoints
    find_separated_profile_positions : Handles peak separation
    skimage.measure.profile_line : Trace a line profile
    scipy.ndimage.gaussian_filter : Apply a Gaussian blur

    Notes
    -----
    Traces a line intensity profile over the channel walls to the
    in(out)let. Uses the derivative to detect the likely `channel_zone` ROI(s)
    x-positions. These positions are located at the line profile inflection
    point.

    """
    walls = find_channel_walls(img, linewidth=4)
    # coordinates of the start point of the scan line (y, x)
    src_1 = (walls[0], 0)
    src_2 = (walls[1], 0)
    # coordinates of the end point of the scan line (y, x)
    dst_1 = (walls[0], img.shape[1])
    dst_2 = (walls[1], img.shape[1])
    x_difference = dst_1[1] - src_1[1]

    if isinstance(sigma, int):
        # could prob just apply gaussian to the profile, would be faster.
        img = img.astype(float)
        img = gaussian_filter(img, sigma=sigma)

    # coords are y, x
    if reduce_func is None:
        reduce_func = np.nanmean

    profile_output = profile_line(image=img, src=src_1, dst=dst_1,
                                  mode='nearest', linewidth=linewidth,
                                  reduce_func=reduce_func, **kwargs)
    prfl_df = np.diff(profile_output, n=1, axis=0)
    # take absolute values of the differential
    abs_prfl_df = np.absolute(prfl_df)
    profile_output_2 = profile_line(image=img, src=src_2, dst=dst_2,
                                    mode='nearest', linewidth=linewidth,
                                    reduce_func=reduce_func, **kwargs)
    prfl_df_2 = np.diff(profile_output_2, n=1, axis=0)
    # take absolute values of the differential
    abs_prfl_df_2 = np.absolute(prfl_df_2)
    abs_prfl_sum = abs_prfl_df + abs_prfl_df_2

    n_profile_positions = find_separated_profile_positions(
        abs_prfl_sum, n, min_separation)
    n_channel_positions = n_profile_positions.copy()

    if plot:
        # create coords for graphical img line marking the x channel boundary
        fig, axes = plt.subplots(nrows=3, figsize=(16, 9))
        ax1, ax2, ax3 = axes
        # plot image with src and dst points
        ax1.imshow(img)
        ax1.scatter(x=src_1[1], y=src_1[0], c='r')
        ax1.scatter(x=dst_1[1], y=dst_1[0], c='orange')

        # plot line profile with calculated differential and inflection point
        ax2.plot(profile_output, c='b')
        ax2.scatter(x=0, y=0, c='r')
        ax2.scatter(x=x_difference, y=0, c='orange')
        ax2.plot(abs_prfl_df, c='r')
        ax2.scatter(n_profile_positions, abs_prfl_df[n_profile_positions],
                    c='cyan')
        ax2.vlines(n_profile_positions, ymin=profile_output.max(),
                   ymax=abs_prfl_df.min(), color='cyan')

        # plot image with found channel position
        ax3.imshow(img)
        ax3.scatter(x=src_1[1], y=src_1[0], c='r')
        ax3.scatter(x=dst_1[1], y=dst_1[0], c='orange')
        ax3.vlines(n_channel_positions, ymin=img.shape[0] * 1 / 4,
                   ymax=img.shape[0] * 3 / 4, color='cyan')
        fig.suptitle(
            f"Linewidth {linewidth}, \n channel_x_pos {n_channel_positions}")

    return n_channel_positions


def find_separated_profile_positions(arr, n, min_separation):
    """Detect `n` prominent peaks in a 1D array, separated by `min_separation`

    Parmeters
    ---------
    arr : 1d array-like
    n : int
        Set the number of channel positions that should be returned.
    min_separation : int
        Minimum peak separation in pixels between peaks.

    Returns
    -------
    n_channel_positions : 1D array

    Raises
    ------
    ValueError
        If `min_separation` is larger than the image width (img.shape[1]).
    UserWarning
        If one of the peaks is located at index 0

    See Also
    --------
    calculate_channel_from_inflection_point : Calculates profile differential

    """
    h_ms = int(min_separation)
    if len(arr) < h_ms:
        raise ValueError(
            "`min_separation` should be smaller than the image (`arr`) length."
            f"The `arr` you have provided is length {len(arr)}, and "
            f"the `min_separation` is length {min_separation}.")

    n_channel_positions = []
    arr_c = arr.copy().astype(float)
    for i in range(n):
        idx_of_max = np.where(arr_c == np.max(arr_c))[0][0]
        if idx_of_max == 0:
            raise UserWarning(
                "The maximum peak value for one of the peaks may not be "
                "correct, as its position is at 0. To fix this, reduce "
                "either `n` or `min_separation`.")

        n_channel_positions.append(idx_of_max)
        slice_start = idx_of_max - h_ms
        slice_end = idx_of_max + 1 + h_ms
        # handle negative indexes
        if slice_start < 0:
            slice_start = 0
        arr_c[slice_start:slice_end] = 0.0

    n_channel_positions = np.sort(np.array(n_channel_positions))

    if n_channel_positions.ndim == 1:
        return n_channel_positions
    else:
        return n_channel_positions.sort(axis=0)


def find_channel_walls(img, linewidth=3) -> list:
    """Use the function profile_line from skimage.measure to find the
    channel walls.

    Parameters
    ---------
    img : 2d array-like
        The image to be processed
    linewidth : int
        Width of the scan, perpendicular to the line.

    Return
    -------
    channel : list
        Position of the channel walls, one value for each wall.
    """
    # relative positions along the channel
    pos_list = [0.40, 0.45, 0.50, 0.55, 0.60]
    wall_pos_top = []
    wall_pos_bottom = []
    # coords are y, x
    for pos in pos_list:
        src = (1, int(pos * img.shape[1]))
        dst = (int(img.shape[0]), int(pos * img.shape[1]))
        profile_output = profile_line(image=img, src=src, dst=dst,
                                      linewidth=linewidth, cval=np.nan,
                                      reduce_func=np.nanmean)
        prfl_df = np.diff(profile_output, n=1, axis=0)
        # take absolute values of the differential
        abs_prfl_df = np.absolute(prfl_df)
        abs_prfl_df[0] = 0
        abs_prfl_df[-1] = 0

        positions = find_separated_profile_positions(
            abs_prfl_df, 2, min_separation=2)
        wall_pos_top.extend([positions[0]])
        wall_pos_bottom.extend([positions[1]])
    return [np.median(wall_pos_top), np.median(wall_pos_bottom)]


def get_end_of_inlet_channel(img, xpos):
    assert len(xpos) == 1, "inlet channel should have only one xpos"
    # xpos found on left side
    if img.shape[1] - xpos[0] > img.shape[1] / 2:
        end_of_channel = img.shape[1]
    # xpos found on right side
    else:
        end_of_channel = 0
    return end_of_channel


def plot_channel_regions_lines(image, regions, auto_close=False):
    """Convenience function for plotting the position of the channel regions

    Parameters
    ----------
    image : 2D ndarray
    regions : list of lists
        List of lists where the length of each sub-list is two. Each sub-list
        represents the start and end of a channel region (between
        constrictions).
    auto_close : bool
        If set to True, the figure will not display. Used for tests.
    """
    if auto_close:
        block = False
    else:
        block = True
    if not isinstance(regions, list):
        raise TypeError("`regions` must be of type list, instead got "
                        f"{type(regions)}")
    for region in regions:
        if not isinstance(region, list):
            raise TypeError("The list `regions` must contain lists, instead "
                            f"got {type(region)}")
        if len(region) != 2:
            raise ValueError("The list `regions` must contain lists of "
                             "length 2, instead got lists of length "
                             f"{len(region)}")

    plt.figure()
    plt.imshow(image, cmap='gray')
    for region in regions:
        plt.axvline(region[0], color='tab:red')
        plt.axvline(region[1], color='tab:red')
    plt.show(block=block)
