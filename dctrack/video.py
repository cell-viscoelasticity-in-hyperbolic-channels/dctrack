
import pathlib
import numpy as np
import cv2
import matplotlib as mpl
from imageio import get_writer
from tqdm.autonotebook import tqdm
import dclab
from dclab.rtdc_dataset.feat_temp import deregister_temporary_feature

# font used for writing object numbers
font = cv2.FONT_HERSHEY_SIMPLEX


def extend_img(img, macro_block_size=16):
    """
    Add black pixels at right and bottom border of image, so that
    xy-size % macro_block_size = 0

    Parameters
    ----------
    img: *ndarray*
        2D or 3D np.array of image pixel values
    macro_block_size: *int*
        Integer value that image.size will be made dividable to

    Returns
    -------
    frame: *ndarray*
        img + black pixels at right and bottom border
    """
    if len(img.shape) == 2:
        x_size = img[0, :].size
        y_size = img[:, 0].size
    if len(img.shape) == 3:
        x_size = img[0, :, 0].size
        y_size = img[:, 0, 0].size

    if x_size % macro_block_size == 0:
        right_add = 0
    else:
        right_add = ((x_size // macro_block_size + 1) * macro_block_size
                     - x_size)

    if y_size % macro_block_size == 0:
        bottom_add = 0
    else:
        bottom_add = ((y_size // macro_block_size + 1) * macro_block_size
                      - y_size)

    if bottom_add != 0 or right_add != 0:
        frame = cv2.copyMakeBorder(img, 0, bottom_add, 0, right_add,
                                   borderType=cv2.BORDER_CONSTANT)
        return frame
    else:
        return img


def video_write(dataset, first_frame=0, last_frame=0,
                save_path="tracked_objects.avi",
                macro_block_size=16,
                draw_zones=False):
    """Write video showing which object number was assigned to each detected
    event. It is necessary to have filtered and unfiltered dataset as input
    because video images should be from unfiltered dataset while contours
    should be taken from the filtered dataset. If draw_zones=True, additionally
    the zones where the objects were predicted for each frame are drawn.

    Parameters
    ----------
    dataset: *dclab.dataset*
        Tracked dataset that has tracking results in feature
        `object_number`.
    first_frame: *int, optional*
        First frame in vidfile to analyze. Default is first_frame = 0.
    last_frame: *int, optional*
        Last frame in vidfile to analyze. If LastFrame = 0 (default) all
        video frames will be analyzed
    save_path: *string, optional*
        File path where finished video will be saved. If not specified, the
        default path is "./Offline/test_avi.avi"
    macro_block_size: *int*
        Parameter that is used in ffmpeg for video coding.
        macro_block_size=16 indicates that every image dimension is
        dividable by 16. The images will be adapted to this size by adding
        black pixels.
        h264 and h265 codec need macro_block_size=16.
    draw_zones: *bool*
        If True then the zones where an object is predicted are drawn.
    filtered : *bool*
        If True use the filtered dataset to create the video

    Returns
    -------
    *None*
        Creates .avi file at `save_path`.
    """
    pixel_size = dataset.config['imaging']['pixel size']

    # create folder tree of path if the folder does not exist
    path = pathlib.Path(save_path)
    video_dir = path.parent
    if not video_dir.is_dir():
        video_dir.mkdir(parents=True)

    vidwriter = get_writer(save_path, fps=60, quality=8,
                           macro_block_size=macro_block_size)

    # default colors of matplotlib. * 255 to convert to uint8 for use with cv2
    colors = np.array(mpl.colormaps['tab10'].colors) * 255
    object_index = np.array(dataset['object_number'], dtype=np.int32)
    pos_x = dataset['pos_x']
    pos_y = dataset['pos_y']
    pos_x_pixel = np.array(pos_x / pixel_size, dtype=np.int32)
    pos_y_pixel = np.array(pos_y / pixel_size, dtype=np.int32)

    if draw_zones:
        zone_starts = np.array(dataset['prediction_zone_start']
                               / pixel_size, dtype=np.int32)
        zone_ends = np.array(dataset['prediction_zone_end']
                             / pixel_size, dtype=np.int32)

    if last_frame == 0 or last_frame > len(dataset['frame']):
        last_frame = len(dataset['frame'])
    if first_frame < 0:
        first_frame = 0

    # don't use duplicates of frames in video --> unique frames only
    frames = dataset['frame'][:]
    frames_unique, idx_frames_unique = np.unique(frames, return_index=True)
    # add offset from first_frame to unique indices
    # idx_frames_unique += first_frame

    pbar = tqdm(idx_frames_unique[first_frame:last_frame],
                desc="Writing video")
    for ii in pbar:
        # try:
        # get image from dataset
        image = dataset['image'][ii]
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        frame = dataset['frame'][ii]

        # find index of all frames in `dataset`
        frame_ind = np.nonzero(dataset['frame'] == frame)[0]
        objects = object_index[frame_ind]
        pos_x_pix_frame = pos_x_pixel[frame_ind]
        pos_y_pix_frame = pos_y_pixel[frame_ind]
        # draw prediction zones on image copy first, then overlay a
        # transparent version over the tracked contour image at the end
        if draw_zones:
            zone_starts_frame = zone_starts[frame_ind]
            zone_ends_frame = zone_ends[frame_ind]
            image_out = image.copy()
            height = image_out.shape[0]

        contours = [dataset['contour'][i] for i in frame_ind]

        # write video to see how detection worked out
        if len(frame_ind) > 0:
            for cc in range(len(frame_ind)):
                # get object index
                obj_ind = objects[cc]
                # get contour object in format suited for
                # cv2.drawContours
                # dtype=int necessary for data from tdms files
                cont_ii = [np.array(contours[cc], dtype=int)]
                # draw closed colored contour around object
                color_ii = colors[obj_ind % len(colors)]
                cv2.drawContours(image, cont_ii, -1,
                                 color_ii, -1)
                # write object index above object
                cv2.putText(image, str(obj_ind),
                            (int(pos_x_pix_frame[cc]) - 5,
                             int(pos_y_pix_frame[cc])),
                            font, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
                if draw_zones:
                    pt1 = (zone_starts_frame[cc], 0)
                    pt2 = (zone_ends_frame[cc], height)
                    if isinstance(pt1[0], (int, np.integer)) and \
                            isinstance(pt2[0], (int, np.integer)):
                        if pt1[0] > 0 or pt2[0] > 0:
                            cv2.rectangle(image_out,
                                          pt1,
                                          pt2,
                                          color_ii, -1)
                    cv2.putText(image_out, str(obj_ind),
                                (int(pos_x_pix_frame[cc]) - 5,
                                 int(pos_y_pix_frame[cc])),
                                font, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
        if draw_zones:
            alpha = 0.3
            cv2.addWeighted(image_out, alpha, image, 1 - alpha, 0, image)

        # extend image with black pixels, so each dimension is dividable by
        # macro_block_size; needed for video codecs to work correctly
        image = extend_img(image, macro_block_size=macro_block_size)

        vidwriter.append_data(image)

    vidwriter.close()


def video_write_from_scalar_export(dataset_scalar, dataset_original,
                                   first_frame=0, last_frame=0,
                                   save_path="tracked_objects.avi",
                                   macro_block_size=16,
                                   draw_zones=False
                                   ):
    """Write a video from an exported tracked dataset only containing scalar
    data. Need to pass the original dataset with images and contours to write
    the video.

    Parameters:
    ----------
    ds_scalar: *dclab.dataset*
        Tracked dataset that has tracking results in feature
        `object_number` but not features `'image'` and `'contour'`.
    ds_original: *dclab.dataset*
        Original dataset with images and contours.
    first_frame: *int, optional*
        First frame in vidfile to analyze. Default is first_frame = 0.
    last_frame: *int, optional*
        Last frame in vidfile to analyze. If LastFrame = 0 (default) all
        video frames will be analyzed
    save_path: *string, optional*
        File path where finished video will be saved. If not specified, the
        default path is "./Offline/test_avi.avi"
    macro_block_size: *int*
        Parameter that is used in ffmpeg for video coding.
        macro_block_size=16 indicates that every image dimension is
        dividable by 16. The images will be adapted to this size by adding
        black pixels.
        h264 ad h265 codec need macro_block_size=16.
    draw_zones: *bool*
        If True then the zones where an object is predicted are drawn.

    Returns
    -------
    *None*
        Creates avifile at `save_path`.

    Raises
    ------
    ValueError
        If dataset does not contain features `'image'` and `'contour'`.
    """

    # register necessary temporary features
    for feat in ['object_number',
                 'prediction_zone_start',
                 'prediction_zone_end']:
        deregister_temporary_feature(feat)
        dclab.register_temporary_feature(feature=feat)

    pixel_size = dataset_scalar.config['imaging']['pixel size']

    if not ('image' in dataset_original.features
            and 'contour' in dataset_original.features):
        err_msg = "Original dataset needs to include images and contours."
        raise ValueError(err_msg)

    # create folder tree of path if the folder does not exist
    path = pathlib.Path(save_path)
    video_dir = path.parent
    if not video_dir.is_dir():
        video_dir.mkdir(parents=True)

    vidwriter = get_writer(save_path, fps=60, quality=8,
                           macro_block_size=macro_block_size)

    # default colors of matplotlib. * 255 to convert to uint8 for use with cv2
    colors = np.array(mpl.colormaps['tab10'].colors) * 255
    object_index = np.array(dataset_scalar['object_number'], dtype=np.int32)
    pos_x = dataset_scalar['pos_x']
    pos_y = dataset_scalar['pos_y']
    pos_x_pixel = np.array(pos_x / pixel_size, dtype=np.int32)
    pos_y_pixel = np.array(pos_y / pixel_size, dtype=np.int32)

    if draw_zones:
        zone_starts = np.array(dataset_scalar['prediction_zone_start']
                               / pixel_size, dtype=np.int32)
        zone_ends = np.array(dataset_scalar['prediction_zone_end']
                             / pixel_size, dtype=np.int32)

    if last_frame == 0 or last_frame > len(dataset_scalar['frame']):
        last_frame = len(dataset_scalar['frame'])
    if first_frame < 0:
        first_frame = 0

    # don't use duplicates of frames in video --> unique frames only
    frames = dataset_scalar['frame'][first_frame:last_frame]
    frames_unique, idx_frames_unique = np.unique(frames, return_index=True)
    # add offset from first_frame to unique indices
    idx_frames_unique += first_frame

    pbar = tqdm(idx_frames_unique, desc="Writing video")
    for ii in pbar:
        # try:
        # get image from dataset
        image = dataset_original['image'][ii]
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        frame = dataset_scalar['frame'][ii]

        # find index of all frames in `dataset`
        frame_ind = np.nonzero(dataset_scalar['frame'] == frame)[0]
        objects = object_index[frame_ind]
        pos_x_pix_frame = pos_x_pixel[frame_ind]
        pos_y_pix_frame = pos_y_pixel[frame_ind]
        # draw prediction zones on image copy first, then overlay a
        # transparent version over the tracked contour image at the end
        if draw_zones:
            zone_starts_frame = zone_starts[frame_ind]
            zone_ends_frame = zone_ends[frame_ind]
            image_out = image.copy()
            height = image_out.shape[0]

        contours = [dataset_original['contour'][i] for i in frame_ind]

        # write video to see how detection worked out
        if len(frame_ind) > 0:
            for cc in range(len(frame_ind)):
                # get object index
                obj_ind = objects[cc]
                # get contour object in format suited for
                # cv2.drawContours
                # dtype=int necessary for data from tdms files
                cont_ii = [np.array(contours[cc], dtype=int)]
                # draw closed colored contour around object
                color_ii = colors[obj_ind % len(colors)]
                cv2.drawContours(image, cont_ii, -1,
                                 color_ii, -1)
                # write object index above object
                cv2.putText(image, str(obj_ind),
                            (int(pos_x_pix_frame[cc]) - 5,
                             int(pos_y_pix_frame[cc])),
                            font, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
                if draw_zones:
                    pt1 = (zone_starts_frame[cc], 0)
                    pt2 = (zone_ends_frame[cc], height)
                    if isinstance(pt1[0], (int, np.integer)) and \
                            isinstance(pt2[0], (int, np.integer)):
                        if pt1[0] > 0 or pt2[0] > 0:
                            cv2.rectangle(image_out,
                                          pt1,
                                          pt2,
                                          color_ii, -1)
                    cv2.putText(image_out, str(obj_ind),
                                (int(pos_x_pix_frame[cc]) - 5,
                                 int(pos_y_pix_frame[cc])),
                                font, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
        if draw_zones:
            alpha = 0.3
            cv2.addWeighted(image_out, alpha, image, 1 - alpha, 0, image)

        # extend image with black pixels, so each dimension is dividable by
        # macro_block_size; needed for video codecs to work correctly
        image = extend_img(image, macro_block_size=macro_block_size)

        vidwriter.append_data(image)

    vidwriter.close()


def video_write_no_annotations(dataset, first_frame=0, last_frame=0,
                               save_path="dataset_as_video.avi",
                               macro_block_size=16):
    """Write the dataset's event images to a video.

    Parameters
    ----------
    dataset: *dclab.dataset*
        Tracked dataset with event images.
    first_frame: *int, optional*
        First frame in vidfile to analyze. Default is first_frame = 0.
    last_frame: *int, optional*
        Last frame in vidfile to analyze. If LastFrame = 0 (default) all
        video frames will be analyzed
    save_path: *string, optional*
        File path where finished video will be saved. If not specified, the
        default path is "./dataset_as_video.avi"
    macro_block_size: *int*
        Parameter that is used in ffmpeg for video coding.
        macro_block_size=16 indicates that every image dimension is
        dividable by 16. The images will be adapted to this size by adding
        black pixels.
        h264 and h265 codec need macro_block_size=16.

    Returns
    -------
    *None*
        Creates .avi file at `save_path`.
    """
    # create folder tree of path if the folder does not exist
    save_path = pathlib.Path(save_path)
    if not save_path.is_file:
        save_path / "dataset_as_video.avi"
    video_dir = save_path.parent
    if not video_dir.is_dir():
        video_dir.mkdir(parents=True)

    if last_frame == 0 or last_frame > len(dataset['frame']):
        last_frame = len(dataset['frame'])
    if first_frame < 0:
        first_frame = 0

    vidwriter = get_writer(save_path, mode='I', fps=30, quality=9,
                           codec='rawvideo',
                           macro_block_size=macro_block_size)

    frames_unique, idx_frames_unique = np.unique(dataset['frame'][:],
                                                 return_index=True)
    pbar = tqdm(idx_frames_unique[first_frame:last_frame],
                desc="Writing video")
    for ii in pbar:
        image = dataset['image'][ii]
        image = extend_img(image, macro_block_size=macro_block_size)
        vidwriter.append_data(image)

    vidwriter.close()
