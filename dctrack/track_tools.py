
import numpy as np
from tqdm.autonotebook import tqdm


def get_velocity(dataset):
    """Compute the velocities between two frames for each object. Unit is m/s

    Parameters
    ----------
    dataset : RTDCBase object
        RT-DC dataset read in with dclab

    Returns
    -------
    velocities: 1d numpy array
        Velocities for each object and event.
    """
    frame_rate = dataset.config['imaging']['frame rate']
    # time per dataframe
    dt = 1 / frame_rate
    velocities = np.full(len(dataset), np.nan)

    # find all object indices
    obj_ind_unique = np.unique(dataset['object_number'])
    # exclude nans
    obj_ind_unique = obj_ind_unique[~np.isnan(obj_ind_unique)]

    pbar = tqdm(obj_ind_unique, desc="Computing velocities")
    for ii in pbar:
        obj_ind = dataset['object_number'] == ii
        # need to improve the precision of the time value
        # important for large time values
        times = np.array(dataset['frame'][obj_ind], dtype=np.float_) * dt
        pos_x = dataset['pos_x'][obj_ind]
        # velocity in m/s
        velocity = np.diff(pos_x * 1e-6) / np.diff(times)
        # np.diff creates object with len(pos_x) - 1
        # velocity cannot be assigned to frame 0
        velocity = np.insert(velocity, 0, np.nan)
        velocities[obj_ind] = velocity

    return velocities


def get_time_after_x(dataset, x_reference, flow):
    """Compute the time after passing position `x_reference` in the
    channel. `x_reference` is in µm, returned time is in seconds.

    Parameters
    ----------
    dataset : RTDCBase or pandas.DataFrame object
        RT-DC dataset or pandas DataFrame read in with dclab. Needs to have
        been tracked already.
    x_reference : float
        x-value in um to compute the time after
    flow : int
        Number that indicates the flow direction. Must be in [-1,1]

    Returns
    -------
    times_x_reference: 1d numpy array
        The computed times in seconds in reference to `x_reference`
    """
    # find all object indices
    obj_ind_unique = np.unique(dataset['object_number'])
    # exclude nans
    obj_ind_unique = obj_ind_unique[~np.isnan(obj_ind_unique)]

    times_x_reference = np.full(len(dataset), np.nan)

    for ii in obj_ind_unique:
        obj_ind = dataset['object_number'] == ii
        times = dataset['time'][obj_ind]
        pos_x = dataset['pos_x'][obj_ind]
        velocity = dataset['velocity'][obj_ind]

        # find events that passed the reference x
        after_x_ref = flow * pos_x > flow * x_reference
        if np.any(after_x_ref):
            times_after_x = times[after_x_ref]
            pos_x_after_x = pos_x[after_x_ref]
            velocity_after_x = velocity[after_x_ref]
            # first frame tracked after `x_reference`
            ii_first_after_x = np.argmin(flow * pos_x_after_x)
            time_first_after_x = times_after_x[ii_first_after_x]
            pos_x_first_after_x = pos_x_after_x[ii_first_after_x]
            velocity_first_after_x = (velocity_after_x[ii_first_after_x]
                                      * 1e6)  # same unit as pos_x
            # if `velocity_first_after_x` is a nan value, take first none-nan
            # value in array
            if np.isnan(velocity_first_after_x):
                isnan = np.isnan(velocity_after_x)
                velocity_no_nans = velocity_after_x[~isnan]
                if velocity_no_nans.size > 0:
                    velocity_first_after_x = velocity_no_nans[0] * 1e6

            # time at `x_reference` based on first velocity captured after
            # `x_reference` was passed
            t0_after_x = (time_first_after_x
                          - (pos_x_first_after_x
                             - x_reference)
                          / velocity_first_after_x
                          )
            times_x_reference[obj_ind] = times - t0_after_x

    return times_x_reference
