
import pathlib
import pytest

import numpy as np
import dclab
import matplotlib.pyplot as plt

from dctrack.track import TrackedDataSet
from dctrack.channel_detection import (
    calculate_channel_from_inflection_point, plot_channel_regions_lines,
)
from dctrack.velocity_curve import (
    get_init_velocity_curve, velocity_fit_channel, velocity_fit_poly,
    fit_velocity, predict_displacement,
)

example_file_name = "M002_data_0002_export_filtered.rtdc"
test_dir = pathlib.Path(__file__).parent
data_dir = test_dir / "data"
example_file = str(data_dir / example_file_name)


def test_calculate_channel_from_inflection_point_plot():

    inlet_outlet_file_name = "M002_data_0002_export_filtered.rtdc"
    ds = dclab.new_dataset(str(data_dir / inlet_outlet_file_name))
    img = ds['image'][0]

    _ = calculate_channel_from_inflection_point(
        img, linewidth=5, plot=True, n=1)
    plt.close("all")


def test_plot_get_init_velocity_curve():
    """Plot the curve resulting from the function output
    Not a unit tests function, just to check output visually
    """
    tr_ds = TrackedDataSet(example_file)
    data = tr_ds.dataset

    flow_rate = tr_ds.flow_rate
    width = tr_ds.channel_width
    height = tr_ds.channel_height
    x, v = get_init_velocity_curve(data, flow_rate, width, height,
                                   last_frame=0)

    plt.figure()
    plt.plot(x, v, '.')
    # plt.savefig(
    #     test_dir / "output_test_plots/test_plot_get_init_velocity_curve.png")
    plt.close()


def test_velocity_fit_channel():
    tr_ds = TrackedDataSet(example_file)
    data = tr_ds.dataset
    tr_ds.set_channel_values_manual(x_min=0, x_max=165)

    flow_rate = tr_ds.flow_rate
    width = tr_ds.channel_width
    height = tr_ds.channel_height
    x, v = get_init_velocity_curve(data, flow_rate, width, height,
                                   last_frame=0)

    x_channel_start = tr_ds.channel_regions[0][-1]
    in_channel = x < x_channel_start
    x_channel = x[in_channel]
    v_channel = v[in_channel]
    v_median_channel = velocity_fit_channel(v_channel)

    plt.figure()
    plt.scatter(x, v, color='tab:blue')
    plt.scatter(x_channel, v_channel, color='tab:orange')
    plt.axhline(v_median_channel, color="tab:gray", linestyle='dashed')
    # plt.savefig(test_dir / "output_test_plots/test_velocity_fit_channel.png")
    plt.close()


def test_velocity_fit():
    tr_ds = TrackedDataSet(example_file)
    data = tr_ds.dataset
    tr_ds.set_channel_values_manual(x_min=0, x_max=165)

    flow_rate = tr_ds.flow_rate
    width = tr_ds.channel_width
    height = tr_ds.channel_height
    x, v = get_init_velocity_curve(data, flow_rate, width, height,
                                   last_frame=0)
    velocity_fits = fit_velocity(x, v, tr_ds.channel_regions[0],
                                 tr_ds.roi_size_x, fit_inlet=True)
    poly_inlet = velocity_fits['inlet']['poly']

    x_plot = np.linspace(x.min(), x.max(), 100)
    plt.figure()
    plt.plot(x, v, '.', color="tab:blue")
    plt.plot(x_plot, poly_inlet(x_plot), color="tab:red")
    plt.axhline(velocity_fits['channel']['v'],
                color="tab:gray", linestyle='dashed')
    plt.ylim(0, 200000)
    # plt.savefig(test_dir / "output_test_plots/test_velocity_fit.png")
    plt.close()


def test_displacement():
    tr_ds = TrackedDataSet(example_file)
    data = tr_ds.dataset
    tr_ds.set_channel_values_manual(x_min=0, x_max=165)

    flow_rate = tr_ds.flow_rate
    width = tr_ds.channel_width
    height = tr_ds.channel_height
    x, v = get_init_velocity_curve(data, flow_rate, width, height,
                                   last_frame=0)
    velocity_fits = fit_velocity(x, v, tr_ds.channel_regions[0],
                                 tr_ds.roi_size_x, fit_inlet=True)
    dt = 1. / tr_ds.fps

    plt.figure()
    plt.scatter(x, v, color="tab:blue", s=12, alpha=0.75)

    for ind in np.arange(0, x.size, 25):
        xx = x[ind]
        vv = v[ind]
        displacement = predict_displacement(xx, velocity_fits, dt,
                                            flow_direction='left')
        x_predict = xx + displacement
        # check in which region of the flow the object is
        # repetition of code from `predict_displacement` to get velocity value
        # velocity value is only needed here in the test to generate a scatter
        # plot
        for key in velocity_fits.keys():
            if velocity_fits[key]['x0'] < xx < velocity_fits[key]['x1']:
                region = key
        if region == 'channel':
            v_predict = velocity_fits[region]['v']
        else:
            poly = velocity_fits[region]['poly']
            v_predict = poly(x_predict)
        plt.plot([xx, x_predict], [vv, v_predict], 'o-')

        assert x_predict < xx, "`x_predict` should be to the left of `xx`"

    # plt.savefig(test_dir / "output_test_plots/test_displacement.png")
    plt.close()


def test_velocity_fit_poly():
    tr_ds = TrackedDataSet(example_file)
    data = tr_ds.dataset

    flow_rate = tr_ds.flow_rate
    width = tr_ds.channel_width
    height = tr_ds.channel_height
    x, v = get_init_velocity_curve(data, flow_rate, width, height,
                                   last_frame=0)
    fit = velocity_fit_poly(x, v)

    assert fit[0] == pytest.approx(4.82962912e+05)
    assert fit[1] == pytest.approx(-1.24579736e+04)


def test_plot_channel_regions_lines_plotting():
    """Plot the channel regions"""
    data_file = "M002_data_0002_export_filtered.rtdc"
    with dclab.new_dataset(str(data_dir / data_file)) as ds:
        tr_ds = TrackedDataSet(ds)
        # plot with the TrackedDataSet class method
        tr_ds.set_channel_values_auto()
        tr_ds.plot_channel_regions(auto_close=True)
        # plot with the base function
        im = tr_ds.dataset["image"][0]
        regions = [[1, 20]]
        plot_channel_regions_lines(im, regions, auto_close=True)
        regions = [[1, 20], [100, 150], [500, 570]]
        plot_channel_regions_lines(im, regions, auto_close=True)
        plt.close("all")


def test_plot_channel_regions_lines_wrong_input():
    """Plot the channel regions"""
    data_file = "M002_data_0002_export_filtered.rtdc"
    with dclab.new_dataset(str(data_dir / data_file)) as ds:
        tr_ds = TrackedDataSet(ds)
        im = tr_ds.dataset["image"][0]

        regions = 1
        with pytest.raises(TypeError):
            plot_channel_regions_lines(im, regions)
        regions = (1, 80)
        with pytest.raises(TypeError):
            plot_channel_regions_lines(im, regions)
        regions = {33: 45}
        with pytest.raises(TypeError):
            plot_channel_regions_lines(im, regions)
        regions = [(1, 20)]
        with pytest.raises(TypeError):
            plot_channel_regions_lines(im, regions)
        regions = [[1, 20, 33]]
        with pytest.raises(ValueError):
            plot_channel_regions_lines(im, regions)
