import pathlib
import numpy as np

from dctrack.track import TrackedDataSet

# hyperbolic channel example file has a lot of cases that can be filtered
data_dir = pathlib.Path(__file__).parent / "data"
data_file = "20210521_Felix_hyperbolic_channels_beads_MC_S03_h30_hyper" \
            "_inlet_M003_data_0001.rtdc"
input_path = str(data_dir / data_file)

# tracked dataset that will be used in all tests
tr_ds = TrackedDataSet(input_path)
tr_ds.inlet = True
tr_ds.channel = True
tr_ds.outlet = False
tr_ds.tolerance_window = 35
# channel start placed at 50 µm of ROI
tr_ds.set_channel_values_manual(x_min=0, x_max=50 / tr_ds.pixel_size)
tr_ds.track(frame_min=0, frame_max=300)


def test_filter_object_events():
    tr_ds.filter_obj_n_events = True
    tr_ds.filter_backward_movement = False
    tr_ds.filter_obj_distance = False
    ff = tr_ds.filter(min_events=3, max_events=10000, return_filter=True)

    # resulting array should have True and False entries
    assert (np.any(ff) and not np.all(ff))


def test_filter_object_distance():
    tr_ds.filter_obj_n_events = False
    tr_ds.filter_backward_movement = False
    tr_ds.filter_obj_distance = True
    ff = tr_ds.filter(min_distance=10, max_distance=300, return_filter=True)

    # resulting array should have True and False entries
    assert (np.any(ff) and not np.all(ff))


def test_filter_move_backwards():
    tr_ds.filter_obj_n_events = False
    tr_ds.filter_backward_movement = True
    tr_ds.filter_obj_distance = False
    ff = tr_ds.filter(max_back_movement=0, return_filter=True)

    # resulting array should have True and False entries
    assert (np.any(ff) and not np.all(ff))


def test_all_filters():
    tr_ds.filter_obj_n_events = True
    tr_ds.filter_backward_movement = True
    tr_ds.filter_obj_distance = True
    ff = tr_ds.filter(max_back_movement=0,
                      min_distance=10, max_distance=300,
                      min_events=3, max_events=10000,
                      return_filter=True)

    # resulting array should have True and False entries
    assert (np.any(ff) and not np.all(ff))


def test_return_filtered_ds():
    tr_ds.filter_obj_n_events = True
    tr_ds.filter_backward_movement = True
    tr_ds.filter_obj_distance = True

    ds_filtered = tr_ds.filter(max_back_movement=0,
                               min_distance=20, max_distance=300,
                               min_events=5, max_events=10000,
                               return_filter=False,
                               return_filtered_ds=True)

    assert (len(ds_filtered) < len(tr_ds.dataset))
    # careful: this needs to be changed if the creation of tr_ds was changed
    assert len(ds_filtered) == 227


def test_return_filter_arr_and_filtered_ds():
    tr_ds.filter_obj_n_events = True
    tr_ds.filter_backward_movement = True
    tr_ds.filter_obj_distance = True

    filt, ds_filtered = tr_ds.filter(max_back_movement=0,
                                     min_distance=20, max_distance=300,
                                     min_events=5, max_events=10000,
                                     return_filter=True,
                                     return_filtered_ds=True)

    assert (np.any(filt) and not np.all(filt))
    assert (len(ds_filtered) < len(tr_ds.dataset))
    # careful: this needs to be changed if the creation of tr_ds was changed
    assert len(ds_filtered) == 227
