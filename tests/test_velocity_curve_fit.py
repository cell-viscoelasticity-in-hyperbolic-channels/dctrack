
import pathlib
import pytest

from dctrack.track import TrackedDataSet
from dctrack.velocity_curve import (
    get_init_velocity_curve, check_flow_direction)

example_file_name = "M002_data_0002_export_filtered.rtdc"
test_dir = pathlib.Path(__file__).parent
data_dir = test_dir / "data"
example_file = str(data_dir / example_file_name)


def test_get_init_velocity_curve():
    tr_ds = TrackedDataSet(example_file)
    data = tr_ds.dataset

    flow_rate = tr_ds.flow_rate
    width = tr_ds.channel_width
    height = tr_ds.channel_height
    x, v = get_init_velocity_curve(data, flow_rate, width, height,
                                   last_frame=0)

    assert len(x) == len(v), \
        "`x_positions` and `velocity` should be the same length"


def test_check_flow_direction():

    flow_left = check_flow_direction("left")
    assert flow_left == -1

    flow_right = check_flow_direction("right")
    assert flow_right == 1

    with pytest.raises(ValueError):
        check_flow_direction("roundabout")
