
import pathlib
import dclab

from dctrack.track import TrackedDataSet


example_file_name = "M002_data_0002_export_filtered.rtdc"
test_dir = pathlib.Path(__file__).parent
data_dir = test_dir / "data"
example_file = str(data_dir / example_file_name)


def test_calling_set_channel_values_gui_twice_and_output(gui):
    """Calling the function twice in the same function or script might
    cause it to crash. Check if this happens.
    Also check the output types. These are in one test function due to
    QT limitations (probably because we aren't using a class)
    should try and have a `pytest.mark.skipif` for the gitlab CI,
    or don't ignore when gitlab CI implements interactive for QT
    """
    tr_ds = TrackedDataSet(str(data_dir / example_file_name))
    ds = dclab.new_dataset(str(data_dir / example_file_name))
    width = ds['image'][0].shape[-1]

    tr_ds.set_channel_values_gui(gui, auto_close=True)

    assert isinstance(tr_ds.channel_x_min, (int, float))
    assert isinstance(tr_ds.channel_x_max, (int, float))
    assert tr_ds.channel_x_min >= 0
    assert tr_ds.channel_x_max <= width

    tr_ds.channel_x_min = 2
    tr_ds.channel_x_max = 300
    tr_ds.set_channel_values_gui(gui, auto_close=True)
    assert isinstance(tr_ds.channel_x_min, (int, float))
    assert isinstance(tr_ds.channel_x_max, (int, float))
    assert tr_ds.channel_x_min >= 0
    assert tr_ds.channel_x_max <= width
