import pathlib
import tempfile as tf
import h5py
import dclab

from dctrack.track import TrackedDataSet
import dctrack.utils as utils

data_dir = pathlib.Path(__file__).parent / "data"
data_file = "M002_data_0002_export_filtered.rtdc"
input_path = str(data_dir / data_file)


def test_create_user_config_section():
    """Show that an empty user section is created"""
    rtdc_ds = dclab.new_dataset(input_path)
    assert "user" not in rtdc_ds.config
    utils.create_user_config_section(rtdc_ds)
    assert "user" in rtdc_ds.config
    assert rtdc_ds.config["user"] == {}


def test_populate_user_config_section():
    """Show that the user section is populated"""
    rtdc_ds = dclab.new_dataset(input_path)
    assert "user" not in rtdc_ds.config
    utils.populate_user_config_section(rtdc_ds)
    assert "user" in rtdc_ds.config
    assert rtdc_ds.config["user"] == utils.TRACK_METADATA_DEFAULTS


def test_user_config_section_functions_together():
    """Show that create and populate functions work together"""
    rtdc_ds = dclab.new_dataset(input_path)
    assert "user" not in rtdc_ds.config
    utils.create_user_config_section(rtdc_ds)
    utils.populate_user_config_section(rtdc_ds)
    assert "user" in rtdc_ds.config
    assert rtdc_ds.config["user"] == utils.TRACK_METADATA_DEFAULTS


def test_tracked_dataset_config():
    """Show that the user section exists after instantiating TrackedDataSet"""
    tr_ds = TrackedDataSet(input_path)
    assert "user" in tr_ds.dataset.config
    assert tr_ds.dataset.config["user"] == utils.TRACK_METADATA_DEFAULTS


def test_user_config_section_with_populated_metadata_defaults():
    """Show that the user config section exists after saving"""
    with tf.TemporaryDirectory() as tpath:
        tpath = pathlib.Path(tpath)
        tr_ds = TrackedDataSet(input_path)
        rtdc_ds = tr_ds.dataset
        assert rtdc_ds.config["user"] == utils.TRACK_METADATA_DEFAULTS

        expath = tpath / "exported.rtdc"
        rtdc_ds.export.hdf5(expath,
                            features=["deform", "area_um", "pos_x",
                                      "frame", "index"])
        # make sure that worked
        with h5py.File(expath, "r") as h5:
            assert h5.attrs["user:inlet"]
        # open again with dclab
        with dclab.new_dataset(expath) as rtdc_ds2:
            assert isinstance(rtdc_ds2.config["user"],
                              (dict,
                               dclab.rtdc_dataset.config.ConfigurationDict))
            for key in rtdc_ds2.config["user"].keys():
                if key == "channel_regions":
                    assert rtdc_ds2.config["user"][key].tolist() == [[0, 10],
                                                                     [10, 20]]
                else:
                    assert (rtdc_ds2.config["user"][key] ==
                            utils.TRACK_METADATA_DEFAULTS[key])

            tr_ds2 = TrackedDataSet(rtdc_ds2)
            assert tr_ds2.dataset.config[
                       "user"] == utils.TRACK_METADATA_DEFAULTS
