import pathlib
import numpy as np
import tempfile as tf
import pandas as pd
import pytest

from dctrack.track import TrackedDataSet
from dctrack.analysis_tools import (
    determine_region_type, get_channel_regions,
    export_data_of_multi_constriction, get_stat_for_feature, convert_x_and_time
)
from dctrack.utils import FeatureDoesNotExistError


data_dir = pathlib.Path(__file__).parent / "data"
data_file = "M002_data_0002_export_filtered.rtdc"
data_file_2 = "channel_multiple_constrictions_20x.rtdc"
data_file_3 = "test_data_multiple_constrictions_small.rtdc"
data_file_scalar = "M002_data_0002_export_filtered_scalar.rtdc"
input_path = str(data_dir / data_file)
input_path_2 = str(data_dir / data_file_2)
input_path_3 = str(data_dir / data_file_3)
input_path_scalar = str(data_dir / data_file_scalar)

ELLIPSE_FEATS = ['ell_cx', 'ell_cy', 'ell_major', 'ell_minor', 'ell_angle']


def test_get_object_basic():
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    tr_ds.set_tolerance_window(40)
    tr_ds.track()
    object_dict = tr_ds.get_object(7)
    time_channel = np.array(
        [-0.000810, -0.000478, -0.000310, -0.000142, 0.000190,
         0.000357, 0.000523, 0.000692])
    np.testing.assert_allclose(object_dict['time_channel'], time_channel,
                               rtol=0.06)


def test_get_object_error():
    """Check that Error is raised when dataset was not tracked yet and user
    wants to access objects."""
    tr_ds = TrackedDataSet(input_path)
    with pytest.raises(FeatureDoesNotExistError):
        _ = tr_ds.get_object(7)


def test_get_object_data_types():
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    object_dict = tr_ds.get_object(12, loaded_only=False, scalar_only=False)
    assert isinstance(object_dict, dict)
    # scalar values
    assert isinstance(object_dict['deform'], np.ndarray)
    assert isinstance(object_dict['deform'][0], np.float32)
    # image or mask values
    assert isinstance(object_dict['image'], np.ndarray)
    assert isinstance(object_dict['image'][0], np.ndarray)
    # contour values are non-uniform in shape, best stored in a list
    assert isinstance(object_dict['contour'], list)
    assert isinstance(object_dict['contour'][0], np.ndarray)


def test_get_object_lengths():
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_tracking_metadata(
        {"channel": True, "inlet": True, "outlet": False})
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    object_dict = tr_ds.get_object(7, loaded_only=False, scalar_only=False)
    # object 7 has only 11 events
    assert len(object_dict['deform']) == 8
    assert len(object_dict['image']) == 8
    assert len(object_dict['contour']) == 8

    deform = np.array([0.0136722, 0.0329733, 0.050246, 0.0989091, 0.105059,
                       0.0634134, 0.0529091, 0.0503894])
    np.testing.assert_almost_equal(object_dict['deform'], deform)
    with pytest.raises(IndexError):
        np.testing.assert_almost_equal(object_dict['deform'][12], deform[12])


def test_get_object_features_choice():
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_tracking_metadata(
        {"channel": True, "inlet": True, "outlet": False})
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    # scalar and available/innate
    object_dict_loaded = tr_ds.get_object(1, loaded_only=True)
    ds = tr_ds.dataset
    features_scalar_loaded = list(set(ds.features_loaded)
                                  & set(ds.features_scalar))
    for feat in features_scalar_loaded:
        assert feat in object_dict_loaded
    # scalar
    object_dict_scalar = tr_ds.get_object(1, loaded_only=False,
                                          scalar_only=True)
    ds = tr_ds.dataset
    for feat in ds.features_scalar:
        assert feat in object_dict_scalar
    # scalar and nonscalar
    object_dict_nonscalar = tr_ds.get_object(1, loaded_only=False,
                                             scalar_only=False)
    ds = tr_ds.dataset
    for feat in ds.features:
        assert feat in object_dict_nonscalar


def test_get_object_shape():
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_tracking_metadata(
        {"channel": True, "inlet": True, "outlet": False})
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    object_dict = tr_ds.get_object(2, loaded_only=False, scalar_only=False)
    assert object_dict['deform'].shape == (7,)
    assert object_dict['image'].shape == (7, 80, 1100)
    assert len(object_dict['contour']) == 7


def test_convert_x_and_time():
    """Basic check that position-time-conversion works."""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_tracking_metadata(
        {"channel": True,
         "inlet": True,
         "outlet": False})
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    old_df = pd.DataFrame()
    old_df["pos_x"] = tr_ds.dataset["pos_x"][:]
    old_df["time"] = tr_ds.dataset["time"][:]
    new_df = convert_x_and_time(tr_ds, old_df, x_ref=300,
                                label="pos_200")
    assert "x_pos_200" in new_df.columns
    assert "time_pos_200" in new_df.columns
    assert np.isclose(new_df.loc[4, "x_pos_200"], 45.356766)
    assert np.isclose(new_df.loc[10, "time_pos_200"], 0.00123909)


def test_determine_region_type_1():
    """Check that the software correctly identifies parts of the channel"""
    tr_ds = TrackedDataSet(input_path_2)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(True)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(13)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_manual(
        channel_regions=[[0, 28], [28, 68], [68, 132], [132, 205],
                         [205, 273], [273, 346], [346, 410], [410, 486],
                         [486, 546], [546, 622], [622, 687], [687, 730],
                         [730, 800]])
    rt = determine_region_type(tracked_ds=tr_ds)
    expected_region_types = ['inlet', 'channel', 'constriction', 'channel',
                             'constriction', 'channel', 'constriction',
                             'channel', 'constriction', 'channel',
                             'constriction', 'channel', 'outlet']
    assert np.array_equal(rt, expected_region_types)


def test_determine_region_type_2():
    """Check that the software correctly identifies parts of the channel"""
    tr_ds = TrackedDataSet(input_path_3)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(False)
    tr_ds.set_outlet(False)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(4)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_auto()
    assert np.allclose(
        tr_ds.channel_regions,
        [[0, 79], [79, 676], [676, 780], [780, 1100]], atol=3)
    rt = determine_region_type(tracked_ds=tr_ds)
    expected_region_types = ['channel', 'constriction', 'channel',
                             'constriction']
    assert np.array_equal(rt, expected_region_types)


def test_get_channel_regions_1():
    """Check the returned channel regions. These are not soo good for analysis,
    so in the other tests we set the channel regions manually."""
    tr_ds = TrackedDataSet(input_path_3)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(False)
    tr_ds.set_outlet(False)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(4)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_auto()
    analysis_regions = get_channel_regions(tr_ds, region_type="channel")
    assert np.allclose(analysis_regions, [[780, 1100], [79, 676]], atol=3)
    analysis_regions = get_channel_regions(tr_ds, region_type="constriction")
    assert np.allclose(analysis_regions, [[676, 780], [0, 79]], atol=3)


def test_export_data_from_multi_constriction_exp():
    """Check that the export of data from tracking in the
    multi-constriction channel are correct."""
    tr_ds = TrackedDataSet(input_path_3)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(False)
    tr_ds.set_outlet(False)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(4)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_manual(channel_regions=[[0, 81],
                                                     [81, 662],
                                                     [662, 798],
                                                     [798, 1100]])
    tr_ds.track()
    with tf.TemporaryDirectory() as tdir:
        tdir = pathlib.Path(tdir)
        export_data_of_multi_constriction(
            tracked_ds=tr_ds,
            output_path=tdir / "multi_con_data.csv",
            filtered=False)
        data = pd.read_csv(tdir / "multi_con_data.csv", sep=";")
        assert "cell number" in data.keys()
        assert "area start" in data.keys()
        assert "transit time" in data.keys()
        np.testing.assert_almost_equal(data["area end"][0],
                                       271.7306, decimal=4)
        np.testing.assert_almost_equal(data["area start"][1],
                                       271.7306, decimal=4)
        np.testing.assert_almost_equal(data["area start"][3],
                                       298.3923, decimal=4)
        np.testing.assert_almost_equal(data["time start"][15],
                                       np.nan, decimal=4)
        export_data_of_multi_constriction(
            tracked_ds=tr_ds,
            output_path=tdir / "multi_con_data_filtered.csv",
            filtered=True)
        data = pd.read_csv(tdir / "multi_con_data_filtered.csv", sep=";")
        np.testing.assert_almost_equal(data["area end"][0],
                                       271.7306, decimal=4)
        np.testing.assert_almost_equal(data["deform start"][5],
                                       0.00848, decimal=4)
        np.testing.assert_almost_equal(data["area start"][6],
                                       240.87439, decimal=4)


def test_calculate_statistics_multi_constriction():
    """Check that the statistics calculated for tracking in the
    multi-constriction channel are correct."""
    tr_ds = TrackedDataSet(input_path_3)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(False)
    tr_ds.set_outlet(False)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(4)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_manual(channel_regions=[[0, 81],
                                                     [81, 662],
                                                     [662, 798],
                                                     [798, 1100]])
    tr_ds.track()
    with tf.TemporaryDirectory() as tdir:
        tdir = pathlib.Path(tdir)
        export_data_of_multi_constriction(
            tracked_ds=tr_ds,
            output_path=tdir / "multi_con_data_filtered.csv",
            filtered=True)
        data = pd.read_csv(tdir / "multi_con_data_filtered.csv", sep=";")
        tt_constrictions = get_stat_for_feature(data,
                                                feature="transit time",
                                                region_type="constriction")
        assert isinstance(tt_constrictions, dict)
        assert isinstance(tt_constrictions["mean"], list)
        np.testing.assert_allclose(tt_constrictions["mean"],
                                   [0.004731, 0.00153],
                                   rtol=0.01)
        tt_channel = get_stat_for_feature(data,
                                          feature="transit time",
                                          region_type="channel")
        np.testing.assert_allclose(tt_channel["mean"],
                                   [0.0013885, 0.003647],
                                   rtol=0.01)
        np.testing.assert_allclose(tt_channel["std"],
                                   [5.0667e-6, 1.04e-5],
                                   rtol=0.01)


def test_calculate_statistics_multi_constriction_2():
    """Check that the statistics calculated for tracking in the
    multi-constriction channel are correct, this time with beads."""
    tr_ds = TrackedDataSet(input_path_2)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(True)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(13)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_manual(
        channel_regions=[[0, 28], [28, 68], [68, 132], [132, 205],
                         [205, 273], [273, 346], [346, 410], [410, 486],
                         [486, 546], [546, 622], [622, 687], [687, 730],
                         [730, 800]])
    tr_ds.track()
    with tf.TemporaryDirectory() as tdir:
        tdir = pathlib.Path(tdir)
        export_data_of_multi_constriction(
            tracked_ds=tr_ds,
            output_path=tdir / "multi_con_data_filtered.csv",
            filtered=True)
        data = pd.read_csv(tdir / "multi_con_data_filtered.csv", sep=";")
        tt_constrictions = get_stat_for_feature(data,
                                                feature="transit time",
                                                region_type="constriction")
        assert isinstance(tt_constrictions, dict)
        assert isinstance(tt_constrictions["mean"], list)
        assert len(tt_constrictions["mean"]) == 5
        np.testing.assert_allclose(tt_constrictions["mean"],
                                   [0.00227233, 0.00228337, 0.00277245,
                                    0.00217126, np.nan],
                                   rtol=1e-5, equal_nan=True)


def test_calculate_statistics_multi_constriction_with_kwargs():
    """Check that keyword arguments are correctly stored together with the
    statistics calculated for tracking in the multi-constriction channel."""
    tr_ds = TrackedDataSet(input_path_2)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(True)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(13)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_manual(
        channel_regions=[[0, 28], [28, 68], [68, 132], [132, 205],
                         [205, 273], [273, 346], [346, 410], [410, 486],
                         [486, 546], [546, 622], [622, 687], [687, 730],
                         [730, 800]])
    tr_ds.track()
    with tf.TemporaryDirectory() as tdir:
        tdir = pathlib.Path(tdir)
        export_data_of_multi_constriction(
            tracked_ds=tr_ds,
            output_path=tdir / "multi_con_data_filtered.csv",
            filtered=True,
            pressure=50,
            sample_nominatus="very_imprt_name",
            nice_list=[1, 42, 666])
        data = pd.read_csv(tdir / "multi_con_data_filtered.csv",
                           header=0, sep=";")
        assert "pressure" in data.keys()
        assert "sample_nominatus" in data.keys()
        assert "nice_list" not in data.keys()
        pressure_val = data.loc[1]["pressure"]
        assert pressure_val == 50


def test_calculate_statistics_multi_constriction_with_filters():
    """Check that `filters` are correctly considered when the statistics are
    calculated for tracking in the multi-constriction channel."""
    tr_ds = TrackedDataSet(input_path_3)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(False)
    tr_ds.set_outlet(False)
    tr_ds.set_tolerance_window(50)
    tr_ds.set_nb_channel_regions(4)
    tr_ds.set_multiple_constrictions(True)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_manual(channel_regions=[[0, 81],
                                                     [81, 662],
                                                     [662, 798],
                                                     [798, 1100]])
    tr_ds.track()
    with tf.TemporaryDirectory() as tdir:
        tdir = pathlib.Path(tdir)
        export_data_of_multi_constriction(
            tracked_ds=tr_ds,
            output_path=tdir / "multi_con_data.csv",
            filtered=False,
            special_filters={"area_um": [0, 270]},
            pressure=50)
        data_unfiltered = pd.read_csv(tdir / "multi_con_data.csv",
                                      header=0, sep=";")
        np.testing.assert_almost_equal(data_unfiltered["cell number"].unique(),
                                       [1., 2., 3., 4.])
        export_data_of_multi_constriction(
            tracked_ds=tr_ds,
            output_path=tdir / "multi_con_data_filtered.csv",
            filtered=True,
            special_filters={"area_um": [0, 270]},
            pressure=50)
        data = pd.read_csv(tdir / "multi_con_data_filtered.csv",
                           header=0, sep=";")
        np.testing.assert_almost_equal(data["cell number"].unique(), [3.])
        assert "pressure" in data.keys()
        pressure_val = data.loc[1]["pressure"]
        assert pressure_val == 50


def test_get_ellipse_data():
    """Check if ellipse features are included in dataset after calling
    function.
    """
    tr_ds = TrackedDataSet(input_path)
    tr_ds.get_ellipse_data()
    for feat in ELLIPSE_FEATS:
        assert feat in tr_ds.dataset.features


# commented because the test datasets have an outdated feature format
# (index_online) does not exist
# def test_get_ellipse_data():
#     """Check if ellipse features are included in dataset after calling
#     function.
#     """
#     tr_ds = TrackedDataSet(input_path_scalar)
#     ds_original = dclab.new_dataset(input_path)
#     tr_ds.get_ellipse_data_scalar(ds_original)
#     for feat in ELLIPSE_FEATS:
#         assert feat in tr_ds.dataset.features
