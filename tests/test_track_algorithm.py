
import pytest
import pathlib
import numpy as np
import dclab

from dctrack.track import TrackedDataSet, DatasetNotTrackedError


data_dir = pathlib.Path(__file__).parent / "data"
# data_file = "M002_data_0002_export_filtered.rtdc"
data_file = "test_data_cells_inlet.rtdc"
input_path = str(data_dir / data_file)


def test_set_channel_values_manual():
    """First simple test to see if the values are written correctly into the
    object. Default flow direction is "left", while the pixel positions
    start from the left!"""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_inlet(False)
    x_min, x_max = 0, 550
    channel_zone = [[x_min, x_max]]
    tr_ds.set_channel_values_manual(x_min=x_min, x_max=x_max,
                                    channel_regions=channel_zone)
    assert tr_ds.channel_x_min < tr_ds.channel_x_max


def test_set_channel_values_auto_output():
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    assert tr_ds.channel_x_min == 0
    assert tr_ds.channel_x_max == 560


def test_assign_velocity_time():
    """Test if the new features `velocity`, `time_channel` and `time_inlet`
    where assigned to the tracked dataset and contain values that are
    non-nan."""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    tr_ds.set_tolerance_window(40)
    tr_ds.track()
    for feat in ['velocity', 'time_channel', 'time_inlet']:
        assert feat in tr_ds.dataset.features
        # assert that at least one non-nan values are in array
        feature_array = tr_ds.dataset[feat]
        assert feature_array[~np.isnan(feature_array)].size > 0


def test_run_track_twice():
    """Running `TrackedDataSet.track()` a second time can cause errors.
    Check that no error occurs when running track again."""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    assert tr_ds.already_tracked is True
    tr_ds.track()


def test_filter_dclab_default_feature():
    """Basic test for filtering a default dclab feature"""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    assert tr_ds.already_tracked is True
    assert dclab.dfn.feature_exists("area_um")

    filt_data = tr_ds.filter_feature(
        feature="area_um", filter_min=140, filter_max=142)
    assert len(filt_data) == 31


def test_filter_added_temporary_feature():
    """Basic test for filtering a temporary feature added during tracking"""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    assert tr_ds.already_tracked is True
    assert dclab.dfn.feature_exists("object_number")

    filt_data = tr_ds.filter_feature(
        feature="object_number", filter_min=10, filter_max=20)
    assert len(filt_data) == 128


def test_filter_untracked_data():
    """Basic test to show that filtering before tracking fails"""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_outlet(False)
    tr_ds.set_channel_values_auto()
    assert dclab.dfn.feature_exists("object_number")
    assert tr_ds.already_tracked is False
    with pytest.raises(DatasetNotTrackedError):
        _ = tr_ds.filter_feature(
            feature="object_number", filter_min=10, filter_max=20)

    tr_ds.track()
    assert tr_ds.already_tracked is True
    filt_data = tr_ds.filter_feature(
        feature="object_number", filter_min=10, filter_max=20)
    assert len(filt_data) == 128


def test_get_time_after_x():
    """Test to check if the new feature variable gets added to the dataset,
    some actual times are computed and if these can be called with `get_object`
    """
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel_values_auto()
    tr_ds.track()

    feat_name = 'time_100'
    time_after_100 = tr_ds.time_after_x(100, return_value=True,
                                        add_to_dataset=True,
                                        variable_name=feat_name)
    assert feat_name in tr_ds.dataset.features
    assert time_after_100[~np.isnan(time_after_100)].size > 0
    assert tr_ds.dataset[feat_name][~np.isnan(tr_ds.dataset[feat_name])].size \
           > 0
    cell_7 = tr_ds.get_object(7)
    assert cell_7[feat_name][~np.isnan(cell_7[feat_name])].size > 0
