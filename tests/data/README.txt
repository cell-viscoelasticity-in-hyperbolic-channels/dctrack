source for the experiment files here:
guck_division2\Data\RTDC\20200729_Felix_S03_fullchannel_flowrate_study\inlet\M002_data.rtdc

M002_data_0002.rtdc was made in DCKit with split file function, events 5001-10000

M002_data_filtered_export* were created with dclab with filters:
area_um_min = 140
area_um_min = 150
aratio_max = 1.05

RTDC_20200729_Felix_S03_fullchannel_flowrate_study_inlet_M002_0.04uls* - files created with old 
tracking scripts and in _untracked tag object index was manually removed.

test_data_cells_inlet.rtdc : 
guck_division2\Data\RTDC\20200729_Felix_HL60_fullchannel_flowrate_study\inlet\M002_data.rtdc

export with filters:
area_um_min = 50
area_um_min = 250
aspect_ratio_max = 5
aratio_max = 1.08
event_index_min = 20000
event_index_max = 20500

MMM_test_data:
HSMFS:\Data\RTDC\20230116_BH_HL60_MMM_optimization\0_0\M001_data.rtdc
filtered for events 1 - 200
then filtered with a Python script to get the first 10 and last 17 events, plus every second (even) event in between:
filter = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, .... 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

test_data_hyperDC_fluo:
HSMFS:\Data\RTDC\20230822_BHCS_HeLa_Enucleation_hyperDC\control\M001_data.rtdc
filtered for events 254 - 323 (index dataset)
