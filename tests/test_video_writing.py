import tempfile as tf
import pathlib
import dclab

from dctrack.track import TrackedDataSet
from dctrack.video import video_write_from_scalar_export


parent = pathlib.Path(__file__).parent
data_dir = parent / "data"
data_file = "test_data_cells_inlet.rtdc"
input_path = str(data_dir / data_file)

data_file_scalar = "M002_data_0002_export_filtered_scalar.rtdc"
input_path_scalar = str(data_dir / data_file_scalar)


def test_video_to_avi_file_not_empty():
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel_values_auto()
    tr_ds.tolerance_window = 50
    tr_ds.track()
    with tf.TemporaryDirectory() as t_dir:
        video_path = pathlib.Path(t_dir) / "tracked_objects.avi"
        tr_ds.write_video(save_path=str(video_path), draw_zones=True)
        assert (video_path.is_file()
                and video_path.stat().st_size > 0)


def test_video_write_from_scalar_export_not_empty():
    tr_ds = TrackedDataSet(input_path_scalar)
    tr_ds.inlet = True
    tr_ds.channel = True
    tr_ds.outlet = False
    tr_ds.set_channel_values_manual(x_min=0, x_max=565)
    tr_ds.tolerance_window = 50
    tr_ds.track()

    dataset_scalar = tr_ds.dataset
    dataset_original = dclab.new_dataset(input_path)

    with tf.TemporaryDirectory() as t_dir:
        video_path_scalar = pathlib.Path(t_dir) / "tracked_objects_scalar.avi"
        video_write_from_scalar_export(dataset_scalar, dataset_original,
                                       save_path=str(video_path_scalar),
                                       draw_zones=True)
        assert (video_path_scalar.is_file()
                and video_path_scalar.stat().st_size > 0)
