import tempfile as tf
import dclab
import pytest
import pathlib
import numpy as np

from dctrack.track_mmm import TrackedMMM, WrongPixelSizeType, path_length

data_dir = pathlib.Path(__file__).parent / "data"
example_file = data_dir / "MMM_test_data.rtdc"


def test_path_length():
    test_p = np.array([(1, 2), (1, 4), (2, 4), (3, 3)])
    p_len = path_length(test_p)
    assert np.isclose(p_len, 4.414213, rtol=0.02)


def test_mmm_dataset_initialization():
    mmm = TrackedMMM(example_file)
    assert mmm.velocities == {}
    assert mmm.flow_direction == "right"
    assert mmm.output_dir == data_dir
    assert mmm.pixel_size == 0.68


def test_mmm_dataset_init_with_rtdc_object():
    """Input is not a path to a file, but an instance of RTDCBase already"""
    ds = dclab.new_dataset(example_file)
    mmm = TrackedMMM(ds)
    assert mmm.output_dir == data_dir
    assert len(mmm.dataset.config["user"]["channel_path"]) == 0


def test_mmm_load_data_wrong_path():
    with pytest.raises(FileNotFoundError):
        _ = TrackedMMM(data_dir / "test_file.rtdc")


def test_load_data_wrong_type():
    with pytest.raises(NotImplementedError):
        _ = TrackedMMM({"some": "value"})
    with pytest.raises(NotImplementedError):
        _ = TrackedMMM(data_dir / "README.txt")


def test_set_output_dir_wrong_type():
    mmm = TrackedMMM(example_file)
    with pytest.raises(ValueError):
        mmm.set_output_dir([1, 2, 3])


def test_set_metadata():
    mmm = TrackedMMM(example_file)
    assert mmm.flow_direction == "right"
    assert mmm.window_size == 8
    metadata = {"roi": [43, 4, 100, 100],
                "flow_direction": "left",
                "window_size": 6}
    mmm.set_tracking_metadata(metadata)
    assert mmm.flow_direction == "left"
    assert mmm.window_size == 6


def test_set_metadata_wrong_type():
    mmm = TrackedMMM(example_file)
    with pytest.raises(TypeError):
        _ = mmm.set_tracking_metadata(["channel_path"])


def test_set_metadata_unknown_key():
    mmm = TrackedMMM(example_file)
    with pytest.warns(UserWarning):
        mmm.set_tracking_metadata({"order_param": [1, 2, 3]})


def test_set_output_dir():
    mmm = TrackedMMM(example_file)
    assert mmm.output_dir == data_dir
    with tf.TemporaryDirectory() as tdir:
        new_output_dir = pathlib.Path(tdir) / "out"
        assert not new_output_dir.exists()
        mmm.set_output_dir(new_output_dir)
        assert new_output_dir.exists()


def test_channel_path():
    mmm = TrackedMMM(example_file)
    mmm.set_roi([3, 13, 866, 482])
    mmm.set_flow_direction("right")
    mmm.get_channel_path()
    assert isinstance(mmm.channel_path, np.ndarray)
    assert len(mmm.channel_path) == 550
    assert np.allclose(mmm.channel_path[0], [70.4399, 31.59], rtol=0.03)


def test_set_pixel_size():
    mmm_ds = TrackedMMM(example_file)
    mmm_ds.set_flow_direction("right")
    assert mmm_ds.pixel_size == 0.68
    mmm_ds.set_pixel_size(0.34)
    assert isinstance(mmm_ds.pixel_size, float)
    assert mmm_ds.pixel_size == 0.34


def test_wrong_pixel_size_type_raises_error():
    mmm = TrackedMMM(example_file)
    with pytest.raises(WrongPixelSizeType):
        mmm.set_pixel_size("bla")


def test_set_pred_zone_size():
    mmm = TrackedMMM(example_file)
    assert mmm.pred_zone_size == 6
    mmm.set_pred_zone_size(5)
    assert mmm.pred_zone_size == 5
    with pytest.raises(TypeError):
        mmm.set_pred_zone_size([5])


def test_set_flow_direction():
    mmm = TrackedMMM(example_file)
    assert mmm.flow_direction == "right"
    mmm.set_flow_direction("left")
    assert mmm.flow_direction == "left"
    with pytest.raises(TypeError):
        mmm.set_flow_direction(5)
    mmm.set_flow_direction("upwards")
    assert mmm.flow_direction == "right"


def test_median_displacement():
    # test the function get_median_displacement
    mmm_ds = TrackedMMM(example_file)
    mmm_ds.set_flow_direction("right")
    mmm_ds.set_roi([3, 13, 866, 482])
    mmm_ds.get_channel_path()
    med_displace, delta_displace = mmm_ds.get_median_displacement()
    assert isinstance(med_displace, float)
    assert isinstance(delta_displace, float) or isinstance(delta_displace, int)
    assert np.isclose(med_displace, 3, rtol=0.02)
    assert np.isclose(delta_displace, 8.0, rtol=0.02)


def test_mmm_tracking():
    with tf.TemporaryDirectory() as tdir:
        tdir = pathlib.Path(tdir)
        mmm = TrackedMMM(example_file)
        mmm.set_flow_direction("right")
        mmm.set_output_dir(tdir)
        mmm.set_roi_automatically()
        mmm.track()
        mmm.export_tracking_results()
        assert (tdir / "channel_img_with_path.png").exists()
        assert (tdir / "tracked_MMM_data_filtered.csv").exists()
