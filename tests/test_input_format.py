
"""Test how different input formats are handled"""
import pathlib
import pytest
from dclab.rtdc_dataset.core import RTDCBase

from dctrack.track import TrackedDataSet

# list of working example input formats
data_dir = pathlib.Path(__file__).parent / "data"

data_files = [
    "M002_data_0002_export_filtered.rtdc",
    "M002_data_0002_export_filtered.hdf5",
    ]
example_files = [str(data_dir / i) for i in data_files]


def test_raise_error_when_input_file_does_not_exist():
    """Check if FileNotFoundError is raised when input path is not a file"""

    path = r"C:/some/bullshit/path.txt"
    with pytest.raises(FileNotFoundError):
        _ = TrackedDataSet(path)


@pytest.mark.parametrize("input_data", [4, True, [1, 2, 3, 5, 6]])
def test_raise_error_when_wrong_input_format(input_data):
    """
    Check if NotImplementedError is raised when input data has wrong format
    is not a file
    """
    with pytest.raises(NotImplementedError):
        _ = TrackedDataSet(input_data)


@pytest.mark.parametrize("input_path", example_files)
def test_if_get_input_return_rtdcbase_object(input_path):
    """The returned input should be a RTDCBase object from dclab."""

    tr_ds = TrackedDataSet(input_path)
    data = tr_ds.dataset

    assert isinstance(data, RTDCBase)


@pytest.mark.parametrize("input_path", example_files)
def test_input_contains_dclab_features(input_path):
    """
    Test if the input data contains dclab features that are
    needed for the tracking.
    """

    tr_ds = TrackedDataSet(input_path)
    data = tr_ds.dataset

    # some dclab features needed for tracking
    dclab_test_features = ['pos_x', 'frame', 'index', 'deform', 'area_um']
    # all scalar features of the input data
    sc_features = data.features_scalar

    assert set(dclab_test_features).issubset(sc_features)
