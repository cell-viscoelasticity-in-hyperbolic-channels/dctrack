
import pathlib
import pytest
import imageio.v2 as imageio
import numpy as np
import dclab

from dctrack.channel_detection import (
    calculate_channel_from_inflection_point, get_end_of_inlet_channel,
    find_channel_regions, find_channel_walls)
from dctrack.track import TrackedDataSet

data_dir = pathlib.Path(__file__).parent / "data"


def test_calculate_channel_from_inflection_point_n():
    """Check that the `calculate_channel_from_inflection_point` function
    returns the correct number of peaks for input `n`.
    """
    example_file_name = "channel_standard_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_file_name))

    for n in range(12):
        n_positions = calculate_channel_from_inflection_point(
            img, linewidth=5, plot=False, n=n)
        assert len(n_positions) == n


def test_calculate_channel_from_inflection_point_fail():
    """Check that the `calculate_channel_from_inflection_point` function
    returns Errors for certain inputs.
    """
    example_file_name = "channel_standard_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_file_name))

    # show that asking for a too-large `min_separation` between peaks results
    # in a ValueError.
    assert img.shape[1] == 1250
    min_separation = 1300
    n = 2
    with pytest.raises(ValueError):
        _ = calculate_channel_from_inflection_point(
            img, linewidth=5, plot=False, n=n,
            min_separation=min_separation)

    # show that asking for a too-large `min_separation` or too high `n`
    # results in a UserWarning
    min_separation = 1200
    n = 2
    with pytest.raises(UserWarning):
        _ = calculate_channel_from_inflection_point(
            img, linewidth=5, plot=False, n=n,
            min_separation=min_separation)
    min_separation = 300
    n = 50
    with pytest.raises(UserWarning):
        _ = calculate_channel_from_inflection_point(
            img, linewidth=5, plot=False, n=n,
            min_separation=min_separation)


def test_geometry_standard_inlet_channel():
    """Verify that the `calculate_channel_from_inflection_point` function
    gives reasonable channel values for a basic channel consisting of an inlet
    and channel.
    """
    # channel_standard_ch_in.png
    inlet_outlet_file_name = "M002_data_0002_export_filtered.rtdc"
    ds = dclab.new_dataset(str(data_dir / inlet_outlet_file_name))
    img = ds['image'][0]

    n_positions = calculate_channel_from_inflection_point(
        img, linewidth=5, plot=False, n=1)

    desired = 565
    assert np.allclose(n_positions, desired, rtol=0.02)


def test_geometry_standard_channel_outlet():
    """Verify that the `calculate_channel_from_inflection_point` function
    gives reasonable channel values for a basic channel consisting of a channel
    and outlet.
    """
    # channel_standard_ch_in.png
    example_file_name = "M002_data_0002_export_filtered.rtdc"
    ds = dclab.new_dataset(str(data_dir / example_file_name))
    # flip in x direction to simulate oppsite channel geometry
    img = np.fliplr(ds['image'][0])

    n_positions = calculate_channel_from_inflection_point(
        img, linewidth=5, plot=False, n=1)

    desired = 535
    assert np.allclose(n_positions, desired, rtol=0.02)


def test_geometry_standard_inlet_channel_outlet():
    """Verify that the `calculate_channel_from_inflection_point` function
    gives reasonable channel values for a basic channel consisting of an inlet,
    channel and outlet.
    """
    example_file_name = "channel_standard_in_ch_out.png"
    img = imageio.imread(str(data_dir / example_file_name))

    n_positions = calculate_channel_from_inflection_point(
        img, linewidth=3, plot=False, n=2,
        sigma=4, min_separation=300)

    desired = [105, 1030]
    assert np.allclose(n_positions, desired, rtol=0.05)


def test_geometry_phase_bf_outlet_channel():
    """Verify that the `calculate_channel_from_inflection_point` function
    gives reasonable channel values for a basic channel consisting of a
    channel and outlet for the bright field of a phase image.
    """
    example_file_name = "channel_phase_bf_out_ch.png"
    img = imageio.imread(str(data_dir / example_file_name))
    img = np.rot90(img)

    n = 1
    n_positions = calculate_channel_from_inflection_point(
        img, linewidth=900, plot=False, n=n, sigma=10)

    desired = 305
    assert np.allclose(n_positions, desired, rtol=0.01)


def test_geometry_standard_multiple_constrictions():
    """Verify that the `calculate_channel_from_inflection_point` function
    gives reasonable channel values for a basic channel consisting of several
    standard inlet-channel-outlet in series.
    """
    example_file_name = "channel_standard_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_file_name))

    n = 10
    n_positions = calculate_channel_from_inflection_point(
        img, linewidth=20, plot=False, n=n, sigma=2)

    desired = [114, 185, 343, 415, 573, 644, 804, 875, 1033, 1105]
    assert np.allclose(n_positions, desired, rtol=0.05)

    # add check for channel zone consistency
    c_zone = n_positions[1] - n_positions[0]
    assert np.allclose(n_positions[3] - n_positions[2], c_zone, atol=5)
    assert np.allclose(n_positions[5] - n_positions[4], c_zone, atol=5)
    assert np.allclose(n_positions[7] - n_positions[6], c_zone, atol=5)
    assert np.allclose(n_positions[9] - n_positions[8], c_zone, atol=5)


def test_geometry_tight_multiple_constrictions():
    """Verify that the `calculate_channel_from_inflection_point` function
    gives reasonable channel values for a basic channel consisting of several
    nonstandard (tightly packed) inlet-channel-outlet in series.
    """
    example_file_name = "channel_tight_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_file_name))

    n = 6
    n_positions = calculate_channel_from_inflection_point(
        img, linewidth=30, plot=False, n=n, sigma=5,
        min_separation=10)

    desired = [29, 86, 119, 175, 207, 263]
    assert np.allclose(n_positions, desired, rtol=0.05)

    # add check for channel zone consistency
    c_zone = n_positions[1] - n_positions[0]
    assert np.allclose(n_positions[3] - n_positions[2], c_zone, atol=5)
    assert np.allclose(n_positions[5] - n_positions[4], c_zone, atol=5)


def test_set_channel_values_auto():
    """Check that the TrackedDataSet method `set_channel_values_auto` returns
    a list of tuples.
    """
    example_file_name = "M002_data_0002_export_filtered.rtdc"
    tr_ds = TrackedDataSet(str(data_dir / example_file_name))
    tr_ds.set_channel(True)
    tr_ds.set_inlet(True)
    tr_ds.set_channel_values_auto()
    assert isinstance(tr_ds.channel_regions, list)
    assert len(tr_ds.channel_regions) == 2, "it is a list of lists"
    assert isinstance(tr_ds.channel_regions[0], list)
    assert len(tr_ds.channel_regions[0]) == 2, "first list"
    assert isinstance(tr_ds.channel_regions, list)
    assert np.allclose(tr_ds.channel_regions, [[0, 561], [561, 1100]], atol=2)


def test_get_end_of_inlet_channel():
    """Check that `get_end_of_inlet_channel` will correctly handle a geometry
    with only one in(out)let.
    """
    inlet_outlet_file_name = "M002_data_0002_export_filtered.rtdc"
    ds = dclab.new_dataset(str(data_dir / inlet_outlet_file_name))
    img = ds['image'][0]
    xpos = 565

    end_of_channel = get_end_of_inlet_channel(img, [xpos])
    assert end_of_channel == 0

    img_flip = np.fliplr(img)
    xpos = img.shape[1] - 565
    end_of_channel = get_end_of_inlet_channel(img_flip, [xpos])
    assert end_of_channel == img_flip.shape[1]


def test_find_channel_regions_inlet_channel():
    """Check that `find_channel_regions` handles `geometry="inlet"`
    correctly.
    """
    inlet_outlet_file_name = "M002_data_0002_export_filtered.rtdc"
    ds = dclab.new_dataset(str(data_dir / inlet_outlet_file_name))
    img = ds['image'][0]

    channel_regions = find_channel_regions(img, inlet=True, channel=True)

    assert isinstance(channel_regions, list)
    assert isinstance(channel_regions[0], list)
    assert len(channel_regions) == 2
    assert len(channel_regions[0]) == 2


def test_find_channel_regions_channel_outlet():
    """Check that `find_channel_regions` handles `geometry="inlet"`
    correctly for a flipped image.
    """
    inlet_outlet_file_name = "M002_data_0002_export_filtered.rtdc"
    ds = dclab.new_dataset(str(data_dir / inlet_outlet_file_name))
    img = np.fliplr(ds['image'][0])

    channel_regions = find_channel_regions(img, inlet=False, channel=True,
                                           outlet=True)

    assert isinstance(channel_regions, list)
    assert len(channel_regions) == 2
    assert isinstance(channel_regions[0], list)
    assert len(channel_regions[0]) == 2


def test_find_channel_regions_inlet_channel_outlet():
    """Check that `find_channel_regions` handles `geometry="outlet"`
    correctly.
    """
    example_file_name = "channel_standard_in_ch_out.png"
    img = imageio.imread(str(data_dir / example_file_name))

    channel_regions = find_channel_regions(img, inlet=True, channel=True,
                                           outlet=True, n=3)

    assert isinstance(channel_regions, list)
    assert len(channel_regions) == 3
    assert isinstance(channel_regions[0], list)
    assert len(channel_regions[0]) == 2


def test_find_channel_regions_multiple_constrictions():
    """Check that `find_channel_regions` handles `geometry="multiple_linear"`
    correctly.
    """
    example_file_name = "channel_standard_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_file_name))

    channel_regions = find_channel_regions(img, multiple_constrictions=True,
                                           n=13)

    assert isinstance(channel_regions, list)
    assert len(channel_regions) == 13
    assert isinstance(channel_regions[0], list)
    assert len(channel_regions[0]) == 2
    assert isinstance(channel_regions[4], list)
    assert len(channel_regions[4]) == 2


def test_userwarning_channel_regions_multiple_constrictions():
    """Check that `find_channel_regions` raises a user warning if multiple
    constrictions = True and nb_channel_regions too small.
    """
    example_file_name = "channel_standard_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_file_name))

    with pytest.raises(UserWarning):
        _ = find_channel_regions(img, multiple_constrictions=True, n=1)


def test_find_channel_regions_tight_multiple_constrictions():
    """Check that `find_channel_regions` handles `geometry="multiple_linear"`
    correctly.
    """
    example_file_name = "channel_tight_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_file_name))

    channel_regions = find_channel_regions(img, multiple_constrictions=True,
                                           n=8)

    assert isinstance(channel_regions, list)
    assert len(channel_regions) == 8
    assert isinstance(channel_regions[0], list)
    assert len(channel_regions[0]) == 2
    assert isinstance(channel_regions[2], list)
    assert len(channel_regions[2]) == 2


def test_find_channel_regions_geometry_error():
    """Check that `find_channel_regions` handles `geometry=None` and an
    incorrect `geometry` name correctly.
    """
    example_file_name = "channel_standard_in_ch_out.png"
    img = imageio.imread(str(data_dir / example_file_name))

    with pytest.raises(ValueError):
        _ = find_channel_regions(img, channel=False)


def test_find_channel_walls():
    """Find the channel walls"""
    example_img = "channel_standard_multiple_constrictions.png"
    # example_img = "channel_tight_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_img))

    walls = find_channel_walls(img, linewidth=3)
    print(walls)
    assert isinstance(walls, list)
    assert walls == [6, 19]


def test_find_constrictions():
    """Find all positions in the image where a new part of the channel
    begins (or ends)"""
    example_img = "channel_standard_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_img))

    channel_pos = calculate_channel_from_inflection_point(
        img, n=12, min_separation=20, linewidth=4, sigma=1,
        plot=False, reduce_func=np.nanmean)

    assert len(channel_pos) == 12
    assert np.allclose(channel_pos, [26, 109, 190, 339, 419, 569, 649,
                                     799, 879, 1028, 1108, 1197])


def test_find_constrictions_2():
    """Find the channel walls and regions in another geometry"""
    example_img = "channel_standard_constrictions_2.png"
    img = imageio.imread(str(data_dir / example_img))

    # first find the channel walls
    walls = find_channel_walls(img, linewidth=3)
    assert isinstance(walls, list)
    assert np.allclose(walls, [13, 72], atol=1)

    channel_pos = calculate_channel_from_inflection_point(
        img, n=3, min_separation=20, linewidth=4, sigma=3,
        plot=False, reduce_func=np.nanmean)

    assert len(channel_pos) == 3
    assert np.allclose(channel_pos, [75, 683, 787], atol=3)

    regions = find_channel_regions(img, inlet=False, channel=True,
                                   outlet=False,
                                   multiple_constrictions=True, n=4)
    assert np.allclose(regions, [[0, 75], [75, 685], [685, 786], [786, 1100]],
                       atol=3)


def test_find_constrictions_20x_img():
    """Find the channel regions in a measurement done with the 20x objective.
    """
    example_img = "channel_standard_multiple_constrictions.png"
    img = imageio.imread(str(data_dir / example_img))

    # first find the channel walls
    walls = find_channel_walls(img, linewidth=3)
    assert isinstance(walls, list)
    assert np.allclose(walls, [6, 19], rtol=0.05)

    channel_pos = calculate_channel_from_inflection_point(
        img, n=12, min_separation=20, linewidth=4, sigma=3,
        plot=False, reduce_func=np.nanmean)

    assert len(channel_pos) == 12
    assert np.allclose(channel_pos, [28, 115, 190, 339, 419, 569, 651,
                                     799, 880, 1029, 1110, 1200])

    regions = find_channel_regions(img, inlet=False, channel=True,
                                   outlet=False,
                                   multiple_constrictions=True, n=13)

    assert np.allclose(regions, [[0,  28], [28, 115], [115,  190],
                                 [190, 339], [339, 419], [419, 569],
                                 [569, 651], [651, 799], [799, 880],
                                 [880, 1029], [1029, 1110],
                                 [1110, 1200], [1200, 1250]], atol=2)
