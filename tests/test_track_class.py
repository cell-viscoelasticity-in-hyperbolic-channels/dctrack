import numpy as np
import pytest
import pathlib
import tempfile as tf
import dclab

from dctrack.track import (TrackedDataSet, FluorescenceNotUsableError,
                           DatasetNotTrackedError)
from dctrack import utils

data_dir = pathlib.Path(__file__).parent / "data"
data_file = "M002_data_0002_export_filtered.rtdc"
input_path = str(data_dir / data_file)
data_file2 = "test_data_hyperDC_fluo.rtdc"
input_path2 = str(data_dir / data_file2)


def test_set_attributes_as_dict():
    """Show how to correctly set the metadata attributes"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        rtdc_ds = tr_ds.dataset
        assert rtdc_ds.config["user"] == utils.TRACK_METADATA_DEFAULTS

        assert tr_ds.inlet is True
        assert tr_ds.channel is True
        assert tr_ds.outlet is False
        new_metadata = {"inlet": False,
                        "channel": False,
                        "outlet": False}
        tr_ds.set_tracking_metadata(new_metadata)
        # show that the above line updates each specified attributes
        assert tr_ds.inlet is False
        assert tr_ds.channel is False
        assert tr_ds.outlet is False


def test_set_attributes_as_dict_bad_type():
    """Show how to correctly set the metadata attributes"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        new_metadata = ["inlet", False, "channel", False]
        # show that non-dicts are not allowed
        with pytest.raises(TypeError):
            tr_ds.set_tracking_metadata(new_metadata)
        with pytest.raises(TypeError):
            tr_ds.set_tracking_metadata(tuple(new_metadata))
        with pytest.raises(TypeError):
            tr_ds.set_tracking_metadata(set(new_metadata))


def test_set_attributes_correct():
    """Show how to correctly set the metadata attributes"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        rtdc_ds = tr_ds.dataset
        assert rtdc_ds.config["user"] == utils.TRACK_METADATA_DEFAULTS

        assert tr_ds.channel is True
        assert tr_ds.dataset.config["user"]["channel"] is True
        tr_ds.set_channel(False)
        # show that the above line correctly updates the config file and
        # attribute
        assert tr_ds.channel is False
        assert tr_ds.dataset.config["user"]["channel"] is False


def test_set_attributes_wrong():
    """Incorrect way to set the metadata attributes"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        rtdc_ds = tr_ds.dataset
        assert rtdc_ds.config["user"] == utils.TRACK_METADATA_DEFAULTS

        assert tr_ds.channel is True
        assert tr_ds.dataset.config["user"]["channel"] is True
        tr_ds.channel = False
        # show that the above line updates the attribute
        assert tr_ds.channel is False
        # show that the above line does not update the config file
        assert tr_ds.dataset.config["user"]["channel"] is True


def test_set_user_tracking_metadata_as_attributes():
    """Show that the tracking metadata is set"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        assert tr_ds.print_tracking_metadata() == tr_ds.dataset.config["user"]


def test_set_metadata_by_key_value():
    """Show that the tracking metadata is set via key and value"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        assert tr_ds.channel is True
        assert tr_ds.print_tracking_metadata() == tr_ds.dataset.config["user"]
        tr_ds._set_metadata_by_key_value("channel", False)
        assert tr_ds.channel is False
        # the defaults are no longer set
        assert tr_ds.dataset.config["user"] != utils.TRACK_METADATA_DEFAULTS
        assert tr_ds.print_tracking_metadata() != utils.TRACK_METADATA_DEFAULTS


def test_print_tracking_metadata():
    """Show that the print tracking metadata prints the correct information"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        # default values
        assert tr_ds.print_tracking_metadata() == tr_ds.dataset.config["user"]
        # change some values
        new_metadata = {
            "inlet": False,
            "channel": False,
            "outlet": False,
            "multiple_constrictions": False,
            "nb_channel_regions": 1,
            "flow_direction": "left",
            "tolerance_window": 60,
            "channel_regions": [[2, 80]],
            "channel_x_min": 2,
            "channel_x_max": 80,
        }
        assert new_metadata != utils.TRACK_METADATA_DEFAULTS
        # change the tracking metadata
        tr_ds.set_tracking_metadata(new_metadata)
        assert tr_ds.print_tracking_metadata() == new_metadata


def test_upating_channel_values_manually_by_min_max():
    """Check that different chip geometries are handled correctly"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        tr_ds.set_channel_values_manual(x_min=4, x_max=100)
        assert tr_ds.channel_x_min == 4
        assert tr_ds.channel_x_max == 100
        assert tr_ds.channel_regions == [[4, 100]]


def test_upating_channel_values_manually_by_channel_regions():
    """Check that different chip geometries are handled correctly.
    Here: inlet + channel"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        channel_regions = [[0, 100], [100, 300]]
        tr_ds.set_channel_values_manual(channel_regions=channel_regions)
        assert tr_ds.channel_x_min == 0
        assert tr_ds.channel_x_max == 100
        assert tr_ds.channel_regions == [[0, 100], [100, 300]]


def test_upating_channel_values_manually_by_min_max_userwarning():
    """Check that different chip geometries are handled correctly.
    This should raise a user warning if multiple_constrictions is True and
    only the channel's min and max (start and end) values are given."""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        tr_ds.set_multiple_constrictions(True)
        with pytest.raises(UserWarning):
            tr_ds.set_channel_values_manual(x_min=4, x_max=100)


def test_set_metadata_by_key_value_wrong():
    """Show that the tracking metadata is set via key and value"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        assert tr_ds.channel is True
        with pytest.warns(UserWarning):
            tr_ds.set_tracking_metadata({"bad key": False})


def test_set_temp_feats():
    """Show that the temporary features exist in dataset after setting"""
    with dclab.new_dataset(input_path) as ds:
        tr_ds = TrackedDataSet(ds)
        for feat in utils.TRACK_TEMPORARY_FEATURES:
            assert dclab.definitions.feature_exists(
                feat), "plugin feature should exist"
            # temp feats are not accessible before setting
            assert feat not in tr_ds.dataset
        # set the required metadata to set temp feats
        tr_ds.set_outlet(True)
        # set the temporary features
        tr_ds._create_object_prediction_features()
        tr_ds._create_velocity_time_features()
        for feat in utils.TRACK_TEMPORARY_FEATURES:
            assert feat in tr_ds.dataset


def test_export_all_temp_feats_set():
    """Show that all temp feats are available after saving if all are set"""
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        tr_ds = TrackedDataSet(dclab.new_dataset(input_path))
        # make sure the outlet is set to True so that the temp feat is set
        tr_ds.set_outlet(True)
        # set some temporary features
        tr_ds._create_object_prediction_features()
        tr_ds._create_velocity_time_features()
        expath = tpath / "exported.rtdc"
        # export only the neccessary features and temporary features
        necessary_features = (utils.TRACKING_FEATURES +
                              utils.TRACK_TEMPORARY_FEATURES)
        for feat in necessary_features:
            assert feat in tr_ds.dataset
        tr_ds.export_tracked_dataset_as_hdf5(expath)
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            for feat in necessary_features:
                assert feat in tr_ds2.dataset


def test_export_all_temp_feats_not_set():
    """Show that temp feats are not available after saving if not set"""
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        tr_ds = TrackedDataSet(dclab.new_dataset(input_path))
        tr_ds.set_outlet(False)
        # set some temporary features
        tr_ds._create_object_prediction_features()
        tr_ds._create_velocity_time_features()

        tr_ds.export_tracked_dataset_as_hdf5(expath)
        del tr_ds
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            assert "time_inlet" in tr_ds2.dataset
            assert "time_outlet" not in tr_ds2.dataset


def test_get_unique_object_numbers():
    """Test the funcionality of `TrackedDataSet.objects()`."""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel_values_auto()
    tr_ds.track()

    objects = tr_ds.objects()
    assert len(objects) > 0
    assert not np.any(np.isnan(objects))


def test_get_unique_object_numbers_filtered():
    """Test the funcionality of `TrackedDataSet.objects()`."""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.set_channel_values_auto()
    tr_ds.track()

    tr_ds.filter_obj_n_events = True
    tr_ds.filter_backward_movement = True
    tr_ds.filter_obj_distance = True
    tr_ds.filter(max_back_movement=0,
                 min_distance=10, max_distance=300,
                 min_events=3, max_events=10000)

    objects = tr_ds.objects()
    objects_filtered = tr_ds.objects(filtered=True)

    assert len(objects) > len(objects_filtered)


def test_fluorescence_assignment():
    """Test whether an assignment of fluorescence peak
    information works."""
    tr_ds = TrackedDataSet(input_path2)
    tr_ds.inlet = True
    tr_ds.channel = True
    tr_ds.outlet = False
    tr_ds.set_nb_channel_regions(1)
    tr_ds.set_flow_direction("left")
    tr_ds.set_channel_values_manual(channel_regions=[[0, 118], [118, 1000]])
    tr_ds.track()
    tr_ds.assign_fluorescence_peak()
    assert "fl3_max_obj" in tr_ds.dataset
    assert np.isnan(tr_ds.dataset["fl3_max_obj"][1])
    assert tr_ds.dataset["fl3_max_obj"][10] == 1535
    assert tr_ds.dataset["fl3_max_obj"][-1] == 1535


def test_fluorescence_assignment_not_possible():
    """Check that error is raised when trying to use fluorescence information
    although it is not possible"""
    tr_ds = TrackedDataSet(input_path)
    tr_ds.inlet = True
    tr_ds.channel = True
    tr_ds.outlet = False
    tr_ds.set_nb_channel_regions(2)
    tr_ds.set_channel_values_auto()
    tr_ds.track()
    with pytest.raises(FluorescenceNotUsableError):
        tr_ds.assign_fluorescence_peak()


def test_fluorescence_assignment_not_possible_2():
    """Check that error is raised when trying to use fluorescence information
    but the dataset has not been tracked yet."""
    tr_ds = TrackedDataSet(input_path2)
    tr_ds.inlet = True
    tr_ds.channel = False
    tr_ds.outlet = False
    tr_ds.set_nb_channel_regions(1)
    tr_ds.set_channel_values_manual(channel_regions=[[0, 118], [118, 1000]])
    with pytest.raises(DatasetNotTrackedError):
        tr_ds.assign_fluorescence_peak()
