import pytest
import pathlib
import tempfile as tf
import dclab

from dctrack import io
from dctrack._version import version
from dctrack.track import TrackedDataSet
from dctrack import utils

data_dir = pathlib.Path(__file__).parent / "data"
data_file = "M002_data_0002_export_filtered.rtdc"
input_path = str(data_dir / data_file)


def test_required_tracking_features():
    """Save and reload an rtdc file that contains utils.TRACKING_FEATURES"""
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        tr_ds = TrackedDataSet(input_path)
        assert "pos_x" and "frame" and "index" in tr_ds.dataset

        expath = tpath / "exported.rtdc"
        tr_ds.dataset.export.hdf5(
            expath, features=["deform", "area_um", "pos_x", "frame", "index"])
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            assert "pos_x" and "frame" and "index" in tr_ds2.dataset


def test_required_tracking_features_fail():
    """Save and reload an rtdc file without utils.TRACKING_FEATURES"""
    tpath = pathlib.Path(tf.mkdtemp())
    tr_ds = TrackedDataSet(input_path)
    assert "pos_x" and "frame" and "index" in tr_ds.dataset

    expath = tpath / "exported.rtdc"
    tr_ds.dataset.export.hdf5(expath, features=["deform", "area_um"])
    # open again with dctrack (will fail)
    with pytest.raises(ValueError):
        TrackedDataSet(expath)


def test_load_data_for_tracking_temp_feats_exist():
    """Show that the temporary features exist upon loading"""
    io.load_data_for_tracking(input_path)
    for feat in utils.TRACK_TEMPORARY_FEATURES:
        assert dclab.definitions.feature_exists(
            feat), "plugin feature should exist"


def test_update_software_version():
    """Check that the version of dctrack is correctly added to the string with
    the list of software."""
    new_version = io.update_software_version(input_path,
                                             write_attribute=False)
    assert isinstance(new_version, str)
    dctrack_version = new_version.split("|")[-1]
    assert "dctrack" in dctrack_version
    assert version in dctrack_version


def test_update_software_version_in_file():
    """Check that the version of dctrack is added to the software list in
    the config section of the .rtdc file."""
    tr_ds = TrackedDataSet(input_path)
    old_version = tr_ds.dataset.config["setup"]["software version"]
    assert "ShapeIn" in old_version
    assert "dclab" in old_version
    assert "dctrack" not in old_version
    # set some temporary features
    tr_ds._create_object_prediction_features()
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        io.export_tracked_dataset_as_hdf5(tr_ds, expath)
        with dclab.new_dataset(expath) as ds:
            new_version = ds.config["setup"]["software version"]
            assert "dctrack" in new_version
            assert version in new_version.split("|")[-1]


def test_saving_temp_feat():
    """Show that temp feats are saved after setting"""
    tr_ds = TrackedDataSet(input_path)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    assert "object_number" in tr_ds.dataset
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        # export this temporary feature
        export_features = [
            "deform", "pos_x", "frame", "index", "object_number"]
        tr_ds.dataset.export.hdf5(expath, features=export_features)
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            for feat in export_features:
                assert feat in tr_ds2.dataset


def test_saving_metadata():
    """Show that metadata is saved"""
    tr_ds = TrackedDataSet(input_path)
    assert tr_ds.outlet is False
    # change some metadata
    tr_ds.set_outlet(True)
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        tr_ds.dataset.export.hdf5(expath, features=[
            "deform", "pos_x", "frame", "index"])
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            assert tr_ds2.outlet


def test_saving_with_dclab_export_hdf5_with_set_temp_feats():
    """Export the TrackedDataSet.dataset after setting all temp feats"""
    tr_ds = TrackedDataSet(input_path)
    # make sure the outlet is set to True so that the temp feat is set
    tr_ds.set_outlet(True)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    assert "object_number" in tr_ds.dataset
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        # export only the neccessary features and temporary features
        necessary_features = (utils.TRACKING_FEATURES +
                              utils.TRACK_TEMPORARY_FEATURES)
        tr_ds.dataset.export.hdf5(expath, features=necessary_features)
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            assert "object_number" in tr_ds2.dataset


def test_saving_with_dclab_export_hdf5_without_set_temp_feats():
    """Cannot export all the temp feats after not setting all temp feats"""
    tr_ds = TrackedDataSet(input_path)
    # make sure the outlet is set to False so that export fails
    tr_ds.set_outlet(False)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    assert "object_number" in tr_ds.dataset
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        # export only the neccessary features and temporary features
        necessary_features = (utils.TRACKING_FEATURES +
                              utils.TRACK_TEMPORARY_FEATURES)
        with pytest.raises(KeyError):
            tr_ds.dataset.export.hdf5(expath, features=necessary_features)


def test_export_tracked_dataset_as_hdf5_all_set():
    """Export the TrackedDataSet after setting all temp feats"""
    tr_ds = TrackedDataSet(input_path)
    # make sure the outlet is set to True so that the temp feat is set
    tr_ds.set_outlet(True)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        # export only the neccessary features and temporary features
        necessary_features = (utils.TRACKING_FEATURES +
                              utils.TRACK_TEMPORARY_FEATURES)
        for feat in necessary_features:
            assert feat in tr_ds.dataset
        io.export_tracked_dataset_as_hdf5(tr_ds, expath)
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            for feat in necessary_features:
                assert feat in tr_ds2.dataset


def test_export_tracked_dataset_as_hdf5_all_not_set():
    """Export the TrackedDataSet after not setting all temp feats"""
    tr_ds = TrackedDataSet(input_path)
    # make sure the outlet is set to False so that it isn't set
    tr_ds.set_outlet(False)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        io.export_tracked_dataset_as_hdf5(tr_ds, expath)
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            assert "time_outlet" not in tr_ds2.dataset


def test_export_dctrack_no_features_set():
    """Export the TrackedDataSet with `features=None`"""
    tr_ds = TrackedDataSet(input_path)
    # make sure the outlet is set to False so that it isn't set
    tr_ds.set_outlet(False)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        tr_ds.export_tracked_dataset_as_hdf5(path=expath)
        # open again with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            test_feats = ["object_number", "time_channel", "deform", "pos_y"]
            for feat in test_feats:
                assert feat in tr_ds2.dataset
            assert "time_outlet" not in tr_ds2.dataset


def test_export_with_dclab_and_reload_with_dctrack():
    """Reload and export TrackedDataSet.dataset with dclab"""
    tr_ds = TrackedDataSet(input_path)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        tr_ds.dataset.export.hdf5(expath, features=[
            "deform", "pos_x", "frame", "index"])
        # open with dctrack
        with TrackedDataSet(expath) as tr_ds2:
            # show that metadata is saved
            assert tr_ds2.dataset.config["user"]["channel"]
            # show that temp feats exist
            assert dclab.definitions.feature_exists("object_number")
            # temp feats cannot be immediately accessed as they are not saved
            # above
            assert "object_number" not in tr_ds2.dataset
            with pytest.raises(KeyError):
                _ = tr_ds2.dataset["object_number"]


def test_export_with_dclab_and_reload_with_dclab():
    """Reload and export TrackedDataSet.dataset with dclab"""
    tr_ds = TrackedDataSet(input_path)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        tr_ds.dataset.export.hdf5(expath, features=[
            "deform", "pos_x", "frame", "index"])
        # open with dclab
        with dclab.new_dataset(expath) as rtdc_ds:
            # show that metadata is saved
            assert rtdc_ds.config["user"]["channel"]
            # show that temp feats exist
            assert dclab.definitions.feature_exists("object_number")
            # temp feats cannot be immediately accessed as they are not
            # saved above
            assert "object_number" not in rtdc_ds
            with pytest.raises(KeyError):
                _ = rtdc_ds["object_number"]


def test_export_with_dctrack_and_reload_with_dclab():
    """Reload the exported TrackedDataSet with dclab"""
    tr_ds = TrackedDataSet(input_path)
    # set some temporary features
    tr_ds._create_object_prediction_features()
    tr_ds._create_velocity_time_features()
    with tf.TemporaryDirectory() as t_dir:
        tpath = pathlib.Path(t_dir)
        expath = tpath / "exported.rtdc"
        io.export_tracked_dataset_as_hdf5(tr_ds, expath)
        # open with dclab
        with dclab.new_dataset(expath) as rtdc_ds:
            # show that metadata is saved
            assert rtdc_ds.config["user"]["channel"]
            # show that temp feats exist
            assert dclab.definitions.feature_exists("object_number")
            # temp feats can be immediately accessed
            assert "object_number" in rtdc_ds
            _ = rtdc_ds["object_number"]
            # at a later time point, temp feats can be accessed after
            # registering
            io.check_and_load_temporary_features()
            assert "object_number" in rtdc_ds
