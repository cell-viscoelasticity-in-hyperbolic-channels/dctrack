import pytest

from dctrack import main


@pytest.fixture(scope="session")
def gui():
    tr_session = main.TrackingSession()
    return tr_session.get_app()
