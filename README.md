# DCTrack

`dctrack` is a Python library for post-measurement analysis of real-time deformability 
cytometry (RT-DC) datasets to track individual objects when they pass a channel 
and assign contour and image parameters. The aim is to make it possible to track 
cells from the datasets independent of the actual channel design.

## Installation

1. Download or clone the `rtdc_tracking2.0` repository to a suitable folder.
   - To clone, use the blue "clone" button at the top right of this page.
   - Alternatively, use the "download" button which is left of the blue
     "clone" button. Unzip the zipped folder.

2. Create a virtual environment in this folder.
    - You can do this with an IDE such as PyCharm easily.
    - Alternatively:
       - open the terminal, cd into the `rtdc_tracking2.0` folder, and
         run `python -m venv venv`.
       - Activate the `venv` by running `venv\Scripts\activate.bat` (Windows)
         or `venv/bin/activate` (Linux/Mac).
         
3. Install the necessary packages.
    - With the virtual enviroment activated, use `pip` to do the following:
       - `pip install wheel` which will help speed up the following:
       - `pip install -e .` which installs dctrack in edittable mode and all
    of its dependencies.
         
## Running the example Jupyter Notebook

The `rtdc_tracking2.0/examples` folder contains example Jupyter Notebooks
that show how `dctrack` works. There is also a `.py` version.
You can run the notebook by:

1. Activating the virtual environment as described above.
2. Install jupyter notebook by running `pip install jupyter`
3. Run `jupyter notebook`. This should open a new browser tab.
4. Open the "examples" folder and click the "dctrack_example.ipynb" file.
   A new jupyter notebook will open, which describes how to use `dctrack`.
   
## Help

If you have any problems or find a bug with `dctrack`,
please [create an issue](https://gitlab.gwdg.de/freiche/rtdc_tracking2.0/-/issues).

For installation and using Jupyter Notebooks, the following videos might be
useful:
 - [How to change directory](https://youtu.be/U5m-EBa8iCQ?t=428)
  in terminal (`cd`).
 - [How to use virtual environments](https://www.youtube.com/watch?v=8E6k2fltn5k)
 - [How to use `pip`](https://www.youtube.com/watch?v=vTSpz-LxYFs)
 - [How to use Jupyter Notebook](https://www.youtube.com/watch?v=HW29067qVWk)


## Information for Developers

Install `flake8` for linting and `pytest` for testing using:

```bash
pip install flake8 pytest
```

Lint the code (checks the syntax) by running:

```bash
flake8 dctrack tests examples
```

Run the tests with:

```bash
pytest tests
```

Note: on Mac/Linux you may need to have `python3` before each command.
