import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

author1 = u"Felix Reichel"
author2 = u"Benedikt Hartmann"
authors = [author1, author2]

setuptools.setup(
    name='dctrack',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description="Track object from rt-dc datasets",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.gwdg.de/freiche/rtdc_tracking2.0',
    author=authors,
    author_email="felix.reichel@mpl.mpg.de",
    year="2021",
    license="GPL v2",
    packages=setuptools.find_packages(),
    install_requires=["numpy>=1.18.0",
                      "scipy>=1.6.0",
                      "opencv-python>=4.5.0",
                      "pyqtgraph>=0.12.0",
                      "pyqt5>=5.15.0",
                      "Pyqt5-sip<12.11.0",
                      "dclab>=0.50.0",
                      "shapely>=1.7.0",
                      "matplotlib>=3.3.4",
                      "imageio>=2.9.0",
                      "imageio-ffmpeg",
                      "pandas>=1.2.0",
                      "tqdm>=4.59.0",
                      "scikit-image>=0.17.0",
                      ],
    tests_require=["pytest"],
    python_requires=">=3.9",
    classifiers=[
              "Programming Language :: Python :: 3",
              "Operating System :: Microsoft :: Windows",
              ],
    zip_safe=False,)
